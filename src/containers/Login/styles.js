import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RF from "react-native-responsive-fontsize";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';


export const COLOR = {
    BLACK: "#000",
    GRAY: "#9A9A9A",
    WHITE: "#FFF",
    LIGHT_ORANGE: "#E69151"
};
var padding
var paddingvet
Platform.select({
    ios: () => {
        padding = 15
        paddingvet = null
    },
    android: () => {
        padding = 0
        paddingvet = 15
    }
})();
export default StyleSheet.create({

    container: {
        backgroundColor: '#CCCCCC',
        height: Dimensions.get('window').height,
        display: 'flex',
        width: '100%',
    },
    overlay:{
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        height:'100%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'

    },
    spinnerStyle: {
        flex: 1,
        alignSelf:'center'
    },
    textinput1: {
        color: '#000',
        marginLeft: 10,
        fontSize: RF(2.3),
        marginTop: 2
    },
    viewInputArea: {
        width: width / 1.15,
        backgroundColor: '#fff',
        alignSelf: 'center',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowColor: '#000000',
        elevation: 5,
        padding: 15,
    },
    ImageBackground: {
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    },

    viewSpace: {
        height: hp('25%')
    },



    mainViewUserName: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        height: hp('7%')
    },
    mainViewPassword: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        height: hp('7%')
    },
    viewInputArea_img: {
        alignSelf: 'center'
    },
    InputArea_img: {
        width: wp('4%'),
        height: hp('2%')
    },
    straightLine: {
        height: 1,
        width: wp('75%'),
        backgroundColor: '#e9e9e9',
        alignSelf: 'center',
    },
    inputTextView: {
        width: wp('70%')
    },
    LinearGradient: {
        height: hp('8%'),
        width: wp('75%'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        alignSelf: 'center',
        marginTop: 20
    },
    txtLogin: {
        color: '#fff',
        fontSize: RF(2.8),
        fontWeight: 'bold'
    },
    txtForget: {
        color: '#fe5328',
        fontSize: RF(1.5),
        textDecorationLine: 'underline',
        fontWeight: 'bold'
    },
    txtSkipLogin: {
        color: '#fe5328',
        fontSize: RF(2.5),
        textDecorationLine: 'underline',
        fontWeight: 'bold'
    },
    txtDontHaveAccount: {
        color: '#2c2c2c',
        fontSize: RF(2.5),
    },
    txtSighUp: {
        color: '#fe5328',
        fontSize: RF(2.5),
        marginLeft: 5,
        textDecorationLine: 'underline',
        fontWeight: 'bold'
    },
    viewSpaceHeight: {
        height: 15
    },
    viewDontAccount: {
        marginTop: 20,
        alignSelf: 'center'
    },
    viewSkipLogin: {
        marginTop: 10,
        alignSelf: 'center'
    },
    viewFacebook: {
        width: wp('35%'),
        height: hp('8%'),
        backgroundColor: '#375999',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6
    },
    viewGmail: {
        width: wp('35%'),
        height: hp('8%'),
        backgroundColor: '#f54133',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        marginLeft: 20
    },
    txtSocial: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: RF(2.5)
    },
    viewSocial: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 10
    },
    viewOr: {
        alignItems: 'center',
        marginTop: 10
    },
    imgOr: {
        width: wp('10%'),
        height: hp('5%')
    },
    viewForget: {
        alignSelf: 'flex-end',
        paddingRight: 10,
        marginTop: 20
    },


});