import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, KeyboardAvoidingView,ActivityIndicator
} from "react-native";
import styles from './styles';
import { TextField } from 'react-native-material-textfield';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import SyncStorage from 'sync-storage';


var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class Login extends React.Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            secureKey: true,
            isLoading:false
        };
    }

    secureKey() {
        if (this.state.secureKey == false) {
            this.setState({
                secureKey: true
            })
        }
        else {
            this.setState({
                secureKey: false
            })
        }
    }

    mLoaderShowHide() {
        this.setState({
            isLoading: !this.state.isLoading
        });
      };

    mValidation() {
    if(this.state.username.length<=0){
    alert('Please enter username')
    return false;
    }else if(this.state.password.length<=0){
    alert('Please enter password')
    return false;
    }
    this.mLoaderShowHide();
    this.login(this.state.username,this.state.password);
        
    }

    login(username,password) {
        let form_data = new FormData();
        form_data.append("username",username)
        form_data.append("password",password)
         const requestOptions = {
         method: 'POST',
         body: form_data
         };
         fetch('https://beta.pedallion.com/api/login', requestOptions)
         .then((response) => this.handleResponse(response))
         .then(user => {
         });
         }

        handleResponse(response) {
         response.text().then(text => {
             if (response.status==200) {
             const data = text && JSON.parse(text);
             this.mLoaderShowHide();
             if(data.status==1){
             const datakey = SyncStorage.init();
             SyncStorage.set('token',data.token);
             SyncStorage.set('user_name',data.user_name);
             SyncStorage.set('user_image_url',data.user_image_url);
             const result =  SyncStorage.get('token');
             console.log("<><><> "+result);
             alert('You have logged in successfully')
             this.props.navigation.navigate('DrawerNavigator')
             }else if(data.status==3){
             alert(data.msg)
             }else{
             alert('Invalid Username or Password')
             }
             }else{
             this.mLoaderShowHide();
             }


           
        });
    }

   
     




    render() {
        return (
            <View>
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <View>
                
                    <View>
                        <ImageBackground source={require("../../components/Images/login_bg_2.jpg")}
                            style={styles.ImageBackground}>
                            <View style={styles.viewSpace}/>
                            <View style={styles.viewInputArea}>
                                <View style={styles.mainViewUserName}>
                                    <View style={styles.viewInputArea_img}>
                                    <Image source={require("../../components/Images/Loginimg.png")}
                                    style={styles.InputArea_img}>
                                    </Image>
                                    </View>
                                    <View style={styles.inputTextView}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Username"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            onChangeText={(text) => this.setState({ username: text })} />
                                    </View>
                                </View>
                                <View style={styles.straightLine} />
                                <View style={styles.mainViewPassword}>
                                    <View style={styles.viewInputArea_img}>
                                    <Image source={require("../../components/Images/passwordimg.png")}
                                    style={styles.InputArea_img}>
                                    </Image>
                                    </View>
                                    <View style={styles.inputTextView}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Password"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            secureTextEntry={true}
                                            onChangeText={(text) => this.setState({password: text })} />
                                    </View>
                                </View>
                                <View style={styles.straightLine} />
                                <TouchableOpacity onPress={() => this.mValidation()}>
                                    <LinearGradient style={styles.LinearGradient}
                                    colors={['#feae3d','#ff8333','#ff572b',]}>
                                    <Text style={styles.txtLogin}>
                                    LOGIN
                                    </Text>

                                    </LinearGradient>
                                </TouchableOpacity>

                                <View style={styles.viewForget}>
                                 <TouchableOpacity onPress={() => this.props.navigation.navigate('Forget')}>
                                    <Text style={styles.txtForget}>
                                    FORGOT PASSWORD
                                    </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.viewOr}>
                                    <Image source={require("../../components/Images/orimg.png")}
                                        style={styles.imgOr}>
                                    </Image>
                                </View>
                                <View style={styles.viewSocial}>
                                    <TouchableOpacity>
                                    <View style={styles.viewFacebook}>
                                    <Text style={styles.txtSocial}>FACEBOOK</Text>
                                    </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                    <View style={styles.viewGmail}>
                                    <Text style={styles.txtSocial}>GMAIL</Text>
                                    </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.viewSpaceHeight} />
                            </View>
                            <View style={styles.viewSpaceHeight}/>

                            <View style={styles.viewSkipLogin}>
                            <TouchableOpacity onPress={() => {this.props.navigation.navigate('DrawerNavigator')} }>
                            <Text style={styles.txtSkipLogin}>SKIP LOGIN >></Text>
                            </TouchableOpacity>
                            </View>

                            <View style={styles.viewDontAccount}>
                            <TouchableOpacity onPress={() => {this.props.navigation.navigate('SignUp')} }>
                            <Text style={styles.txtDontHaveAccount}>Don't Have An Account? <Text style={styles.txtSighUp}>SIGN UP</Text></Text>
                            </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>

                  
                  
                </View>
                </KeyboardAvoidingView>
                {this.state.isLoading==true ? <View style={styles.overlay}>
                  <ActivityIndicator size="large" color="#f27029" />
                  </View> : <View></View>}
                </View>

        );
    }
}


export default Login;