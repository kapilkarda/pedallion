import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, KeyboardAvoidingView,ActivityIndicator
} from "react-native";
import styles from './styles';
import { TextField } from 'react-native-material-textfield';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';


var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class SignUp extends React.Component {

    static navigationOptions = {
        header: null
    }

    mValidateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      };

    constructor(props) {
        super(props);
        this.state = {
            fullname:'',
            username: '',
            email:'',
            password: '',
            secureKey: true,
            isLoading:false
        };
    }

    secureKey() {
        if (this.state.secureKey == false) {
            this.setState({
                secureKey: true
            })
        }
        else {
            this.setState({
                secureKey: false
            })
        }
    }

    mLoaderShowHide() {
        this.setState({
            isLoading: !this.state.isLoading
        });
      };

    mValidation() {
        if(this.state.fullname.length<=0){
        alert('Please enter full name')
        return false;
        }else if(this.state.username.length<=0){
        alert('Please enter user name')
        return false;
        }else if(this.state.email.length<=0){
        alert('Please enter email address')
        return false;
        }else if (!this.mValidateEmail(this.state.email)) {
        alert('Please enter a valid email address.')
        return false;
        }else if(this.state.password.length<=0){
        alert('Please enter password')
        return false;
        }
        this.mLoaderShowHide();
        this.signUp();
            
        }

        signUp() {
            let form_data = new FormData();
            form_data.append("username",this.state.username)
            form_data.append("password",this.state.password)
            form_data.append("email",this.state.email)
            form_data.append("name",this.state.fullname)
             const requestOptions = {
             method: 'POST',
             body: form_data
             };
             fetch('https://beta.pedallion.com/api/signup', requestOptions)
             .then((response) => this.handleResponse(response))
             .then(user => {
             });
             }

             handleResponse(response) {
                response.text().then(text => {
                    if (response.status==200) {
                    const data = text && JSON.parse(text);
                    this.mLoaderShowHide();
                    if(data.status==1){
                    alert('Registration successful')
                    this.props.navigation.navigate('DrawerNavigator')
                    }else{
                    alert(data.msg)
                    }
                    }else{
                    this.mLoaderShowHide();
                    }
       
       
                  
               });
           }
    


    render() {
        return (
          <View>
<KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <View>
                    <View>
                        <ImageBackground source={require("../../components/Images/login_bg_2.jpg")}
                            style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                            <View   style={{height:hp('25%')}}/>
                            <View style={styles.view1}>
                                <View style={{ flexDirection: 'row', alignSelf: 'center',alignItems:'center',height: hp('7%') }}>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Image source={require("../../components/Images/Loginimg.png")}
                                            style={{ width: wp('4%'), height: hp('2%'), }}>

                                        </Image>
                                    </View>
                                    <View style={{ width: wp('70%') }}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Full Name"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            onChangeText={(text) => this.setState({ fullname: text })} />
                                    </View>
                                </View>
                                <View style={{
                                    height: 1,
                                    width: wp('75%'),
                                    backgroundColor: '#e9e9e9',
                                    alignSelf: 'center',
                                   
                                }} />
                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems:'center', height: hp('7%') }}>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Image source={require("../../components/Images/Loginimg.png")}
                                            style={{ width: wp('3.2%'), height: hp('2.5%'), }}>

                                        </Image>
                                    </View>
                                    <View style={{ width: wp('70%') }}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Username"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            onChangeText={(text) => this.setState({ username: text })} />
                                    </View>
                                </View>
                                <View style={{
                                    height: 1,
                                    width: wp('75%'),
                                    backgroundColor: '#e9e9e9',
                                    alignSelf: 'center'
                                }} />


<View style={{ flexDirection: 'row', alignSelf: 'center', alignItems:'center', height: hp('7%') }}>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Image source={require("../../components/Images/mail.png")}
                                            style={{ width: wp('3.2%'), height: hp('2.5%'), }}>

                                        </Image>
                                    </View>
                                    <View style={{ width: wp('70%') }}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Email"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            onChangeText={(text) => this.setState({ email: text })} />
                                    </View>
                                </View>
                                <View style={{
                                    height: 1,
                                    width: wp('75%'),
                                    backgroundColor: '#e9e9e9',
                                    alignSelf: 'center'
                                }} />


<View style={{ flexDirection: 'row', alignSelf: 'center', alignItems:'center', height: hp('7%') }}>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Image source={require("../../components/Images/passwordimg.png")}
                                            style={{ width: wp('3.2%'), height: hp('2.5%'), }}>

                                        </Image>
                                    </View>
                                    <View style={{ width: wp('70%') }}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Password"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            secureTextEntry={true}
                                            onChangeText={(text) => this.setState({ password: text })} />
                                    </View>
                                </View>
                                <View style={{
                                    height: 1,
                                    width: wp('75%'),
                                    backgroundColor: '#e9e9e9',
                                    alignSelf: 'center'
                                }} />

<View style={{
                                    marginTop: 20
                                }}>
                              
                              <Text style={{
                                        color: '#fe5328',
                                        fontSize: RF(2.2),
                                    }}>
                                     By registering here you are agreed to the Terms & Conditions.
                                    </Text>

                                </View>

                                <TouchableOpacity
                                  onPress={() => this.mValidation()}
                                >
                                    <LinearGradient style={{
                                        height: hp('8%'),
                                        width: wp('75%'),
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderRadius: 2,
                                        alignSelf: 'center',
                                        marginTop: 20
                                    }}
                                        start={{
                                            x: 0,
                                            y: 0
                                        }}
                                        end={{
                                            x: 0,
                                            y: 1
                                        }}
                                        colors={['#feae3d',
                                            '#ff8333',
                                            '#ff572b',]}>
                                        <Text style={{
                                            color: '#fff',
                                            fontSize: RF(2.8),
                                            fontWeight: 'bold'
                                        }}>
                                            REGISTER
                                         </Text>

                                    </LinearGradient>
                                </TouchableOpacity>
                               
                              
                            </View>
                            <View   style={{height:15}}/>

                            <View style={{marginTop:10,alignSelf:'center'}}>
                            <TouchableOpacity onPress={() => {this.props.navigation.navigate('Login')} }>
                                        <Text style={{
                                             color: '#fe5328',
                                             fontSize: RF(2.5),
                                             textDecorationLine: 'underline',
                                             fontWeight: 'bold'
                                        }}>Already Registered?</Text>
                                        </TouchableOpacity>
                                    </View>
                        </ImageBackground>
                    </View>
                  
                </View>
                </KeyboardAvoidingView>
                {this.state.isLoading==true ? <View style={styles.overlay}>
                  <ActivityIndicator size="large" color="#f27029" />
                  </View> : <View></View>}
                </View>
        );
    }
}


export default SignUp;