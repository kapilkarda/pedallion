import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,Dimensions,ScrollView,SafeAreaView,
} from 'react-native';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
import Header from "./Header";

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class MessageDetail extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#f3f3f3'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>


<View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2.2%'),marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('20%'),width:wp('20%')}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('20%'),
                                                 height:wp('20%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginRight:15}}>Revv Amplification G4 Distortion</Text>
 <Text style={{ color:'#fe5328',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>{'\u20B1'} 11900.00</Text>
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>Placed On 14th April 2019</Text>
 

  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>

 <View style={{height:1,backgroundColor:'#7f7f7f',marginTop:10,marginBottom:10}}/>

 <ScrollView style={{marginBottom:20}}>
        <View style={{ alignSelf: 'center',alignItems:'center'}}>
                            <Text style={{color: '#000',fontSize:hp('1.9%')}}>18th April 2019</Text>
                            <Text style={{color: '#7f7f7f',fontSize:hp('1.9%'),marginTop:2}}>14:30</Text>
                        </View>

<View style={{flex:1,flexDirection:'row',marginLeft:10,marginRight:10,marginTop:30}}>

<View style={{flex:2}}>

</View>

<View style={{flex:3}}>
<View style={{backgroundColor:'#F95127',padding:10,alignItems:'center',borderRadius: 2,}}>
<Text style={{color: '#ffffff',fontSize:hp('1.9%')}}>Hi, By when i can have this product at my home??</Text>
</View>

</View>


</View>

<View style={{backgroundColor:'#FEFEAB',alignItems:'center',marginLeft:20,marginRight:20,marginTop:20,marginBottom:20,padding:7,borderRadius: 2}}>
<Text style={{color: '#000',fontSize:hp('1.8%'),fontWeight:'bold'}}>4 NEW MESSAGES</Text>
</View>

<View style={{flex:1,flexDirection:'row',marginLeft:10,marginRight:10,marginTop:10}}>


<View style={{flex:3}}>
<View style={{backgroundColor:'#FFFFFF',padding:10,borderRadius: 2,}}>
<Text style={{color: '#000',fontSize:hp('1.9%')}}>Hello, Thanks for buying from us:)</Text>
</View>
<View style={{backgroundColor:'#FFFFFF',padding:10,borderRadius: 2,marginTop:5}}>
<Text style={{color: '#000',fontSize:hp('1.9%')}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry</Text>
</View>
<View style={{backgroundColor:'#FFFFFF',padding:10,borderRadius: 2,marginTop:5}}>
<Text style={{color: '#000',fontSize:hp('1.9%')}}>Dummy text of the printing and type</Text>
</View>
<View style={{backgroundColor:'#FFFFFF',padding:10,borderRadius: 2,marginTop:5}}>
<Text style={{color: '#000',fontSize:hp('1.9%')}}>Thanks! Happy Buying</Text>
</View>

</View>

<View style={{flex:2}}>

</View>




</View>


                        </ScrollView>

    
 <View style={{width: '100%',position: 'absolute',bottom: 0,flexDirection:'row',backgroundColor:'#fff',padding:10}} >
<View style={{flex:5,backgroundColor:'#F2F3F8',justifyContent:'center',alignItems:'center'}}>
<TextInput style={{width:'95%',height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
</View> 
<View style={{flex:1,alignItems:'flex-end'}}>
<Image source={require('./../../components/Images/chat_icon.png')}
                                            style={{ width:35, height:35}}>
                                        </Image>
</View> 

</View>
      

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});
