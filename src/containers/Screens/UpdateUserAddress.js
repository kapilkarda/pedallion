import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ActivityIndicator
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RNPickerSelect from 'react-native-picker-select';
import { Chevron } from 'react-native-shapes';
import SyncStorage from 'sync-storage';

const sports = [
    {
      label: 'Football',
      value: 'football',
    },
    {
      label: 'Baseball',
      value: 'baseball',
    },
    {
      label: 'Hockey',
      value: 'hockey',
    },
  ];



export default class UpdateUserAddress extends Component{


  static navigationOptions = {
    header: null
}


constructor(props) {
    super(props);
    this.state = {
    mStrAddressId:'',
    mStrName:'',
    mStrAddress1:'',
    mStrAddress2:'',
    mStrCountry:'',
    mStrCountryId:'',
    mStrState:'',
    mStrStateId:'',
    mStrCity:'',
    mStrCityId:'',
    mStrArea:'',
    mStrAresId:'',
    mStrZip:'',
    mStrPhone:'',
    isLoading:false,
    dataSource:[],
    dataSourceState:[],
    dataSourceCity:[],
    dataSourceArea:[],
    mStrToken:'',
    mStrCityTxt:''
    };
  }
  

  componentWillMount() {
    const { navigation } = this.props;  
    const result =  SyncStorage.get('token');
    
    const id = navigation.getParam('id', 'NO-User');  
    const name = navigation.getParam('name', 'User Name'); 
    const address1 = navigation.getParam('address1', 'Address1');  
    const address2 = navigation.getParam('address2', 'Address2'); 
    const countryName = navigation.getParam('countryName', 'Select Country');  
    const stateName = navigation.getParam('stateName', 'Select State'); 
    const cityName = navigation.getParam('cityName', 'Select City');  
    const zipCode = navigation.getParam('zipCode', 'Zip Code');  
    const phoneNum = navigation.getParam('phoneNum', 'Phone Number');   
    const countryId = navigation.getParam('countryId', '0');   
    const stateId = navigation.getParam('stateId', '0');   
    const cityId = navigation.getParam('cityId', '0');   
    const AreaId = navigation.getParam('mArId', '0');   
    const AreaName = navigation.getParam('mArName', '0');   

    this.setState({
      mStrAddressId:id,
      mStrToken:result,
      mStrName:name,
      mStrAddress1:address1,
      mStrAddress2:address2,
      mStrCountry:countryName,
      mStrState:stateName,
      mStrCity:cityName,
      mStrZip:zipCode,
      mStrPhone:phoneNum,
      mStrCountryId:countryId,
      mStrStateId:stateId,
      mStrCityId:cityId,
      mStrArea:AreaName,
      mStrAresId:AreaId,
  });
    this.mLoaderShowHide();
    this.getCountry();
}

   getCountry() {
   const requestOptions = {
   method: 'GET',
   };
   fetch('https://beta.pedallion.com/api/countries', requestOptions)
   .then((response) => this.handleResponseCountry(response))
   .then(user => {
   });
   }


   getCity(stateId) {
    let form_data = new FormData();
    form_data.append("state_id",stateId)
    const requestOptions = {
    method: 'POST',
    body: form_data
    };
    fetch('https://beta.pedallion.com/api/cities', requestOptions)
    .then((response) => this.handleResponseCity(response))
    .then(user => {
    });
    }


    getArea(cityId) {
      let form_data = new FormData();
      form_data.append("city_id",cityId)
      const requestOptions = {
      method: 'POST',
      body: form_data
      };
      fetch('https://beta.pedallion.com/api/barangay', requestOptions)
      .then((response) => this.handleResponseArea(response))
      .then(user => {
      });
      }


      handleResponseArea(response) {
        var rawData = [];
        response.text().then(text => {
            if (response.status==200) {
            const data = text && JSON.parse(text);
            this.mLoaderShowHide();
            if(data.status==1){

              for (var i = 0; i < data.barangays.length; i++){
                rawData.push({
                  label: data.barangays[i].name,
                  value: data.barangays[i].id,
                })
                }
                this.setState({
                  dataSourceArea: [...this.state.dataSourceArea, ...rawData],
                });
            }else{
            //alert('Invalid Username or Password')
            }
            }else{
            }
    });
    }




    handleResponseCity(response) {
      var rawData = [];
      response.text().then(text => {
          if (response.status==200) {
          const data = text && JSON.parse(text);
          this.mLoaderShowHide();
          if(data.status==1){
            for (var i = 0; i < data.cities.length; i++){
              rawData.push({
                label: data.cities[i].name,
                value: data.cities[i].id,
              })
              }
              this.setState({
                dataSourceCity: [...this.state.dataSourceCity, ...rawData],
              });
          }else{
          //alert('Invalid Username or Password')
          }
          }else{
          }
  });
  }


   getState(countryId) {
    let form_data = new FormData();
    form_data.append("country_id",countryId)
    const requestOptions = {
    method: 'POST',
    body: form_data
    };
    fetch('https://beta.pedallion.com/api/states', requestOptions)
    .then((response) => this.handleResponseState(response))
    .then(user => {
    });
    }


    handleResponseState(response) {
      var rawData = [];
      response.text().then(text => {
          if (response.status==200) {
          const data = text && JSON.parse(text);
          this.mLoaderShowHide();
          if(data.status==1){
            for (var i = 0; i < data.states.length; i++){
              rawData.push({
                label: data.states[i].name,
                value: data.states[i].id,
              })
              }
              this.setState({
                dataSourceState: [...this.state.dataSourceState, ...rawData],
              });
          }else{
          //alert('Invalid Username or Password')
          }
          }else{
          }
  
  
        
     });
  }




   handleResponseCountry(response) {
    var rawData = [];
    response.text().then(text => {
        if (response.status==200) {
        const data = text && JSON.parse(text);
        this.mLoaderShowHide();
        if(data.status==1){
          for (var i = 0; i < data.countries.length; i++){
            rawData.push({
              label: data.countries[i].name,
              value: data.countries[i].id,
            })
            }
            this.setState({
              dataSource: [...this.state.dataSource, ...rawData],
            });
        }else{
        //alert('Invalid Username or Password')
        }
        }else{
        }
   });
}



  mSearchState(value,index) {
  console.log("<><><> "+value)
  if(value!=0){
  if(value==this.state.mStrCountryId){
    this.mLoaderShowHide();
    this.getState(value);
  }else{
    this.setState({dataSourceState:[],mStrCountryId:value,mStrCountry:this.state.dataSource[index-1].label});  
    this.mLoaderShowHide();
    this.getState(value);
  }
  }  
  //this.props.navigation.navigate('ProductDetailConfirm')
 }

 mSearchCity(value,index){
   if(value !=0){
     if(value==this.state.mStrStateId){
      this.mLoaderShowHide();
      this.getCity(value)
     }else{
      this.setState({dataSourceCity:[],mStrStateId:value,mStrState:this.state.dataSourceState[index-1].label});
      this.mLoaderShowHide();
      this.getCity(value)
     }
   
   }
  
 }

 mSearchArea(value,index){
  if(value !=0){

    if(value==this.state.mStrCityId){
      this.mLoaderShowHide();
      this.getArea(value)
    }else{
      this.setState({dataSourceArea:[],mStrCityId:value,mStrCity:this.state.dataSourceCity[index-1].label});
      this.mLoaderShowHide();
      this.getArea(value)
    }
    
  }
 }


 mSelectArea(value,index){
  if(value !=0){
    if(value==this.state.mStrAresId){
    }else{
    this.setState({mStrAresId:value,mStrArea:this.state.dataSourceArea[index-1].label});
    }
  
  //this.setState({dataSourceArea:[],mStrCityId:value,mStrCity:this.state.dataSourceCity[index-1].label});
  //this.mLoaderShowHide();
  //this.getArea(value)
  }
 }




 mLoaderShowHide() {
  this.setState({
      isLoading: !this.state.isLoading
  });
};

mValidation(){
  if(this.state.mStrName.length<=0){
  alert('Please enter name')
  return false;
  }else if(this.state.mStrAddress1.length<=0){
  alert('Please enter address1')
  return false;
  }else if(this.state.mStrAddress2.length<=0){
  alert('Please enter address2')
  return false;
  }else if(this.state.mStrCountry.length<=0){
  alert('Please select country')
  return false;
  }else if(this.state.mStrState.length<=0){
  alert('Please select state')
  return false;
  }else if(this.state.mStrCity.length<=0){
  alert('Please enter city')
  return false;
  }else if(this.state.mStrZip.length<=0){
  alert('Please enter zip code')
  return false;
  }else if(this.state.mStrPhone.length<=0){
  alert('Please enter phone number')
  return false;
  }
  this.mLoaderShowHide();
  this.setAddress();
}

setAddress() {
  let form_data = new FormData();
  form_data.append("id",this.state.mStrAddressId)
  form_data.append("_token",this.state.mStrToken)
  form_data.append("name",this.state.mStrName)
  form_data.append("address1",this.state.mStrAddress1)
  form_data.append("address2",this.state.mStrAddress2)
  form_data.append("country",this.state.mStrCountryId)
  form_data.append("state",this.state.mStrStateId)
  form_data.append("zip",this.state.mStrZip)
  form_data.append("phone",this.state.mStrPhone)
  if(this.state.mStrCountry=='Philippines'){
  form_data.append("city",this.state.mStrCityId)
  form_data.append("barangay",this.state.mStrAresId)
  }else{
  form_data.append("city_name",this.state.mStrCity)
  }
  const requestOptions = {
  method: 'POST',
  body: form_data
  };
  fetch('https://beta.pedallion.com/api/addresses_setup', requestOptions)
  .then((response) => this.handleResponseAddress(response))
  .then(user => {
  });
  }
  handleResponseAddress(response) {
    response.text().then(text => {
        if (response.status==200) {
        const data = text && JSON.parse(text);
        this.mLoaderShowHide();
        if(data.status==1){
        alert('Address Update Successfully')
        this.props.navigation.goBack() 
        }else{
        //alert('Invalid Username or Password')
        }
        }else{
        }


      
   });
}



  render() {
      const placeholderC = {
        label: this.state.mStrCountry,
        value: this.state.mStrCountryId,
        color: '#000',
      };

      const placeholderS = {
        label: this.state.mStrState,
        value: this.state.mStrStateId,
        color: '#000',
      };

      const placeholderCity = {
        label: this.state.mStrCity,
        value: this.state.mStrCityId,
        color: '#000',
      };


      const placeholderArea = {
        label: this.state.mStrArea,
        value: this.state.mStrAresId,
        color: '#000',
      };

    return (
<SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity  onPress={ () => {    this.props.navigation.navigate('DrawerNavigator')   } }>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Cart')} }>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>


<KeyboardAvoidingView style={{  backgroundColor: '#f3f3f3',height: Dimensions.get('window').height,display: 'flex',width: '100%',}} behavior="padding" enabled>
       <ScrollView >
       <View style={{marginBottom:120}}>


       <View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>UPDATE ADDRESS</Text>
        </View>


<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrName}
                                    underlineColorAndroid="transparent"
                                    placeholder="User Name"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrName: text })}
                                    autoCapitalize="none"/>
                            </View>
                        </View>

<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrAddress1}
                                    underlineColorAndroid="transparent"
                                    placeholder="Address1"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrAddress1: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>

<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrAddress2}
                                    underlineColorAndroid="transparent"
                                    placeholder="Address2"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrAddress2: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>



<View style={{height: 40,width:width-30,marginLeft:15,marginRight:15,marginTop:15}}>
<RNPickerSelect
placeholder={placeholderC}
placeholderTextColor={'#989898'}
items={this.state.dataSource}
onValueChange={(value,index) => {this.mSearchState(value,index)}}
//onDonePress={() => {}}
style={pickerSelectStyles}
Icon={() => {
return <Chevron size={1} color="#000" />;
}}
/>
</View>



<View style={{height: 40,width:width-30,marginLeft:15,marginRight:15,marginTop:25}}>
<RNPickerSelect
placeholder={placeholderS}
placeholderTextColor={'#989898'}
items={this.state.dataSourceState}
onValueChange={(value,index) => {this.mSearchCity(value,index)}}
//onDonePress={() => {}}
style={pickerSelectStyles}
Icon={() => {
return <Chevron size={1} color="#000" />;
}}
/>
</View>

{
this.state.mStrCountry=='Philippines'  
?
<View>
<View style={{height: 40,width:width-30,marginLeft:15,marginRight:15,marginTop:25}}>
<RNPickerSelect
placeholder={placeholderCity}
placeholderTextColor={'#989898'}
items={this.state.dataSourceCity}
onValueChange={(value,index) => {this.mSearchArea(value,index)}}
//onDonePress={() => {}}
style={pickerSelectStyles}
Icon={() => {
return <Chevron size={1} color="#000" />;
}}
/>
</View>

<View style={{height: 40,width:width-30,marginLeft:15,marginRight:15,marginTop:25,marginBottom:10}}>
<RNPickerSelect
placeholder={placeholderArea}
placeholderTextColor={'#989898'}
items={this.state.dataSourceArea}
onValueChange={(value,index) => {this.mSelectArea(value,index)}}
//onDonePress={() => {this.mSearchArea()}}
style={pickerSelectStyles}
Icon={() => {
return <Chevron size={1} color="#000" />;
}}
/>
</View>
</View>
:
<View>

<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:25,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrCity}
                                    underlineColorAndroid="transparent"
                                    placeholder="City"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrCity: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>


</View>
}





                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrZip}
                                    underlineColorAndroid="transparent"
                                    placeholder="Zip Code"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrZip: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>



                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,marginBottom:25,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrPhone}
                                    underlineColorAndroid="transparent"
                                    placeholder="Phone Number"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrPhone: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>




















</View>

       </ScrollView>
       </KeyboardAvoidingView>


    

    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
    <TouchableOpacity  onPress={() => {this.mValidation()} }>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'SUBMIT'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>


{this.state.isLoading==true ? <View style={styles.overlay}>
                  <ActivityIndicator size="large" color="#f27029" />
                  </View> : <View></View>}



       </View>
      </SafeAreaView>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
       
        borderColor:'#E2E2E2',borderWidth:2,
        backgroundColor:'#ffffff',
        fontSize: 12,
        paddingVertical: 9,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: '#999999', // to ensure the text is never behind the icon
    },
    inputAndroid: {
      borderColor:'#E2E2E2',borderWidth:2,
      backgroundColor:'#ffffff',
      fontSize: 12,
      paddingVertical: 5,
      paddingHorizontal: 10,
      borderRadius: 4,
      color: '#999999', // to ensure the text is never behind the icon
    },
    iconContainer: {
      top: 20,
      right: 20,
    },
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
},
overlay:{
  flex: 1,
  position: 'absolute',
  left: 0,
  top: 0,
  opacity: 0.5,
  backgroundColor: 'black',
  height:'100%',
  width:'100%',
  justifyContent:'center',
  alignItems:'center'

},
});