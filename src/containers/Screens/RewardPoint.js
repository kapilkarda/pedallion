import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  ImageBackground,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';

import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import Header from "./Header";



const items = [
    {
        img1: require("../../components/Images/new11.png"),
    },
    {
        img1: require("../../components/Images/new6.png"),
    },
    {
        img1: require("../../components/Images/new2.png"),
    },
    {
        img1: require("../../components/Images/new4.png"),
    },
    {
        img1: require("../../components/Images/new5.png"),
    },
    {
        img1: require("../../components/Images/new6.png"),
    },
    {
        img1: require("../../components/Images/new77.png"),
    },
    {
        img1: require("../../components/Images/new8.png"),
    },
  ];

export default class RewardPoint extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >



<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>





<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center',justifyContent:'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b'}}>REWARD POINTS</Text>
</View>
</View>


<View>
                                <ImageBackground source={require('../../components/Images/roro.jpg')}
                                style={{width:'100%',height:hp('25%'),justifyContent:'center'}}>

<View style={{ borderRadius: 60,width: 120,height: 120,backgroundColor: '#F97B26',justifyContent:'center',alignSelf:'center'}}>
<View style={{borderRadius: 50,width: 100,height: 100,margin: 10,backgroundColor: '#ffffff',justifyContent:'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b',alignSelf:'center'}}>10</Text>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b',alignSelf:'center'}}>Points</Text>

</View>
</View>
                                </ImageBackground>
                                </View>
                                <FlatList
                                 data={items}
                                 showsVerticalScrollIndicator={true}
                                 renderItem={({ item }) =>
                                 
                                 <View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('1.5%'),width:width-15,marginTop:wp('1.5%'),marginLeft:8,marginRight:8}}>
                                 <Text style={{ color:'#F97B26',fontSize:RF(2),fontWeight:'500'}}>5 Points</Text>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing</Text>
                                 <Text style={{ color:'#838181',fontSize:RF(1.8),marginTop:3}}>Added Date : Sep 20, 2019</Text>
                                 <Text style={{ color:'#838181',fontSize:RF(1.8),marginTop:3}}>Expiry Date : Sep 20, 2019</Text>
 </View>
                                 }
                             />
    

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});