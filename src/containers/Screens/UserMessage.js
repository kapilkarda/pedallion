import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from "react-native-linear-gradient";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RF from "react-native-responsive-fontsize";
import { Dialog } from 'react-native-simple-dialogs';

const akki = [
    {
        img1: require("../../components/Images/circle.png"),
    },
    {
        img1: require("../../components/Images/tms.png"),
    },
    {
        img1: require("../../components/Images/happy.png"),
    },
    {
        img1: require("../../components/Images/spaces.png"),
    },
    {
        img1: require("../../components/Images/spaces.png"),
    },
  ];

  const items  = [
    {
        txt: 'Home',
    },
    {
        txt: 'Messages',
    },
    {
        txt: 'Upload Product',
    },
    {
        txt: 'Share and Earn',
    },
    {
        txt: 'Favorites',
    },
    {
        txt: 'My Account',
    },
    {
        txt: 'Reward Points',
    },
    {
        txt: 'My Orders',
    },
    {
        txt: 'My Downloads',
    },
    {
        txt: 'My Wallet',
    },
    {
        txt: 'Return Requests',
    },
    {
        txt: 'Cancellation Requests',
    },
    {
        txt: 'Contact Us',
    },
    {
        txt: 'FAQ',
    },
    {
        txt: 'Privacy Policy',
    },
  ];

const sports = [
    {
      label: 'Football',
      value: 'football',
    },
    {
      label: 'Baseball',
      value: 'baseball',
    },
    {
      label: 'Hockey',
      value: 'hockey',
    },
  ];

export default class UserMessage extends Component{




  static navigationOptions = {
    header: null
}
constructor(props) {
    super(props);
    this.state = {
        dialogVisible:false
    };
  }

secureKey() {
    this.setState({dialogVisible: !this.state.dialogVisible})
    
}

goToScreen(index) {
    if(index==0){
    this.props.navigation.navigate('DrawerNavigator')  
    this.secureKey();  
    }else if(index==1){
    this.props.navigation.navigate('UserMessage') 
    this.secureKey(); 
    }else if(index==2){
    this.props.navigation.navigate('ProductImageUpload') 
    this.secureKey();
    }else if(index==3){
    this.secureKey();
    }else if(index==111){
    this.props.navigation.navigate('Login') 
    this.secureKey();
    }else if (index==11) {
    this.props.navigation.navigate('MessageDetail') 
    this.secureKey();
    }else{
    this.secureKey();
    }


       
        
    }

  render() {

    
    return (

<SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={() => {this.secureKey()} }>
                                            <Image source={require('./../../components/Images/drawer.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>

       <View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#f3f3f3' }}>
        <ScrollView>
        <View style={{marginTop: wp('2.2%'),marginLeft:10}}>
                            <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>MESSAGES</Text>
                        </View>

      
        
 
        <FlatList
                                 data={akki}
                                 showsVerticalScrollIndicator={true}
                                 renderItem={({ item,index }) =>
<TouchableOpacity
                                  onPress={() => this.props.navigation.navigate('MessageDetail')}
                                >
                                 {
                                index<2 ? 
                               
                                 <View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2.2%'),marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('20%'),width:wp('20%')}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('20%'),
                                                 height:wp('20%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginRight:15}}>Revv Amplification G4 Distortion</Text>
 <Text style={{ color:'#fe5328',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>{'\u20B1'} 11900.00</Text>
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>Placed On 14th April 2019</Text>
 

  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>
 :

                                 <View style={{backgroundColor:'#E3E3E3',padding:8,marginBottom:wp('2.2%'),marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('20%'),width:wp('20%')}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('20%'),
                                                 height:wp('20%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginRight:15}}>Revv Amplification G4 Distortion</Text>
 <Text style={{ color:'#fe5328',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>{'\u20B1'} 11900.00</Text>
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>Placed On 14th April 2019</Text>
 

  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>

 
  }
  </TouchableOpacity>
                             
                                 }
                             /> 
 
        </ScrollView>
        </View>



                               
        <Dialog
    style={{height:height-250}}  
    visible={this.state.dialogVisible}
    onTouchOutside={() => this.setState({dialogVisible: false})} >
    <View style={{alignItems:'center'}}>
    <Image 
    source={require('../../components/Images/icon8.png')}  
    style={{width: hp('14%'), height: hp('14%'), borderRadius: hp('14%')/ 2,marginTop:-hp('7%')}}/>
    <Text style={{fontSize:hp('2.5%'),marginTop:hp('2%')}}>{'Welcome'}</Text>
    <Text style={{fontSize:hp('3.5%'),marginTop:hp('1%'),fontWeight:'700'}}>{'Akhilesh Pathak'}</Text>
    <TouchableOpacity  onPress={() => {this.goToScreen(111)} }>
    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width:hp('12%'),height:hp('6%'),borderRadius:5,alignItems:'center',justifyContent:'center',marginTop:hp('1.5%')}}>
    <View>
    <Text style={{fontSize:hp('1.9%'),color:'#fff',fontWeight:'bold'}}>{'LOGOUT'}</Text>
    </View>
    </LinearGradient>
    </TouchableOpacity>

                               <FlatList
                                style={{width:'100%',marginTop:hp('2%'),height:height/2}} 
                                data={items}
                                renderItem={({item,index}) =>
                                <TouchableOpacity  onPress={() => {this.goToScreen(index)} }>
                              
                                {
                                index%2 == 0 ?
                                <View style={{backgroundColor:'#f3f4f5',width:'100%',height:hp('6%'),alignItems:'center',justifyContent:'center'}}>
    <Text style={{fontSize:hp('1.9%'),color:'#000',fontWeight:'bold'}}>{item.txt}</Text>
    </View>
                                :
                                <View style={{backgroundColor:'#ffffff',width:'100%',height:hp('6%'),alignItems:'center',justifyContent:'center'}}>
    <Text style={{fontSize:hp('1.9%'),color:'#000',fontWeight:'bold'}}>{item.txt}</Text>
    </View>
                                }


                                
                          
                                </TouchableOpacity>
                                }
                            />

 
    </View>
</Dialog> 


     


       </View>
      </SafeAreaView>
    );
  }
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        backgroundColor:'#f3f4f5',
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        fontSize: 18,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#f3f4f5',
        borderRadius: 4,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        backgroundColor:'#cacaca',
        marginLeft:20,
        marginRight:20,
        fontSize: 18,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#cacaca',
        borderRadius: 4,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});