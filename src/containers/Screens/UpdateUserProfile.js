import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ActivityIndicator
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RNPickerSelect from 'react-native-picker-select';
import { Chevron } from 'react-native-shapes';
import SyncStorage from 'sync-storage';




export default class UpdateUserProfile extends Component{


  static navigationOptions = {
    header: null
}


constructor(props) {
    super(props);
    this.state = {
    mStrImg:'',
    mStrAddressId:'',
    mStrName:'',
    mStrCountry:'',
    mStrCountryId:'',
    mStrState:'',
    mStrStateId:'',
    mStrCity:'',
    mStrPhone:'',
    isLoading:false,
    dataSource:[],
    dataSourceState:[],
    dataSourceCity:[],
    mStrToken:'',
    };
  }

  

  getProfile(token) {
    let form_data = new FormData();
    form_data.append(" _token",token)
     const requestOptions = {
     method: 'POST',
     body: form_data
     };
     fetch('https://beta.pedallion.com/api/profile_info', requestOptions)
     .then((response) => this.handleResponseProfile(response))
     .then(user => {
     });
     }

     handleResponseProfile(response) {
      response.text().then(text => {
          if (response.status==200) {
          const data = text && JSON.parse(text);
          //this.mLoaderShowHide();
          this.setState({
             mStrName: data.name,
             mStrPhone: data.phone,
             mStrCity: data.city_town,
             mStrState: data.state,
             mStrStateId: data.state_id,
             mStrCountry: data.country,
             mStrCountryId: data.country_id,
             mStrImg:data.user_image_url
         });
          }else{
          //this.mLoaderShowHide();
          }
     });
 }
  

  componentWillMount() {
    const result =  SyncStorage.get('token');
    this.setState({
        mStrToken:result
    });
    this.mLoaderShowHide();
    this.getProfile(result);
    this.getCountry();
}

   getCountry() {
   const requestOptions = {
   method: 'GET',
   };
   fetch('https://beta.pedallion.com/api/countries', requestOptions)
   .then((response) => this.handleResponseCountry(response))
   .then(user => {
   });
   }


  


 






   getState(countryId) {
    let form_data = new FormData();
    form_data.append("country_id",countryId)
    const requestOptions = {
    method: 'POST',
    body: form_data
    };
    fetch('https://beta.pedallion.com/api/states', requestOptions)
    .then((response) => this.handleResponseState(response))
    .then(user => {
    });
    }


    handleResponseState(response) {
      var rawData = [];
      response.text().then(text => {
          if (response.status==200) {
          const data = text && JSON.parse(text);
          this.mLoaderShowHide();
          if(data.status==1){
            for (var i = 0; i < data.states.length; i++){
              rawData.push({
                label: data.states[i].name,
                value: data.states[i].id,
              })
              }
              this.setState({
                dataSourceState: [...this.state.dataSourceState, ...rawData],
              });
          }else{
          //alert('Invalid Username or Password')
          }
          }else{
          }
  
  
        
     });
  }




   handleResponseCountry(response) {
    var rawData = [];
    response.text().then(text => {
        if (response.status==200) {
        const data = text && JSON.parse(text);
        this.mLoaderShowHide();
        if(data.status==1){
          for (var i = 0; i < data.countries.length; i++){
            rawData.push({
              label: data.countries[i].name,
              value: data.countries[i].id,
            })
            }
            this.setState({
              dataSource: [...this.state.dataSource, ...rawData],
            });
        }else{
        //alert('Invalid Username or Password')
        }
        }else{
        }
   });
}



  mSearchState(value,index) {
  console.log("<><><> "+value)
  if(value!=0){
  if(value==this.state.mStrCountryId){
    this.mLoaderShowHide();
    this.getState(value);
  }else{
    this.setState({dataSourceState:[],mStrCountryId:value,mStrCountry:this.state.dataSource[index-1].label});  
    this.mLoaderShowHide();
    this.getState(value);
  }
  }  
  //this.props.navigation.navigate('ProductDetailConfirm')
 }

 mSearchCity(value,index){
 if(value !=0){
 if(value==this.state.mStrStateId){
 }else{
 this.setState({mStrStateId:value,mStrState:this.state.dataSourceState[index-1].label});
 }
}
}

 mLoaderShowHide() {
  this.setState({
      isLoading: !this.state.isLoading
  });
};

mValidation(){
  if(this.state.mStrName.length<=0){
  alert('Please enter name')
  return false;
  }else if(this.state.mStrCountry.length<=0){
  alert('Please select country')
  return false;
  }else if(this.state.mStrState.length<=0){
  alert('Please select state')
  return false;
  }else if(this.state.mStrCity.length<=0){
  alert('Please enter city')
  return false;
  }else if(this.state.mStrPhone.length<=0){
  alert('Please enter phone number')
  return false;
  }
  this.mLoaderShowHide();
  this.setAddress();
}

setAddress() {
console.log("<><><>mStrToken  "+this.state.mStrToken)
console.log("<><><>mStrName  "+this.state.mStrName)
console.log("<><><>mStrCountryId  "+this.state.mStrCountryId)
console.log("<><><>mStrStateId  "+this.state.mStrStateId)
console.log("<><><>mStrPhone  "+this.state.mStrPhone)
console.log("<><><>mStrCity  "+this.state.mStrCity)

  let form_data = new FormData();
  form_data.append("_token",this.state.mStrToken)
  form_data.append("name",this.state.mStrName)
  form_data.append("country_id",this.state.mStrCountryId)
  form_data.append("state_id",this.state.mStrStateId)
  form_data.append("phone",this.state.mStrPhone)
  form_data.append("city",this.state.mStrCity)
  const requestOptions = {
  method: 'POST',
  body: form_data
  };
  fetch('http://beta.pedallion.com/api/update_profile_info', requestOptions)
  .then((response) => this.handleResponseAddress(response))
  .then(user => {
  });
  }

  handleResponseAddress(response) {
    response.text().then(text => {
        if (response.status==200) {
        const data = text && JSON.parse(text);
        console.log("<><><>JSON  "+text)
        this.mLoaderShowHide();
        if(data.status==1){
        alert('Profile Update Successfully')
        this.props.navigation.goBack() 
        }else{
        alert(data.msg)
        }
        }else{
        }


      
   });
}



  render() {
      const placeholderC = {
        label: this.state.mStrCountry,
        value: this.state.mStrCountryId,
        color: '#000',
      };

      const placeholderS = {
        label: this.state.mStrState,
        value: this.state.mStrStateId,
        color: '#000',
      };

    return (
<SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity  onPress={ () => {    this.props.navigation.navigate('DrawerNavigator')   } }>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Cart')} }>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>


<KeyboardAvoidingView style={{  backgroundColor: '#f3f3f3',height: Dimensions.get('window').height,display: 'flex',width: '100%',}} behavior="padding" enabled>
       <ScrollView >
       <View style={{marginBottom:120}}>


       <View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>UPDATE PROFILE</Text>
        </View>


<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrName}
                                    underlineColorAndroid="transparent"
                                    placeholder="Name"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrName: text })}
                                    autoCapitalize="none"/>
                            </View>
                        </View>



<View style={{height: 40,width:width-30,marginLeft:15,marginRight:15,marginTop:15}}>
<RNPickerSelect
placeholder={placeholderC}
placeholderTextColor={'#989898'}
items={this.state.dataSource}
onValueChange={(value,index) => {this.mSearchState(value,index)}}
//onDonePress={() => {}}
style={pickerSelectStyles}
Icon={() => {
return <Chevron size={1} color="#000" />;
}}
/>
</View>



<View style={{height: 40,width:width-30,marginLeft:15,marginRight:15,marginTop:25}}>
<RNPickerSelect
placeholder={placeholderS}
placeholderTextColor={'#989898'}
items={this.state.dataSourceState}
onValueChange={(value,index) => {this.mSearchCity(value,index)}}
//onDonePress={() => {}}
style={pickerSelectStyles}
Icon={() => {
return <Chevron size={1} color="#000" />;
}}
/>
</View>

<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:25,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrCity}
                                    underlineColorAndroid="transparent"
                                    placeholder="City"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrCity: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>

                       


                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,marginBottom:25,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:16}}
                                    value={this.state.mStrPhone}
                                    underlineColorAndroid="transparent"
                                    placeholder="Phone Number"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrPhone: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>




















</View>

       </ScrollView>
       </KeyboardAvoidingView>


    

    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
    <TouchableOpacity  onPress={() => {this.mValidation()} }>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'SUBMIT'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>


{this.state.isLoading==true ? <View style={styles.overlay}>
                  <ActivityIndicator size="large" color="#f27029" />
                  </View> : <View></View>}



       </View>
      </SafeAreaView>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
       
        borderColor:'#E2E2E2',borderWidth:2,
        backgroundColor:'#ffffff',
        fontSize: 12,
        paddingVertical: 9,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: '#999999', // to ensure the text is never behind the icon
    },
    inputAndroid: {
      borderColor:'#E2E2E2',borderWidth:2,
      backgroundColor:'#ffffff',
      fontSize: 12,
      paddingVertical: 5,
      paddingHorizontal: 10,
      borderRadius: 4,
      color: '#999999', // to ensure the text is never behind the icon
    },
    iconContainer: {
      top: 20,
      right: 20,
    },
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
},
overlay:{
  flex: 1,
  position: 'absolute',
  left: 0,
  top: 0,
  opacity: 0.5,
  backgroundColor: 'black',
  height:'100%',
  width:'100%',
  justifyContent:'center',
  alignItems:'center'

},
});