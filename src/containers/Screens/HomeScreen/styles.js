import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RF from "react-native-responsive-fontsize";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';

export const COLOR = {
    background: "#e6e6e6",
    BLACK: "#2b2b2b",
    GRAY: "#9A9A9A",
    WHITE: "#FFF",
    LIGHT_ORANGE: "#E69151",
    DARK_ORANGE: "#fe5328",

};

export default StyleSheet.create({

    container: {
        backgroundColor: COLOR.background
    },
    SubMain: {
        width: width - 30,
        alignSelf: 'center',
        marginTop: 10,
    },
    paginationStyleItem: {
        height: 3,
        width: 15
    },
    paginationStyle: {
        height: 10,
    },
    LinearGradient: {
        height: hp('5%'),
        width: wp('28%'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        marginLeft: hp('3.5%'),
        marginTop:hp('10%') 
    },
    SliderImageBackground: {
        width:width - hp('3%'),
        height:hp('24%'),
        justifyContent: 'center'
    },
    sliderBtnText: {
        color: COLOR.WHITE,
        fontSize: hp('1.9%'),
        fontWeight: 'bold'
    },
    tabheadingView: {
        alignSelf: 'center',
        marginTop: 15,
        marginBottom:15
    },
    tabheadingText: {
        fontWeight: 'bold',
        color: COLOR.BLACK
    },
    futureProductListView: {
        height: hp('18%'),
        width: hp('18%'),
        marginLeft:5,
        marginRight:5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLOR.BLACK,
        borderRadius: 4
    },
    futureProductListViewImageBackground: {
        height: hp('18%'),
        width: hp('18%'),
        alignItems: 'center',
        borderRadius: 4
    },
    futureProductListViewImage: {
        width: 50,
        height: 50,
        marginTop: 20
    },
    latestblogView: {
        marginBottom: 30,
        marginLeft:10,
        marginRight:10
    },
    latestblogListView: {
        backgroundColor: COLOR.WHITE,
        height: hp('33%'),
        width: wp('50%'),
        borderRadius: 3,
        marginLeft: 5,
        marginRight: 5
    },
    latestblogImageBackground: {
        width: wp('50%'),
        height: hp('17%'),
        borderRadius: 3
    },
    latestblogTitleView: {
        width: width / 2 - 40,
        alignSelf: 'center',
        marginTop: 15
    },
    latestblogTitleText: {
        color: COLOR.DARK_ORANGE,
        fontWeight: 'bold',
        fontSize: RF(2),
        backgroundColor: COLOR.WHITE
    },
    latestblogDateView: {
        marginTop: 5,
        marginLeft: 18
    },
    latestblogDateText: {
        color: COLOR.BLACK,
        fontSize: RF(1.8)
    },
    feturproductView:{
marginLeft:10,
marginRight:10
    },
    scrollText:{
        color:'#fff', fontSize: RF(2),marginTop:10,fontWeight:'600'
    },
});