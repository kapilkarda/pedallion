import React, { Component } from 'react';
import {
  View, Text,Image,ImageBackground, TouchableOpacity,
  ScrollView,FlatList
} from "react-native";
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import SwiperFlatList from 'react-native-swiper-flatlist';





const items = [
    {
        imgbg: require("../../../components/Images/gitar.jpg"),
        img1: require("../../../components/Images/guitar.png"),
        txt:'Electric Guitar'
    },
    {
        imgbg: require("../../../components/Images/gitar.jpg"),
        img1: require("../../../components/Images/amplifier.png"),
        txt:'Amplifier'
    },
    {
        imgbg: require("../../../components/Images/gitar.jpg"),
        img1: require("../../../components/Images/guitar.png"),
        txt:'Effect Pedal'
    },
    
  ];



const datas = [
  {
      img1: require("../../../components/Images/icon8.png"),
  },
  {
      img1: require("../../../components/Images/icon8.png"),
  },
  {
      img1: require("../../../components/Images/icon8.png"),
  },
  {
      img1: require("../../../components/Images/icon8.png"),
  },
];

export default class Home extends Component{

  

  render() {
    return (
      <View style={styles.container} >
       <ScrollView>
                        <View style={styles.SubMain}>
                            <SwiperFlatList
                                autoplay
                                autoplayDelay={2}
                                autoplayLoop
                                index={2}
                                showPagination
                                paginationStyleItem={styles.paginationStyleItem}
                                paginationActiveColor="#ff582b"
                                paginationStyle={styles.paginationStyle}>
                                <View>
                                    <ImageBackground source={require('../../../components/Images/homepage1.png')}
                                        style={styles.SliderImageBackground}>
                                        <TouchableOpacity>
                                            <LinearGradient style={styles.LinearGradient}
                                            start={{x: 0,y: 0}}
                                            end={{x: 0,y: 1}}
                                            colors={['#feae3d','#ff8333','#ff572b',]}>
                                            <Text style={styles.sliderBtnText}>
                                            SHOP NOW
                                            </Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </ImageBackground>
                                </View>
                                <View>
                                <ImageBackground source={require('../../../components/Images/guiter.jpg')}
                                style={styles.SliderImageBackground}>
                                </ImageBackground>
                                </View>
                                <View style={{}}>
                                    <ImageBackground source={require('../../../components/Images/homepage1.png')}
                                        style={styles.SliderImageBackground}>
                                    </ImageBackground>
                                </View>
                                <View style={{}}>
                                    <ImageBackground source={require('../../../components/Images/guiter.jpg')}
                                        style={styles.SliderImageBackground}>
                                    </ImageBackground>
                                </View>
                            </SwiperFlatList>
                        </View>
                        <View style={styles.tabheadingView}>
                        <Text style={styles.tabheadingText}>FEATURED PRODUCTS</Text>
                        </View>
                        <View style={styles.feturproductView}>

                        <FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList')}>
                        <View style={styles.futureProductListView}>
                        <ImageBackground source={item.imgbg}
                        style={styles.futureProductListViewImageBackground}>
                        <Image source={item.img1} style={styles.futureProductListViewImage}>
                        </Image>
                        <Text style={styles.scrollText}>{item.txt}</Text>
                        </ImageBackground>
                        </View>
                        </TouchableOpacity>
                        }/>

                        </View>

                        <View style={styles.tabheadingView}>
                        <Text style={styles.tabheadingText}>LATEST BLOG</Text>
                        </View>
                        <View style={styles.latestblogView}>
                        <FlatList
                        data={datas}
                        horizontal={true}
                        renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList')}>
                        <View style={styles.latestblogListView}>

                        <ImageBackground source={item.img1}
                        style={styles.latestblogImageBackground}>
                        </ImageBackground>

                        <View style={styles.latestblogTitleView}>
                        <Text style={styles.latestblogTitleText}>
                        HOW TO REPLACE YOUR STRINGS:A BEGINEER'S GUIDE
                        </Text>
                        </View>

                        <View style={styles.latestblogDateView}>
                        <Text style={styles.latestblogDateText}>12 Mar 2019</Text>
                        </View>

                        </View>
                                            
                        </TouchableOpacity>
                        }/>
                           
                        </View>
                    </ScrollView>
      </View>
    );
  }
}
