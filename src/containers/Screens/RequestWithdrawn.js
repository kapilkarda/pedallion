import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ActivityIndicator
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import SyncStorage from 'sync-storage';



export default class RequestWithdrawn extends Component{


  static navigationOptions = {
    header: null
}


constructor(props) {
    super(props);
    this.state = {
      mStrAcHolderName:'',
      mStrBankName:'',
      mStrAcNumber:'',
      mStrIFSC:'',
      mStrBankAddress:'',
      mStrToken:'',
    };
  }


  componentWillMount() {
    const { navigation } = this.props;
    const result =  SyncStorage.get('token');
    const mStrBanName = navigation.getParam('mStrBankName', 'Bank Name');  
    const mStrAcholderNam = navigation.getParam('mStrAcholderName', 'Account Holder Name'); 
    const mStrBankAddres = navigation.getParam('mStrBankAddress', 'Bank Address');  
    const mStrAccountNumber = navigation.getParam('mStrAccountNumber', 'Account Number');
    const mStrBankIfsc = navigation.getParam('mStrBankIfsc', 'IFSC Code'); 
    this.setState({
      mStrToken:result,
      mStrAcHolderName:mStrAcholderNam,
      mStrBankName:mStrBanName,
      mStrAcNumber:mStrAccountNumber,
      mStrIFSC:mStrBankIfsc,
      mStrBankAddress:mStrBankAddres,
  });
  
}

  

  


   








  



  




 mLoaderShowHide() {
  this.setState({
      isLoading: !this.state.isLoading
  });
};




mValidation(){
  if(this.state.mStrAcHolderName=='Account Holder Name' || this.state.mStrAcHolderName.length<=0){
  alert('Please enter bank account holder name')
  return false;
  }else if(this.state.mStrAcNumber=='Account Number' || this.state.mStrAcNumber.length<=0){
  alert('Please enter bank account number')
  return false;
  }else if(this.state.mStrIFSC=='IFSC Code' || this.state.mStrIFSC.length<=0){
  alert('Please enter IFSC code')
  return false;
  }else if(this.state.mStrBankName=='Bank Name' || this.state.mStrBankName.length<=0){
  alert('Please enter bank name')
  return false;
  }else if(this.state.mStrBankAddress=='Bank Address' || this.state.mStrBankAddress.length<=0){
  alert('Please enter bank address')
  return false;
  }
  this.mLoaderShowHide();
  this.setAddress();
}

setAddress() {
  let form_data = new FormData();
  form_data.append("_token",this.state.mStrToken)
  form_data.append("ub_bank_name",this.state.mStrBankName)
  form_data.append("ub_account_holder_name",this.state.mStrAcHolderName)
  form_data.append("ub_account_number",this.state.mStrAcNumber)
  form_data.append("ub_ifsc_swift_code",this.state.mStrIFSC)
  form_data.append("ub_bank_address",this.state.mStrBankAddress)
  const requestOptions = {
  method: 'POST',
  body: form_data
  };
  fetch('https://beta.pedallion.com/api/save_bank_info', requestOptions)
  .then((response) => this.handleResponseAddress(response))
  .then(user => {
  });
  }
  handleResponseAddress(response) {
    response.text().then(text => {
        if (response.status==200) {
        const data = text && JSON.parse(text);
        this.mLoaderShowHide();
        if(data.status==1){
        alert('Bank Detail Update Successfully')
        }else{
        //alert('Invalid Username or Password')
        }
        }else{
        }


      
   });
}



  render() {
  return (
<SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity  onPress={ () => {    this.props.navigation.navigate('DrawerNavigator')   } }>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Cart')} }>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>

<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flex:1,alignItems: 'center',justifyContent:'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b'}}>WITHDRAWAL REQUEST</Text>
<Text style={{fontSize: hp('2%'), fontWeight: 'bold', color: '#2b2b2b'}}>Balance : {'\u20B1'}20122.00</Text>
</View>
</View>


<KeyboardAvoidingView style={{  backgroundColor: '#f3f3f3',height: Dimensions.get('window').height,display: 'flex',width: '100%',}} behavior="padding" enabled>
       <ScrollView >
       <View>
     


<View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:18}}
                                    
                                    underlineColorAndroid="transparent"
                                    placeholder="Account Holder Name"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrAcHolderName: text })}
                                    autoCapitalize="none"/>
                            </View>
                        </View>


                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:18}}
                                    
                                    underlineColorAndroid="transparent"
                                    placeholder="Account Number"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrAcNumber: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>

                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:18}}
                                   
                                    underlineColorAndroid="transparent"
                                    placeholder="IFSC Code"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrIFSC: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>


                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:18}}
                                    
                                    underlineColorAndroid="transparent"
                                    placeholder="Bank Name"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrBankName: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>




                      

                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:18}}
                                    
                                    underlineColorAndroid="transparent"
                                    placeholder="Bank Address"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrBankAddress: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>

                        <View style={{height: 55,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 55,fontSize:18}}
                                   
                                    underlineColorAndroid="transparent"
                                    placeholder="Other Info"
                                    placeholderTextColor="#989898"
                                    onChangeText={(text) => this.setState({mStrBankAddress: text })} 
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>

       </ScrollView>
       </KeyboardAvoidingView>


    

    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
    <TouchableOpacity>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'SUBMIT'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>


{this.state.isLoading==true ? <View style={{flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        height:'100%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'}}>
                  <ActivityIndicator size="large" color="#bd4c04" />
                  </View> : <View></View>}

       </View>
      </SafeAreaView>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
       
        borderColor:'#E2E2E2',borderWidth:2,
        backgroundColor:'#ffffff',
        fontSize: 12,
        paddingVertical: 9,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: '#999999', // to ensure the text is never behind the icon
    },
    inputAndroid: {
        backgroundColor:'#cacaca',
        marginLeft:20,
        marginRight:20,
        fontSize: 18,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#cacaca',
        borderRadius: 4,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    iconContainer: {
      top: 15,
      right: 20,
    },
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});