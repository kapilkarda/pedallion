import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RF from "react-native-responsive-fontsize";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';

export const COLOR = {
    background: "#e6e6e6",
    BLACK: "#2b2b2b",
    GRAY: "#9A9A9A",
    WHITE: "#FFF",
    LIGHT_ORANGE: "#E69151",
    DARK_ORANGE: "#fe5328",

};

export default StyleSheet.create({

    container: {
        backgroundColor: COLOR.background
    },
    SubMain: {
    },
    listViewMain: {
        marginTop: wp('2.2%'), alignSelf: 'center'
    },
    ImageBackground: {
        width: wp('70%'), height: wp('70%'), borderRadius: 3
    },
    tabheadingView: {
        alignSelf: 'center', marginTop: 20
    },
    tabheadingText: {
        fontWeight: 'bold', color: COLOR.BLACK,fontSize:hp('3%')
    },
});