import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';




export default class Header extends Component {


  

    render() {

        return (

            <View>
                <LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
                    <View style={{ flexDirection: 'row', height: hp('8%') }}>
                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('8%') }}>
                            <TouchableOpacity onPress={this.props.mGoBack}>
                                <Image source={require('./../../components/Images/icback.png')}
                                    style={{
                                        width: hp('4%'),
                                        height: hp('4%'),
                                    }}>
                                </Image>
                            </TouchableOpacity>


                            <TouchableOpacity onPress={this.props.mGoHome}>
                                <Image source={require('./../../components/Images/ichome.png')}
                                    style={{
                                        width: hp('4%'),
                                        height: hp('4%'),
                                        marginLeft: hp('2%'),
                                    }}>
                                </Image>
                            </TouchableOpacity>

                        </View>

                        <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center', height: hp('8%') }}>

                            <Image source={require('./../../components/Images/body.png')}
                                style={{
                                    width: hp('10%'),
                                    height: hp('4%'),

                                    // tintColor: '#737373',
                                }}>
                            </Image>

                        </View>


                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('8%') }}>

                            <TouchableOpacity onPress={this.props.mGoNotification}>
                                <Image source={require('./../../components/Images/notification.png')}
                                    style={{
                                        width: hp('4%'),
                                        height: hp('4%'),
                                        marginRight: hp('3%'),
                                    }}>
                                </Image>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.props.mGoCart}>
                                <Image source={require('./../../components/Images/cart.png')}
                                    style={{ width: hp('4%'), height: hp('4%'), marginRight: hp('3%') }}>
                                </Image>
                            </TouchableOpacity>

                        </View>




                    </View>
                </LinearGradient>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textinput1: {
        color: '#000',
        marginLeft: 10,
        fontSize: RF(2.3), marginTop: 2
    },
    row: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 12
    }
});