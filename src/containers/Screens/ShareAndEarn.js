import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ImageBackground
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from "./Header";
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-banner-carousel';
import { Dialog } from 'react-native-simple-dialogs';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const BannerHeight = 260;

const images = [
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png"
];
const STAR_IMAGE = require('../../components/Images/ratstar.png')

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ShareAndEarn extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        dialogVisible:false
    };
  }

  secureKey() {
    this.props.navigation.navigate('ProductUploadOne')
    
}

renderPage(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: hp('25%'), height: hp('25%'),alignSelf:'center',marginTop:hp('5%'),marginBottom:hp('5%')}} source={{ uri: image }} />
        </View>
    );
}

renderPage2(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: 180, height: 180 }} source={{ uri: image }} />
        </View>
    );
}


  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>


<View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#F3F3F3' }}>
       <ScrollView>
       <View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>SHARE AND EARN</Text>
        </View>

        <View style={{ alignSelf: 'center', marginTop: wp('3%'),}}>
        <Text style={{ fontWeight: '700', color: '#5c5c5c',fontSize:hp('2.1%')}}>Want more offers for less?</Text>
        </View>

        <Text style={{color: '#838383',marginLeft:25,marginRight:25,textAlign: 'center',fontStyle:'italic', fontSize:hp('2%'),alignSelf:'center',marginTop:15,marginBottom:15}}>Start sharing your unique URL on Facebook, Twitter, Linkedin, Blog, Email and ask friends to sign up and purchase. You and your friend may get reward benefits on signup and making purchase with us, which can be used against item purchase.</Text>




        

        <View style={{alignSelf:'center',marginTop:10,marginBottom:10,width:hp('25%'),height:hp('25%')}}>
        <Image source={require('./../../components/Images/peopleimg.png')}
        style={{ width: hp('25%'), height: hp('25%'),alignSelf:'center'}}></Image>
        </View>

        <Text style={{color: '#2b2b2b',marginLeft:25,marginRight:25,textAlign: 'center',fontStyle:'italic', fontSize:hp('2%'),alignSelf:'center',marginTop:10,marginBottom:10,fontWeight:'700'}}>You may copy invitation link below and share to your friends on social channels Facebook, Twitter, Linkedin, Blog, Email etc.</Text>




<View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2.2%'),marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
<Text style={{ color:'#fe5328',fontSize:RF(2),}}>https://www.pedallion.com/home/referral/5dff584ffrt</Text>
</View>




</ScrollView>
</View>
<LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),position: 'absolute',bottom: 0,flexDirection: 'row'}} >

<View style={{flex:1,justifyContent: 'center',alignItems: 'center'}}>
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'COPY LINK'}</Text>
</View> 

<View style={{flex:1,justifyContent: 'center',alignItems: 'center'}}>
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'SHARE'}</Text>
</View> 


</LinearGradient>

</View>


  
       
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});