import React, { Component } from 'react';
import {
  View, Text, StatusBar, StyleSheet, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
  ScrollView, KeyboardAvoidingView, FlatList
} from "react-native";
import { Rating, AirbnbRating } from 'react-native-ratings';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];

export default class Shopes extends Component {
  render() {
    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#e6e6e6' }}>
        <ScrollView>
        <View style={{ alignSelf: 'center', marginTop: wp('2.2%')}}>
                            <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>SHOPS</Text>
                        </View>

      
        
 
        <FlatList
                                 data={items}
                                 showsVerticalScrollIndicator={true}
                                 renderItem={({ item }) =>
                                 
                                 <View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2.2%'),marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('22%'),width:wp('22%')}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('22%'),
                                                 height:wp('22%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginRight:15}}>New Gear Day</Text>
 <Text style={{ color:'#000',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15}}>Location : <Text style={{color:'#fe5328'}}>Metro Manila, Philippines</Text></Text>
 <View style={{marginTop:5,flexDirection:'row',marginLeft:10}}>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
</View>
<TouchableOpacity onPress={() => this.props.navigation.navigate('ShopDetail')} >
<Text style={{ color:'#fe5328',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15,textDecorationLine: 'underline',}}>Visit Shop</Text>
</TouchableOpacity>


  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>
                                 }
                             /> 
 
        </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});
