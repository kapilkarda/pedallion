import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from "./Header";


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class PrivacyPolicy extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>

<ScrollView>

<View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>PRIVACY POLICY</Text>
        </View>

    <Text style={{fontSize:hp('2%'),color:'#2b2b2b',marginLeft:10,marginRight:10,marginTop:20}}>

    Welcome to Pedallion (the "Site”). This Privacy Policy sets out how Pedallion collects, retains, uses and disclosure of information through Pedallion (the "Site”)., and any other websites, features, applications, widgets or online services that are owned or controlled by us, including personally identifiable{"\n"}{"\n"}

 

<Text style={{fontWeight:'bold'}}>User Consent.</Text>{"\n"}{"\n"}

 

By accessing, using and submitting Personal Information through our Site or Services, you expressly consent to the collection, use and disclosure of your Personal Information in accordance with this Privacy Policy. If you do not agree to this Privacy Policy, you must not use the Site.{"\n"}{"\n"}

 

Information We Collect. We may collect, store and use from Users (Buyers, Sellers & Affiliates) when you visit our website pages or register on our site ("Account”), place an order, use our services and our tools, subscribe to a newsletter, fill out a form, correspondence, open a support ticket or enter information on our site. The following types of information:{"\n"}{"\n"}

 

a.Personal Information: is data that allows someone to identify or contact you, including, for examples name, email address, phone number, mobile telephone number, physical address, password and (depending on the service used); as well as any other non-public information about you that is associated with or linked to any of the foregoing data.{"\n"}{"\n"}

 

b.Payment Information: based on your activities on the Site (such as bidding, buying, selling, item and content you generate or that relates to your Account) to make or receive payments, we will also collect certain payment information, such as bank account numbers, credit card, PayPal or other financial account information and billing address.{"\n"}{"\n"}

 

c.Identity Verification: We may collect Personal Information, such as your date of birth, or taxpayer identification number, to validate your identity or as may be required by law, such as to complete tax filings. We may request documents to verify this information, such as a copy of your government-issued identification or photo or a billing statement, if we believe you are violating the Site policies{"\n"}{"\n"}

 

d.Non-Identifying Information/Usernames: We also may collect other information including but not limited to device ID, zip codes, location, geo-location, ad data, IP address and standard web log information, information regarding your use of the Service, and general project-related data ("Non-Identifying Information”). We may aggregate information collected from our registered and non-registered Users. We consider usernames to be Non-Identifying Information. Usernames are made public through the Service and are viewable by other our Users.{"\n"}{"\n"}

 

e.Combination of Personal and Non-Identifying Information: We may combine your Personal Information with Non-Identifying Information (for example, combining information with your name), where we do so, we will treat the combined information as Personal Information. If the combination may be used to readily identify or locate you in the same manner as Personal Information alone.{"\n"}{"\n"}{"\n"}{"\n"}

 

<Text style={{fontWeight:'bold'}}>Information collected from Third Parties</Text>{"\n"}{"\n"}

 

We also may receive information about you from third parties feature our services or promotional offers. You may also choose to participate in a third party application or social media site through which you allow us to collect (or the third party to share) information about you, including Usage Information and Personal Information. If we combine such data with information with information we collect through the Site or Services, such information will be treated as combined information as described in this Privacy Policy and Cookie Notice unless we have disclosed otherwise.{"\n"}{"\n"}{"\n"}{"\n"}

 
<Text style={{fontWeight:'bold'}}>Information Collected Automatically</Text>{"\n"}{"\n"}


 

We and our third party service providers, including analytics and third party content providers, may automatically collect certain information when you visit or interact with our websites, services, mobile applications or other products, including:{"\n"}{"\n"}

 

a.Internet Protocol ("IP") address and Internet Service Provider ("ISP"){"\n"}{"\n"}

 

b.Computer, device and connection information, such as browser and operating system information, mobile platform{"\n"}{"\n"}

 

c.Unique device identifier ("UDID”) for any device (computer, mobile phone, tablet, etc.) and other technical identifiers;{"\n"}{"\n"}

 

d.Uniform Resource Locator ("URL") click stream data, including date and time, and content you viewed or searched for on a Service;{"\n"}{"\n"}

 

e.Geographic location information for location-aware Services to provide you with more relevant content for where you are in the world.{"\n"}{"\n"}

    </Text>

    </ScrollView>
      

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});