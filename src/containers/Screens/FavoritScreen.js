import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import Header from "./Header";
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RBSheet from "react-native-raw-bottom-sheet";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
    {
        img1: require("../../components/Images/new11.png"),
    },
    {
        img1: require("../../components/Images/new6.png"),
    },
    {
        img1: require("../../components/Images/new2.png"),
    },
    {
        img1: require("../../components/Images/new4.png"),
    },
    {
        img1: require("../../components/Images/new5.png"),
    },
    {
        img1: require("../../components/Images/new6.png"),
    },
    {
        img1: require("../../components/Images/new77.png"),
    },
    {
        img1: require("../../components/Images/new8.png"),
    },
  ];


export default class FavoritScreen extends Component{


  static navigationOptions = {
    header: null
  }

constructor(props) {
    super(props);
    this.state = {
       isViewItem:true,
       isViewShop:false,
    };
  }

  callShop(){
    this.setState({
        isViewShop: true,
        isViewItem:false,
    })
  }

  callItem(){
    this.setState({
        isViewShop: false,
        isViewItem:true,
    })  
  }

  render() {
      const YourOwnComponent = () => 

<View style={{marginLeft:15,alignItems:'flex-start',alignSelf:'flex-start'}}>
<Text style={{ color: '#ddd',fontSize: RF(3),marginTop:10 }}>
                                                       SORT BY
                                                       </Text>
                                                        <Text style={{ color: '#000', fontWeight: 'bold', fontSize: RF(2.8),marginTop:10 }}>
                                                        Most Recent
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>
                                                       Featured
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>
                                                      Bestselling
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>
                                                       Price (Low to High)
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>Price (High to Low)
                                                       </Text>
                                                    </View>;
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#ddd'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>


<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b' }}>MY FAVORITE</Text>
</View>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<Text style={{fontSize: hp('2.5%'),color: '#2b2b2b' }}>Filter</Text>
<Image source={require('./../../components/Images/filter.png')}
style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:5}}>
</Image>
</View>
</View>


<View style={{flexDirection: 'row',width:width-20,marginLeft:10,height:hp('5%'),backgroundColor:'#fff',marginTop:8}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity  onPress={() => {this.callItem()} }>
    {
    this.state.isViewItem==true ? 
    <Text style={{fontSize: hp('2.5%'),fontWeight:'bold',color: '#F97B26' }}>Items</Text>
    :
    <Text style={{fontSize: hp('2.5%'),color: '#2b2b2b' }}>Items</Text>
    }
</TouchableOpacity>
</View>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity  onPress={() => {this.callShop()} }>
{this.state.isViewShop==true ? 
<Text style={{fontSize: hp('2.5%'),fontWeight:'bold',color: '#F97B26' }}>Shops</Text>
:
<Text style={{fontSize: hp('2.5%'),color: '#2b2b2b' }}>Shops</Text>
}
</TouchableOpacity>
</View>
</View>


{this.state.isViewItem==true ? 
<View>
<View style={{width:width-10,marginLeft:5,backgroundColor:'#fff',marginTop:8}}>
<Text style={{fontSize: hp('2.5%'),color: '#2b2b2b',marginTop:5,marginLeft:5}}>My List Name <Text style={{color:'#fe5328'}}>(8 Products)</Text></Text>
<FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item }) =>
                        <View style={{ height: hp('16%'),width: hp('16%'),margin:5,justifyContent: 'center',alignItems: 'center',borderRadius: 4}}>
                        <Image source={item.img1} style={{height: hp('16%'),width: hp('16%')}}>
                        </Image>
                        </View>
                        }/>


</View>


<View style={{width:width-10,marginLeft:5,backgroundColor:'#fff',marginTop:20}}>
<Text style={{fontSize: hp('2.5%'),color: '#2b2b2b',marginTop:5,marginLeft:5}}>My List Name <Text style={{color:'#fe5328'}}>(8 Products)</Text></Text>
<FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item }) =>
                        <View style={{ height: hp('16%'),width: hp('16%'),margin:5,justifyContent: 'center',alignItems: 'center',borderRadius: 4}}>
                        <Image source={item.img1} style={{height: hp('16%'),width: hp('16%')}}>
                        </Image>
                        </View>
                        }/>


</View>
</View> : <View></View>}

{this.state.isViewShop==true ? <View>
    <FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                renderItem={({ item }) =>
                                <View style={{backgroundColor:'#fff',padding:8,margin:wp('2%')}}>
     
                                <View>
                            <View style={{flexDirection: 'row',width:width}}>
                            <View style={{height:wp('22%'),width:wp('22%')}}>
                            <Image source={require('./../../components/Images/new1.png')}
                                                                        style={{
                                                                            width:wp('22%'),
                                                                            height:wp('22%'),
                                                                        }}>
                                                                    </Image>
                            </View>
                            <View>
                            
                            <Text style={{ color:'#000',fontSize:RF(3),fontWeight:'bold',marginLeft:10}}>New Gear Day</Text>
                            <View style={{flexDirection:'row'}}>
                            <View style={{width:130,marginLeft:10,backgroundColor:'#ff572b',borderRadius:5,alignItems:'center',justifyContent:'center',marginTop:5,padding:6}}>
                            <Text style={{fontSize:14,color:'#fff',fontWeight:'bold'}}>{'Check It Out'}</Text>
                            </View>
                            <Text style={{alignSelf:'center',color:'#000',fontSize:RF(2),marginLeft:5}}>10,000 Products</Text>
                            </View>
                            
                            
                            </View>
                            
                            
                            
                            
                            
                            
                            </View>
                            </View>
                            
                            </View>
                                }
                            />
</View> : <View></View>}




    

{this.state.isViewItem==true ? 
<LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
    <TouchableOpacity>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'Create New List'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>
: <View></View>
}


       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});