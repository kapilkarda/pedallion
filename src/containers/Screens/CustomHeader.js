import React, { Component } from 'react';
import { View, Platform, Dimensions, Image,FlatList,TouchableOpacity, TextInput, Text,SafeAreaView } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right,Tab, Tabs } from 'native-base';
import { DrawerActions } from 'react-navigation';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { Dialog } from 'react-native-simple-dialogs';
import SyncStorage from 'sync-storage';



const items = [
    {
        txt: 'Home',
    },
    {
        txt: 'Messages',
    },
    {
        txt: 'Upload Product',
    },
    {
        txt: 'Share and Earn',
    },
    {
        txt: 'Favorites',
    },
    {
        txt: 'My Account',
    },
    {
        txt: 'Reward Points',
    },
    {
        txt: 'My Orders',
    },
    {
        txt: 'My Downloads',
    },
    {
        txt: 'My Wallet',
    },
    {
        txt: 'Return Requests',
    },
    {
        txt: 'Cancellation Requests',
    },
    {
        txt: 'Contact Us',
    },
    {
        txt: 'FAQ',
    },
    {
        txt: 'Privacy Policy',
    },
  ];



export default class CustomHeader extends Component{
    
    constructor(props) {
        super(props);
        this.state = {
            dialogVisible:false,
            userName:'',
            imgUrl:'',
        };
      }

      secureKey() {
        this.setState({dialogVisible: !this.state.dialogVisible})
        
    }

    componentWillMount() {
        const imgUrls =  SyncStorage.get('user_image_url');
        const userNames =  SyncStorage.get('user_name');
        this.setState({
            userName: userNames,
            imgUrl:imgUrls
        });
    }

    goToScreen(index) {
    if(index==0){
    this.props.navigation.navigate('DrawerNavigator')  
    this.secureKey();  
    }else if(index==1){
    this.props.navigation.navigate('UserMessage') 
    this.secureKey(); 
    }else if(index==2){
    this.props.navigation.navigate('ProductImageUpload') 
    this.secureKey();
    }else if(index==3){
    this.props.navigation.navigate('ShareAndEarn') 
    this.secureKey();
    }else if(index==4){
    this.props.navigation.navigate('FavoritScreen') 
    this.secureKey();
    }else if(index==5){
    this.props.navigation.navigate('MyAccount') 
    this.secureKey();
    }else if(index==6){
    this.props.navigation.navigate('RewardPoint') 
    this.secureKey();
    }else if(index==7){
    this.props.navigation.navigate('MyOrders') 
    this.secureKey();
    }else if(index==9){
    this.props.navigation.navigate('MyWallet') 
    this.secureKey();
    }else if(index==10){
    this.props.navigation.navigate('MessageDetail') 
    this.secureKey();
    }else if(index==111){
    this.props.navigation.navigate('Login') 
    this.secureKey();
    }else if (index==11) {
    this.props.navigation.navigate('MessageDetail') 
    this.secureKey();
    }else if (index==12) {
    this.props.navigation.navigate('ContactUs') 
    this.secureKey();
    }else if (index==13) {
    this.props.navigation.navigate('FAQ') 
    this.secureKey();
    }else if (index==14) {
    this.props.navigation.navigate('PrivacyPolicy') 
    this.secureKey();
    }else{
    this.secureKey();
    }
    }



    render() {
      return (
        <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']}>
        <View style={{height:95}} >
            <View style={{flexDirection:'row',height:60}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:60}}>
<TouchableOpacity  onPress={() => {this.secureKey()} }>
                                            <Image source={require('./../../components/Images/drawer.png')}
                                            style={{
                                                width: 25,
                                                height: 25,
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


</View>

<View style={{flex:3,justifyContent: 'center', alignItems: 'center',height:60}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: 80,
                                            height: 30,

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:60}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: 25,
                                                height: 25,
                                                marginRight:20,
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Cart')} }>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: 25, height: 25,marginRight:40,}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
<View style={{height: 35,width: width - 40,backgroundColor: '#fff',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10}}>
                            <View style={{}}>
                                <TextInput style={{width: width - 95, height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Search Here"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                            <TouchableOpacity>
                                <View>
                                    <Image source={require("../../components/Images/searchheader.png")}
                                        style={{ width: 18, height:18,marginTop:8.5 }}>
                                    </Image>
                                </View>
                            </TouchableOpacity>
                        </View>








      

      <Dialog
    style={{height:height/1.5}}  
    visible={this.state.dialogVisible}
    onTouchOutside={() => this.setState({dialogVisible: false})} >
    <View style={{alignItems:'center'}}>
    <TouchableOpacity  onPress={() => {this.goToScreen(5)} }>
    <Image 
    source={{uri: this.state.imgUrl}}
    style={{width: 120, height: 120, borderRadius: 120/ 2,marginTop:-40}} 
/>
</TouchableOpacity>
    <Text style={{fontSize:20,marginTop:20}}>{'Welcome'}</Text>
    <Text style={{fontSize:25,marginTop:10,fontWeight:'bold'}}>{this.state.userName}</Text>
    <View style={{width:100,height:40,backgroundColor:'#ff572b',borderRadius:5,alignItems:'center',justifyContent:'center',marginTop:15}}>
    <Text style={{fontSize:18,color:'#fff',fontWeight:'bold'}}>{'LOGOUT'}</Text>
    </View>
                               <View style={{height:height/2,width:'100%',marginTop:20}}>
                               <FlatList
                                data={items}
                                renderItem={({item,index}) =>
                                <TouchableOpacity  onPress={() => {this.goToScreen(index)} }>
                                {
                                index%2 == 0 ?
                                <View style={{backgroundColor:'#f3f4f5',width:'100%',height:45,alignItems:'center',justifyContent:'center'}}>
    <Text style={{fontSize:18,color:'#000',fontWeight:'bold'}}>{item.txt}</Text>
    </View>
                                :
                                <View style={{backgroundColor:'#ffffff',width:'100%',height:45,alignItems:'center',justifyContent:'center'}}>
    <Text style={{fontSize:18,color:'#000',fontWeight:'bold'}}>{item.txt}</Text>
    </View>
                                }


                                
                          
                                </TouchableOpacity>
                                }
                            />
                               </View>
                            

 
    </View>
</Dialog> 
</View>
</LinearGradient>

      );
    }
  }
  
