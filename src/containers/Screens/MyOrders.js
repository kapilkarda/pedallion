import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity, Dimensions,ScrollView,SafeAreaView,
} from 'react-native';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from "./Header";
var width = Dimensions.get('window').width; //full width



const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class MyOrders extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        a:false,
        b:false,
    };
  }

  updateFuntionA(){
    this.setState({
        a:!this.state.a,
        b:false,
       
    })
}
updateFuntionB(){
    this.setState({
        a:false,
        b:!this.state.b,
    })
}


  render() {
     


    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#E6E6E6'}}>
<View style={{flex:1}} >


<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>


<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff',padding:12}}>
<View style={{flexDirection: 'row',flex:1, alignItems: 'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b',marginLeft:wp('1.5%')}}>MY ORDERS</Text>
</View>
<View style={{flexDirection: 'row',flex:1, alignItems: 'center',justifyContent:'flex-end',marginRight:wp('1.5%')}}>
<Text style={{fontSize: hp('2.5%'),color: '#2b2b2b' }}>Filter</Text>
<Image source={require('./../../components/Images/filter.png')}
style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:5}}>
</Image>
</View>
</View>

<ScrollView>
<View>
<View style={{backgroundColor:'#fff',margin:wp('1.5%')}}>
<View style={{backgroundColor:'#fff',padding:12}}>
<View style={{flexDirection: 'row'}}>
<View style={{flex:2}}>
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Order No</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Order Date</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Payment Methode</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Retail Price</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>#</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Ship-on-Time SLA</Text> 
</View>

<View style={{flex:0.2}}>
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 

</View>

<View style={{flex:2}}>
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>326531210</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>SEP 24,2019</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>NA</Text> 
<Text style={{color:'#fe5328',fontSize:RF(2.3),fontWeight:'800'}}>{'\u20B1'} 12,900.00</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>#337</Text> 
<Text style={{color:'#22C536',fontSize:RF(2.3),fontWeight:'800'}}>Blackarrow</Text> 
</View>

</View>
</View>

<View style={{height:1,marginTop:5,marginBottom:5,backgroundColor:'#ebebeb',justifyContent:'center',alignItems:'center'}}>
{this.state.a==true
    ?  
<TouchableOpacity  onPress={ () => { this.updateFuntionA() } }>
<Image source={require('./../../components/Images/cross.png')}
style={{width:15,height:15,alignSelf:'center'}}></Image>
</TouchableOpacity>
:<View></View>
}


</View>
<View style={{backgroundColor:'#fff',paddingLeft:12,paddingRight:12,paddingTop:12,margin:wp('1.5%')}}>


{this.state.a==false
    ?
<View style={{flexDirection: 'row',justifyContent:'center',alignItems:'center'}}>
<TouchableOpacity  onPress={ () => { this.updateFuntionA() } }>
<Text style={{color:'#000',fontSize:RF(2.1),alignSelf:'center',fontWeight:'500'}}>More Information</Text>
</TouchableOpacity>
<Image source={require('./../../components/Images/down.png')}
style={{marginLeft:8,width:13,height:13,alignSelf:'center'}}></Image>
</View>
:
<View></View>
}



{this.state.a==true
?
<View style={{marginTop:8}}>
<View style={{flexDirection: 'row',width:width}}>
<View style={{height:wp('24%'),width:wp('23%')}}>
<Image source={require('./../../components/Images/new1.png')}
                                            style={{
                                                width:wp('23%'),
                                                height:wp('24%'),
                                            }}>
                                        </Image>
</View>
<View>

<Text style={{ color:'#000',fontSize:RF(2.2),marginLeft:5}}>Revv Amplification G4 Distortion</Text>
<View style={{flexDirection: 'row',marginLeft:5}}>
<View style={{flex:1.7}}>
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>Seller</Text> 
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>Ship-on-Time SLA</Text> 
</View>

<View style={{flex:0.2}}>
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>:</Text> 
</View>

<View style={{flex:1.4}}>
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>Beta Seller</Text> 
<Text style={{color:'#22C536',fontSize:RF(2),fontWeight:'800'}}>Blackarrow</Text> 
</View>

</View>
<View style={{width:100,marginLeft:5,backgroundColor:'#B4B4B4',borderRadius:5,alignItems:'center',justifyContent:'center',marginTop:3,padding:4}}>
<Text style={{fontSize:14,color:'#fff',fontWeight:'bold'}}>{'In Process'}</Text>
</View>


</View>







</View>
<View style={{flexDirection: 'row',marginTop:8}}>

<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800'}}>Send To</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800',marginLeft:3}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),marginLeft:3}}>Test Address</Text> 


</View>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>Quezon Street, Santo Niño Tukuran, Philippines</Text>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>T:25326554</Text>


<View style={{flexDirection: 'row',marginTop:10}}>

<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800'}}>Shipping To</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800',marginLeft:3}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),marginLeft:3}}>Test Address</Text> 


</View>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>Quezon Street, Santo Niño Tukuran, Philippines</Text>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>T:25326554</Text>


</View>
:
<View></View>
}




     
                            
                            
                            
                            </View>
                            <View style={{backgroundColor:'#ff572b',height:40,marginTop:10,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{alignSelf:'center',fontWeight:'800',color:'#fff',fontSize:RF(2.5)}}>Ready To Ship</Text>
                            </View>
                            </View>

                            <View style={{backgroundColor:'#fff',margin:wp('1.5%')}}>
<View style={{backgroundColor:'#fff',padding:12}}>
<View style={{flexDirection: 'row'}}>
<View style={{flex:2}}>
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Order No</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Order Date</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Payment Methode</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Retail Price</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>#</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>Ship-on-Time SLA</Text> 
</View>

<View style={{flex:0.2}}>
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>:</Text> 

</View>

<View style={{flex:2}}>
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>326531210</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>SEP 24,2019</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>NA</Text> 
<Text style={{color:'#fe5328',fontSize:RF(2.3),fontWeight:'800'}}>{'\u20B1'} 12,900.00</Text> 
<Text style={{color:'#000',fontSize:RF(2.3),fontWeight:'800'}}>#337</Text> 
<Text style={{color:'#22C536',fontSize:RF(2.3),fontWeight:'800'}}>Blackarrow</Text> 
</View>

</View>
</View>

<View style={{height:1,marginTop:5,marginBottom:5,backgroundColor:'#ebebeb',justifyContent:'center',alignItems:'center'}}>
{this.state.b==true
    ?  
<TouchableOpacity  onPress={ () => { this.updateFuntionB() } }>
<Image source={require('./../../components/Images/cross.png')}
style={{width:15,height:15,alignSelf:'center'}}></Image>
</TouchableOpacity>
:<View></View>
}


</View>
<View style={{backgroundColor:'#fff',paddingLeft:12,paddingRight:12,paddingTop:12,margin:wp('1.5%')}}>


{this.state.b==false
    ?
<View style={{flexDirection: 'row',justifyContent:'center',alignItems:'center'}}>
<TouchableOpacity  onPress={ () => { this.updateFuntionB() } }>
<Text style={{color:'#000',fontSize:RF(2.1),alignSelf:'center',fontWeight:'500'}}>More Information</Text>
</TouchableOpacity>
<Image source={require('./../../components/Images/down.png')}
style={{marginLeft:8,width:13,height:13,alignSelf:'center'}}></Image>
</View>
:
<View></View>
}



{this.state.b==true
?
<View style={{marginTop:8}}>
<View style={{flexDirection: 'row',width:width}}>
<View style={{height:wp('24%'),width:wp('23%')}}>
<Image source={require('./../../components/Images/new1.png')}
                                            style={{
                                                width:wp('23%'),
                                                height:wp('24%'),
                                            }}>
                                        </Image>
</View>
<View>

<Text style={{ color:'#000',fontSize:RF(2.2),marginLeft:5}}>Revv Amplification G4 Distortion</Text>
<View style={{flexDirection: 'row',marginLeft:5}}>
<View style={{flex:1.7}}>
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>Seller</Text> 
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>Ship-on-Time SLA</Text> 
</View>

<View style={{flex:0.2}}>
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>:</Text> 
</View>

<View style={{flex:1.4}}>
<Text style={{color:'#000',fontSize:RF(2),fontWeight:'800'}}>Beta Seller</Text> 
<Text style={{color:'#22C536',fontSize:RF(2),fontWeight:'800'}}>Blackarrow</Text> 
</View>

</View>
<View style={{width:100,marginLeft:5,backgroundColor:'#B4B4B4',borderRadius:5,alignItems:'center',justifyContent:'center',marginTop:3,padding:4}}>
<Text style={{fontSize:14,color:'#fff',fontWeight:'bold'}}>{'In Process'}</Text>
</View>


</View>







</View>
<View style={{flexDirection: 'row',marginTop:8}}>

<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800'}}>Send To</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800',marginLeft:3}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),marginLeft:3}}>Test Address</Text> 


</View>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>Quezon Street, Santo Niño Tukuran, Philippines</Text>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>T:25326554</Text>


<View style={{flexDirection: 'row',marginTop:10}}>

<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800'}}>Shipping To</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),fontWeight:'800',marginLeft:3}}>:</Text> 
<Text style={{color:'#000',fontSize:RF(2.2),marginLeft:3}}>Test Address</Text> 


</View>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>Quezon Street, Santo Niño Tukuran, Philippines</Text>
<Text style={{ color:'#000',fontSize:RF(2.2)}}>T:25326554</Text>


</View>
:
<View></View>
}




     
                            
                            
                            
                            </View>
                            <View style={{backgroundColor:'#ff572b',height:40,marginTop:10,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{alignSelf:'center',fontWeight:'800',color:'#fff',fontSize:RF(2.5)}}>Ready To Ship</Text>
                            </View>
                            </View>





</View>


</ScrollView>





       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});