import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ActivityIndicator
} from 'react-native';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
import SyncStorage from 'sync-storage';
import Header from "./Header";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class MyAccount extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        mStrName: '',
        mStrPhone: '',
        mStrCity: '',
        mStrState: '',
        mStrCountry: '',
        mStrImg:'',
        mStrAddName:'',
        mStrAddOne:'',
        mStrAddTwo:'',
        mStrAddCity:'',
        mStrAddState:'',
        mStrAddZip:'',
        mStrBankName:'',
        mStrBankIfsc:'',
        mStrToken:'',
        mStrAcholderName:'',
        mStrAccountNumber:'',
        mStrBankAddress:'',
        secureKey: true,
        isLoading:false,
        dataAddress:[],
    };
}



componentWillMount(){
     this.props.navigation.addListener('didFocus', () => {
     this.LoadData();
     //Put your Data loading function here instead of my this.LoadData()
    });}

   LoadData(){
    const result =  SyncStorage.get('token');
    this.setState({
        mStrToken:result
    });
    console.log("<><><> "+result);
    this.mLoaderShowHide();
    this.getProfile(result);
    this.getBankAdd(result);
    this.getAddress(result);
   }



mEditAddFun(mStrId,mStrName,mStrAddOne,mStrAddTwo,mStrCountry,mStrState,mStrCity,mStrZip,mStrPhone,mStrCountryId,mStrStateId,mStrCityId,mAreaId,mAreaName){

    this.props.navigation.navigate('UpdateUserAddress', {  
        id: mStrId,
        name: mStrName,
        address1: mStrAddOne,
        address2: mStrAddTwo,
        countryName: mStrCountry,
        stateName: mStrState,  
        cityName: mStrCity,
        zipCode: mStrZip,
        phoneNum: mStrPhone,  
        countryId:mStrCountryId,
        stateId:mStrStateId,
        cityId:mStrCityId,
        mArName:mAreaName,
        mArId:mAreaId
    })  
}


 mEditBankFun(){
     
 this.props.navigation.navigate('UpdateBankDetail', {  
        mStrBankName: this.state.mStrBankName,
        mStrAcholderName: this.state.mStrAcholderName,
        mStrBankAddress: this.state.mStrBankAddress,
        mStrAccountNumber: this.state.mStrAccountNumber,
        mStrBankIfsc: this.state.mStrBankIfsc,
    })
}


    mDeleteFun(mStrId){
    this.mLoaderShowHide();
    let form_data = new FormData();
    form_data.append("_token",this.state.mStrToken)
    form_data.append("id",mStrId)
    const requestOptions = {
    method: 'POST',
    body: form_data
    };
    fetch('https://beta.pedallion.com/api/delete_addresses', requestOptions)
    .then((response) => this.handleResponse(response))
    .then(user => {
    });
    }


    handleResponse(response) {
        response.text().then(text => {
            if (response.status==200) {
            const data = text && JSON.parse(text);
            console.log("<><> "+text)
            this.mLoaderShowHide();
            if(data.status==1){
            alert('Address deleted')
            this.mLoaderShowHide();
            this.getAddress(this.state.mStrToken);
            }else{
            alert('Something went wrong')
            }
            }else{
            alert('Something went wrong')
            }
       });
    }







mLoaderShowHide() {
    this.setState({
        isLoading: !this.state.isLoading
    });
  };

getProfile(token) {
    let form_data = new FormData();
    form_data.append(" _token",token)
     const requestOptions = {
     method: 'POST',
     body: form_data
     };
     fetch('https://beta.pedallion.com/api/profile_info', requestOptions)
     .then((response) => this.handleResponseProfile(response))
     .then(user => {
     });
     }

     getBankAdd(token) {
        let form_data = new FormData();
        form_data.append(" _token",token)
         const requestOptions = {
         method: 'POST',
         body: form_data
         };
         fetch('https://beta.pedallion.com/api/bank_info', requestOptions)
         .then((response) => this.handleResponseBank(response))
         .then(user => {
         });
         }



     getAddress(token) {
        console.log("<><><>T "+token);
        let form_data = new FormData();
        form_data.append(" _token",token)
         const requestOptions = {
         method: 'POST',
         body: form_data
         };
         fetch('https://beta.pedallion.com/api/addresses', requestOptions)
         .then((response) => this.handleResponseAddress(response))
         .then(user => {
         });
         }


         handleResponseAddress(response) {
            var rawData = [];
            this.setState({dataAddress:[]});  
            response.text().then(text => {
                if (response.status==200) {
                const data = text && JSON.parse(text);
                console.log("<><><> "+data);  
                this.mLoaderShowHide();
                if(data.status==1){
                for (var i = 0; i < data.addresses.length; i++){
                 
                rawData.push(data.addresses[i])
                }
                this.setState({
                dataAddress: [...this.state.dataAddress, ...rawData],
                });
                }else{
                //alert('Invalid Username or Password')
                }
                }else{
                    this.mLoaderShowHide();
                }
           });
       }

       handleResponseBank(response) {
        response.text().then(text => {
            if (response.status==200) {
            const data = text && JSON.parse(text);
           
            if(data.status==1){
             this.setState({
                mStrBankName:data.bank_details.ub_bank_name,
                mStrBankIfsc:data.bank_details.ub_ifsc_swift_code,
                mStrAcholderName:data.bank_details.ub_account_holder_name,
                mStrAccountNumber:data.bank_details.ub_account_number,
                mStrBankAddress:data.bank_details.ub_bank_address,
                });

            }else{
            //alert('Invalid Username or Password')
            }
            }else{
         
            }


          
       });
   }
   

    


    handleResponseProfile(response) {
     response.text().then(text => {
         if (response.status==200) {
         const data = text && JSON.parse(text);
         //this.mLoaderShowHide();
         this.setState({
            mStrName: data.name,
            mStrPhone: data.phone,
            mStrCity: data.city_town,
            mStrState: data.state,
            mStrCountry: data.country,
            mStrImg:data.user_image_url
        });
         }else{
         //this.mLoaderShowHide();
         }


       
    });
}




  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#ddd'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>



<View style={{flexDirection: 'row',width:width,height:hp('20%'),backgroundColor:'#000',padding:10}}>
 <View style={{flexDirection: 'row',width:width,marginTop:wp('2.2%')}}>
 <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}}>
 <Image source={{uri: this.state.mStrImg}}
                                             style={{
                                                 width:wp('25%'),
                                                 height:wp('25%'),
                                                 borderRadius:wp('25%')/2
                                             }}>
                                         </Image>
 </View>


 <View style={{flex:2,justifyContent: 'center'}}>
 <Text style={{color:'#fff',fontSize:RF(2.5),fontWeight:'bold'}}>{this.state.mStrName}</Text>
 <Text style={{color:'#fff',fontSize:RF(2),marginTop:2}}>{"Ph. No. : "+this.state.mStrPhone}</Text>
 <Text style={{color:'#fff',fontSize:RF(2),marginTop:2}}>{"City : "+this.state.mStrCity}</Text>
 <Text style={{color:'#fff',fontSize:RF(2),marginTop:2}}>{"State : "+this.state.mStrState}</Text>
 <Text style={{color:'#fff',fontSize:RF(2),marginTop:2}}>{"Country : "+this.state.mStrCountry}</Text>

</View>

</View>



</View>
<TouchableOpacity  onPress={ () => {    this.props.navigation.navigate('UpdateUserProfile')   } }>
<Image source={require('./../../components/Images/edit-circule.png')}
style={{ width: hp('4%'), height: hp('4%'),marginTop:-hp('2%'),alignSelf:'flex-end',marginRight:40}}></Image>
</TouchableOpacity>



<ScrollView>

<View style={{flexDirection:'row',padding:8,backgroundColor:'#fff',marginTop:10,marginLeft:10,marginRight:10}}>
<View style={{flexDirection: 'row',flex:4}}>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600'}}>ADDRESS</Text>
</View>

<View style={{flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity  onPress={ () => {    this.props.navigation.navigate('AddUserAddress')   } }>
<Image source={require('./../../components/Images/plus.png')}
style={{ width: hp('2.5%'), height: hp('2.5%')}}>
</Image>
</TouchableOpacity>
</View>




</View>


                                <FlatList
                                data={this.state.dataAddress}
                                showsVerticalScrollIndicator={true}
                                renderItem={({ item }) =>

                                <View style={{flexDirection:'row',padding:8,backgroundColor:'#fff',marginTop:10,marginLeft:10,marginRight:10,borderRadius:4}}>
<View style={{flex:4}}>
<Text style={{color:'#2b2b2b',fontSize:RF(2.5),fontWeight:'bold'}}>{item.name}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2}}>{item.address1}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2}}>{item.address2}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2}}>{item.city_name+", "+item.state_name+", "+item.country_name+", "+item.zip}</Text>
</View>



<View style={{flex:1,justifyContent: 'center', alignItems: 'center',flexDirection:'column'}}>
<TouchableOpacity onPress={() => this.mEditAddFun(item.id,item.name,item.address1,item.address2,item.country_name,item.state_name,item.city_name,item.zip,item.phone,item.country_id,item.state_id,item.city,item.barangay_id,item.barangay_name)}>
<Image source={require('./../../components/Images/edit.png')}
style={{ width: hp('3.5%'), height: hp('3.5%')}}>
</Image>
</TouchableOpacity>

<TouchableOpacity onPress={() => this.mDeleteFun(item.id)}>
<Image source={require('./../../components/Images/trash.png')}
style={{ width: hp('3.5%'), height: hp('3.5%'),marginTop:10}}>
</Image>
</TouchableOpacity>

</View>




</View>
                                    
                                }
                            />






<View style={{flexDirection:'row',padding:8,backgroundColor:'#fff',marginTop:10,marginLeft:10,marginRight:10}}>
<View style={{flexDirection: 'row',flex:4}}>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600'}}>BANK DETAIL</Text>
</View>
</View>


<View style={{flexDirection:'row',padding:8,backgroundColor:'#fff',marginTop:10,marginLeft:10,marginRight:10,marginBottom:10,borderRadius:4}}>
<View style={{flex:4}}>
<Text style={{color:'#2b2b2b',fontSize:RF(2.5),fontWeight:'bold'}}>{this.state.mStrBankName}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2,fontWeight:'700'}}>{this.state.mStrAcholderName}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2}}>{this.state.mStrBankAddress}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2}}>{this.state.mStrAccountNumber}</Text>
 <Text style={{color:'#2b2b2b',fontSize:RF(2),marginTop:2}}>{this.state.mStrBankIfsc}</Text>
 </View>

<View style={{flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity onPress={() => this.mEditBankFun()}>
<Image source={require('./../../components/Images/edit.png')}
style={{ width: hp('3.5%'), height: hp('3.5%')}}>
</Image>
</TouchableOpacity>
</View>




</View>
</ScrollView>
      {this.state.isLoading==true ? <View style={styles.overlay}>
                  <ActivityIndicator size="large" color="#bd4c04" />
                  </View> : <View></View>}

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
overlay:{
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0.5,
    backgroundColor: 'black',
    height:'100%',
    width:'100%',
    justifyContent:'center',
    alignItems:'center'

},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});