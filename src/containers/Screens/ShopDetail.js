import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
  {
      img1: require("../../components/Images/productw.png"),
      txt:'Products'
  },
  {
      img1: require("../../components/Images/starwhite.png"),
      txt:'Reviews'
  },
  {
      img1: require("../../components/Images/docs.png"),
      txt:'Policies'
  },
  {
    img1: require("../../components/Images/message.png"),
    txt:'Message'
},
  
];



export default class ShopDetail extends Component{


  static navigationOptions = {
    header: null
}

_onPressButton(index) {
    if(index=='0'){
        this.props.navigation.navigate('ShopProductList')
        
    }else if(index=='1'){
        this.props.navigation.navigate('ShopReviews')
        
    }else if(index=='2'){
        this.props.navigation.navigate('ShopPolicies')
        
    }else if(index=='3'){
        this.props.navigation.navigate('ShopMessage')
        
    }
    
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#e6e6e6'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                marginLeft:hp('3%'),
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>



</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>
<View style={{height: 45,width:width,backgroundColor:'#F97B26'}}>
 <View style={{height: 35,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('12%'),height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Search Here"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                            <TouchableOpacity>
                                <View>
                                    <Image source={require("../../components/Images/searchheader.png")}
                                        style={{ width: 18, height:18,marginTop:8.5 }}>
                                    </Image>
                                </View>
                            </TouchableOpacity>
                        </View>

</View>

<ScrollView>
<View style={{flexDirection: 'row',width:width,height:hp('6%'),backgroundColor:'#F97B26'}}>
</View>

<View style={{width:width,height:hp('18%'),marginTop:-hp('3%'),alignSelf:'center',marginLeft:20,marginRight:20}}>
<Image source={require("../../components/Images/mapi.png")} style={{width:width-40,height:hp('18%'),marginLeft:20,marginRight:20,borderRadius:4}}/>
</View>

<View style={{justifyContent: 'center', alignItems: 'center',marginTop:-hp('7%'),marginBottom:30}}>

<Image source={require("../../components/Images/new11.png")} style={{width:hp('13%'),height:hp('13%'),borderColor:'#fff',borderRadius:4,borderWidth:4}}/>

<Text style={{ color:'#000000',fontSize: hp('4%'),fontWeight:'bold',marginTop:10}}>New Gear Day</Text>
<Text style={{ color:'#7f7f7f',fontSize: hp('2.5%'),marginTop:10}}>Metro Manila, Phili</Text>
<View style={{marginTop:10,flexDirection:'row'}}>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: hp('3%'), height: hp('3%')}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: hp('3%'), height: hp('3%'),marginLeft:3}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: hp('3%'), height: hp('3%'),marginLeft:3}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: hp('3%'), height: hp('3%'),marginLeft:3}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: hp('3%'), height: hp('3%'),marginLeft:3}}/>
</View>
<Text style={{ color:'#FA7F27',fontSize: hp('2.1%'),marginTop:10}}>{'( 4.0 Out Of 280 Reviews )'}</Text>

<View style={{marginTop:10,width:hp('12%'),flexDirection: 'row',padding:hp('1.5%'),justifyContent: 'center', alignItems: 'center',backgroundColor:'#fff',borderRadius:3}}>

<Image source={require('./../../components/Images/heart.png')}
style={{ width: hp('2.2%'), height: hp('2.2%')}}>
</Image>
<Text style={{fontSize: hp('2%'),color: '#7f7f7f',marginLeft:3}}>Watch</Text>

</View>


</View>



    
                               <FlatList
                                data={items}
                                horizontal={true}
                                renderItem={({ item,index }) =>
                                    <TouchableOpacity  onPress={() => this._onPressButton(index)}>
                                       <View style={{height:hp('19%'),width:hp('19%'),margin:5,justifyContent: 'center', alignItems: 'center',backgroundColor:'#2b2b2b',borderRadius:4}} >
                                       <Image source={item.img1} style={{width:hp('8%'),height:hp('8%')}}>
                                        </Image>
                                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize:hp('2.5%'),marginTop:10}}>{item.txt}</Text>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
      
</ScrollView>
       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});