import React, { Component } from 'react';
import {
  View, Text, StatusBar, StyleSheet, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
  ScrollView, KeyboardAvoidingView, FlatList
} from "react-native";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];

export default class MostViewed extends Component {
  render() {
    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#e6e6e6' }}>
        <ScrollView>
                         <View style={{ alignSelf: 'center', marginTop: wp('2.2%')}}>
                             <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>MOST VIEWED PRODUCTS</Text>
                         </View>
 
 
                         <View>
                         <FlatList
                                 data={items}
                                 showsVerticalScrollIndicator={true}
                                 columnWrapperStyle={styles.row}
                                 numColumns={2}
                                 renderItem={({ item }) =>
                                     <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{marginLeft:wp('1.5%'),marginRight:wp('1.5%'),borderRadius:3}}>
                                        <View>
                                             <View style={{flexDirection:'row',}}>
                                                 <View style={{marginTop:wp('2.2%'),backgroundColor: '#fff',alignSelf:'center',
                                                     height: hp('40%'), width: wp('42%'), borderRadius: 3,
                                                 }}>
 
                                            <Image source={item.img1}
                                             style={{
                                                 width: wp('42%'),
                                                 height: wp('42%'),
                                             }}>
                                         </Image>
                                                    
                                                     <View style={{ width: width / 2 - 40, alignSelf: 'center', marginTop: 10,marginLeft:15}}>
                                                         <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize: RF(2.5) }}>
                                                         {'\u20B1'} 11,900.00
                                                        </Text>
                                                        <Text style={{ fontSize: RF(1.7),textDecorationLine:'line-through',color:'#a6a6a6' }}>
                                                         {'\u20B1'} 12,900.00
                                                        </Text>
                                                        <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                         Rev Amplification G4
                                                        </Text>
                                                        <Text style={{ fontSize: RF(1.7), }}>
                                                         Distortion Pedal
                                                        </Text>
                                                        <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                         By: <Text style={{color: '#fe5328', fontWeight: 'bold'}}>Guiter Pusher</Text>
                                                        </Text>
                                                     </View>
 
                                                  
                                                 </View>
                                             </View>
                                         </View>
                                     </TouchableOpacity>
                                 }
                             />
                             
                         </View>
 
                       
 
                         
                     </ScrollView>
       </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});