import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
import Header from "./Header";


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ContactUs extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>






<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center',justifyContent:'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b'}}>CONTACT US</Text>
</View>
</View>

<View style={{justifyContent: 'center', alignItems: 'center',marginLeft:7,marginRight:7}}>

<View style={{height: 55,width:width,marginTop:10}}>
 <View style={{height: 45,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10,borderColor:'#E2E2E2',borderWidth:1}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('8%'),height: 45}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Name"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>

<View style={{height: 55,width:width,marginTop:5}}>
 <View style={{height: 45,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10,borderColor:'#E2E2E2',borderWidth:1}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('8%'),height: 45}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Email"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>

<View style={{height: 55,width:width,marginTop:5}}>
 <View style={{height: 45,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10,borderColor:'#E2E2E2',borderWidth:1}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('8%'),height: 45}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Phone. No"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>

<View style={{height: 125,width:width,marginTop:5}}>
 <View style={{height: 115,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10,borderColor:'#E2E2E2',borderWidth:1}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('8%'),height: 115,textAlignVertical:'top'}}
                                    underlineColorAndroid="transparent"
                                    multiline={true}
                                    placeholder="Write Message"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>

</View>
    

    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
<TouchableOpacity>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'SEND'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});