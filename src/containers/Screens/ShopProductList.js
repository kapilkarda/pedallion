import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ShopProductList extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Cart')} }>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
<View style={{height: 45,width:width}}>
 <View style={{height: 35,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('12%'),height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Search Here"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                            <TouchableOpacity>
                                <View>
                                    <Image source={require("../../components/Images/searchheader.png")}
                                        style={{ width: 18, height:18,marginTop:8.5 }}>
                                    </Image>
                                </View>
                            </TouchableOpacity>
                        </View>

</View>
</LinearGradient>

<View style={{padding:8,marginBottom:wp('2%'),marginTop:wp('2%'),marginLeft:5,marginRight:5}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('23%'),width:wp('23%'),borderWidth:3,borderRadius:3,borderColor:'#fff'}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('21%'),
                                                 height:wp('21%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 
 <Text style={{ color:'#000',fontWeight:'700',fontSize:hp('4%'),marginLeft:5,marginRight:15}}>New Gear Day</Text>
 <Text style={{ color:'#838383',fontSize:hp('2.4%'),marginLeft:5,marginTop:2,marginRight:15}}>Metro Manila, Phili</Text>
 <View style={{marginTop:5,flexDirection:'row',marginLeft:5}}>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Text style={{ color:'#FA7E2A',fontSize:RF(2),marginLeft:6,marginRight:15}}>(4.0 Out of 280 Reviews)</Text>
</View>


  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>




<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center',marginLeft:15}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b' }}>FILTERS</Text>
<Image source={require('./../../components/Images/filter.png')}
style={{ width: hp('3%'), height: hp('3%'),marginLeft:5}}>
</Image>

</View>
<View style={{marginRight:15,flexDirection: 'row',flex:1,alignItems:'flex-end',justifyContent:'flex-end',alignItems: 'center'}}>
<Text style={{fontSize: hp('2.5%'),fontWeight: 'bold', color: '#2b2b2b' }}>SORT BY</Text>
<Image source={require('./../../components/Images/down.png')}
style={{ width: hp('3%'), height: hp('3%'),marginLeft:5}}>
</Image>
</View>

</View>

    
<FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                columnWrapperStyle={styles.row}
                                numColumns={2}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{marginLeft:wp('1.5%'),marginRight:wp('1.5%'),borderRadius:3}}>
                                       <View>
                                            <View style={{flexDirection:'row',}}>
                                                <View style={{marginTop:wp('2.2%'),backgroundColor: '#fff',alignSelf:'center',
                                                    height: hp('40%'), width: wp('42%'), borderRadius: 3,
                                                }}>

                                           <Image source={item.img1}
                                            style={{
                                                width: wp('42%'),
                                                height: wp('42%'),
                                            }}>
                                        </Image>
                                                   
                                                    <View style={{ width: width / 2 - 40, alignSelf: 'center', marginTop: 10,marginLeft:15}}>
                                                        <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize: RF(2.5) }}>
                                                        {'\u20B1'} 11,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),textDecorationLine:'line-through',color:'#a6a6a6' }}>
                                                        {'\u20B1'} 12,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        Rev Amplification G4
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7), }}>
                                                        Distortion Pedal
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        By: <Text style={{color: '#fe5328', fontWeight: 'bold'}}>Guiter Pusher</Text>
                                                       </Text>
                                                    </View>

                                                 
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
      

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});