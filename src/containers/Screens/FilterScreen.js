import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];

const items2 = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
  {
    img1: require("../../components/Images/new2.png"),
},
{
    img1: require("../../components/Images/new4.png"),
},
{
    img1: require("../../components/Images/new5.png"),
},
{
    img1: require("../../components/Images/new6.png"),
},
{
    img1: require("../../components/Images/new77.png"),
},
{
    img1: require("../../components/Images/new8.png"),
},
];



export default class FilterScreen extends Component{


  static navigationOptions = {
    header: null
}

renderSeparator = () => {
  return (
    <View
      style={{
        height: 1,
        backgroundColor: "#CED0CE",
      }}
    />
  );
};

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('10%'),
                                            height: hp('4%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>


<View style={{flexDirection: 'row',width:width,height:height,backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1}}>
<FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                ItemSeparatorComponent={this.renderSeparator}
                                
                                renderItem={({ item }) =>

                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{borderRadius:3,backgroundColor:'#dddddd'}}>
                                       <View>
                                            <View style={{flexDirection:'row',paddingTop:15,paddingBottom:15,paddingLeft:10,paddingRight:10}}>
                                            <Text style={{ fontSize: RF(3),fontWeight:'bold',marginTop:5}}>
                                                        Brand
                                                      </Text>
                                               </View>
                                        </View>
                                    </TouchableOpacity>
                                  
                                }
                            />

</View>

<View style={{flexDirection: 'row',flex:1}}>

<FlatList
                                data={items2}
                                showsVerticalScrollIndicator={true}
                                ItemSeparatorComponent={this.renderSeparator}
                                renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{backgroundColor:'#fff'}}>
                                       <View>
                                            <View style={{flexDirection:'row',paddingTop:8,paddingBottom:8,paddingLeft:5,paddingRight:5}}>
                                            <Image source={require('./../../components/Images/checkmark.png')}
                                            style={{
                                                width: hp('2%'),
                                                height: hp('2%'),
                                            }}>
                                        </Image>
                                            <Text style={{ marginLeft:10,fontSize: RF(1.8),fontWeight:'200'}}>
                                                        Brand
                                                       </Text>
                                               </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />

</View>

</View>

    

      

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});