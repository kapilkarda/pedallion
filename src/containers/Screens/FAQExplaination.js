import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity, Dimensions,SafeAreaView,
} from 'react-native';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
import Header from "./Header";




export default class FAQExplaination extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        a:false,
        b:false,
        c:false,
        d:false,
        e:false,
        f:false,
        g:false,
        h:false,
    };
  }


updateFuntionA(){
    this.setState({
        a:!this.state.a,
        b:false,
        c:false,
        d:false,
        e:false,
    })
}
updateFuntionB(){
    this.setState({
        a:false,
        b:!this.state.b,
        c:false,
        d:false,
        e:false,
    })
}

updateFuntionC(){
    this.setState({
        a:false,
        b:false,
        c:!this.state.c,
        d:false,
        e:false,
    })
}

updateFuntionD(){
    this.setState({
        a:false,
        b:false,
        c:false,
        d:!this.state.d,
        e:false,
    })
}

updateFuntionE(){
    this.setState({
        a:false,
        b:false,
        c:false,
        d:false,
        e:!this.state.e,
    })
}






  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>






<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center',justifyContent:'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b'}}>Get Started</Text>
</View>
</View>

<View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginTop:5,marginLeft:5,marginRight:5}}>
                                 <View style={{flex:4}}>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>Get Started</Text>
                                 </View>
                                 <View style={{flex:2,justifyContent:'center'}}>
                                 <TouchableOpacity  onPress={ () => { this.updateFuntionA() } }>
                                 {this.state.a==true
                                  ?
                                  <Image source={require('./../../components/Images/down_p.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                 :
                                 <Image source={require('./../../components/Images/down_q.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                }
</TouchableOpacity>
                                 </View>
                                 </View> 
                                 {this.state.a==true
                                  ?
                                  <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <Text style={{ color:'#191919',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing However, two of the most requested features are still missing today – file management and syncing</Text>
                                 </View>
                                 :
                                 <View></View>
                                }
                                 <View style={{borderBottomColor: 'black',borderBottomWidth: 1,marginLeft:5,marginRight:5}}/>



                                 <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <View style={{flex:4}}>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>Buyer Resource</Text>
                                 </View>
                                 <View style={{flex:2,justifyContent:'center'}}>
                                 <TouchableOpacity  onPress={ () => { this.updateFuntionB() } }>
                                 {this.state.b==true
                                  ?
                                  <Image source={require('./../../components/Images/down_p.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                 :
                                 <Image source={require('./../../components/Images/down_q.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                }
</TouchableOpacity>
                                 </View>
                                 </View> 
                                 {this.state.b==true
                                  ?
                                  <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <Text style={{ color:'#191919',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing However, two of the most requested features are still missing today – file management and syncing</Text>
                                 </View>
                                 :
                                 <View></View>
                                }
                                 <View style={{borderBottomColor: 'black',borderBottomWidth: 1,marginLeft:5,marginRight:5}}/>


                                 <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <View style={{flex:4}}>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>Seller Resource</Text>
                                 </View>
                                 <View style={{flex:2,justifyContent:'center'}}>
                                 <TouchableOpacity  onPress={ () => { this.updateFuntionC() } }>
                                 {this.state.c==true
                                  ?
                                  <Image source={require('./../../components/Images/down_p.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                 :
                                 <Image source={require('./../../components/Images/down_q.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                }
</TouchableOpacity>
                                 </View>
                                 </View> 
                                 {this.state.c==true
                                  ?
                                  <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <Text style={{ color:'#191919',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing However, two of the most requested features are still missing today – file management and syncing</Text>
                                 </View>
                                 :
                                 <View></View>
                                }
                                 <View style={{borderBottomColor: 'black',borderBottomWidth: 1,marginLeft:5,marginRight:5}}/>


                                 <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <View style={{flex:4}}>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>Pedallion Fees for Sellers</Text>
                                 </View>
                                 <View style={{flex:2,justifyContent:'center'}}>
                                 <TouchableOpacity  onPress={ () => { this.updateFuntionD() } }>
                                 {this.state.d==true
                                  ?
                                  <Image source={require('./../../components/Images/down_p.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                 :
                                 <Image source={require('./../../components/Images/down_q.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                }
</TouchableOpacity>
                                 </View>
                                 </View> 
                                 {this.state.d==true
                                  ?
                                  <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <Text style={{ color:'#191919',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing However, two of the most requested features are still missing today – file management and syncing</Text>
                                 </View>
                                 :
                                 <View></View>
                                }
                                 <View style={{borderBottomColor: 'black',borderBottomWidth: 1,marginLeft:5,marginRight:5}}/>


                                 <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <View style={{flex:4}}>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>Payment Methods</Text>
                                 </View>
                                 <View style={{flex:2,justifyContent:'center'}}>
                                 <TouchableOpacity  onPress={ () => { this.updateFuntionE() } }>
                                 {this.state.e==true
                                  ?
                                  <Image source={require('./../../components/Images/down_p.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                 :
                                 <Image source={require('./../../components/Images/down_q.png')}
                                  style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}/>
                                }

                                 
</TouchableOpacity>
                                 </View>
                                 </View> 
                                 {this.state.e==true
                                  ?
                                  <View style={{flexDirection:'row',backgroundColor:'#fff',padding:10,marginLeft:5,marginRight:5}}>
                                 <Text style={{ color:'#191919',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing However, two of the most requested features are still missing today – file management and syncing</Text>
                                 </View>
                                 :
                                 <View></View>
                                }
                                 

                                 <View style={{borderBottomColor: 'black',borderBottomWidth: 1,marginLeft:5,marginRight:5}}/>


    

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});