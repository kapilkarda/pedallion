import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RNPickerSelect from 'react-native-picker-select';
import { Chevron } from 'react-native-shapes';

const sports = [
    {
      label: 'Football',
      value: 'football',
    },
    {
      label: 'Baseball',
      value: 'baseball',
    },
    {
      label: 'Hockey',
      value: 'hockey',
    },
  ];

const items = [
  {
      img1: require("../../components/Images/one.png"),
  },
  {
      img1: require("../../components/Images/two.png"),
  },
  {
      img1: require("../../components/Images/three.png"),
  },
  {
      img1: require("../../components/Images/four.png"),
  },
];



export default class ProductUploadOne extends Component{


  static navigationOptions = {
    header: null
}


constructor(props) {
    super(props);

    this.inputRefs = {
      firstTextInput: null,
      favSport0: null,
      favSport1: null,
      lastTextInput: null,
    };

    this.state = {
      numbers: [
        {
          label: '1',
          value: 1,
          color: 'orange',
        },
        {
          label: '2',
          value: 2,
          color: 'green',
        },
      ],
      favSport0: undefined,
      favSport1: undefined,
      favSport2: undefined,
      favSport3: undefined,
      favSport4: 'baseball',
      favNumber: undefined,
    };
  }

  secureKey() {
    this.props.navigation.navigate('ProductDetailConfirm')
    
}

  render() {

    const placeholder = {
        label: 'Select One',
        value: null,
        color: '#000',
      };

    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/additem.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                          width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/male.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>

<View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#F3F3F3' }}>
       <ScrollView>
       <View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>PRODUCT UPLOAD</Text>
        </View>

        <View style={{ flexDirection:'row',justifyContent: "center", alignItems: "center",alignSelf: 'center', marginTop: wp('6%')}}>
        <View style={{justifyContent: "center", alignItems: "center"}}>
        <Image source={require("../../components/Images/verified.png")}  style={{width: wp('9%'),height: wp('9%'),}}></Image>
        <Text style={{color: '#2b2b2b',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Upload{"\n"}image(s)</Text>
        </View>

       

        <View style={{justifyContent: "center", alignItems: "center",marginLeft:30,marginRight:30}}>
        <View style={{justifyContent: "center", alignItems: "center",width: wp('9%'),height: wp('9%'),backgroundColor:'#FA9C36',borderRadius: wp('9%')/2}}>
        <Text style={{color: '#ffffff',fontWeight:'bold',fontSize:hp('2%')}}>2</Text>
        </View>
        <Text style={{color: '#FA9C36',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Fill{"\n"}Discription</Text>
        </View>
        
        <View style={{justifyContent: "center", alignItems: "center",}}>
        <View style={{justifyContent: "center", alignItems: "center",width: wp('9%'),height: wp('9%'),backgroundColor:'#2b2b2b',borderRadius: wp('9%')/2}}>
        <Text style={{color: '#ffffff',fontWeight:'bold',fontSize:hp('2%')}}>3</Text>
        </View>
        <Text style={{color: '#2b2b2b',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Review{"\n"}& Upload</Text>
        </View>


        </View>

                                <FlatList
                                style={{marginTop:wp('5%'),marginLeft:10,marginRight:10}}
                                data={items}
                                horizontal={true}
                                renderItem={({ item,index }) =>
                                    <TouchableOpacity>
                                       <View style={{height:hp('15%'),width:hp('15%'),margin:5,justifyContent: 'center', alignItems: 'center',backgroundColor:'#ffffff',borderRadius:4}} >
                                       <Image source={item.img1} style={{width:hp('10%'),height:hp('10%')}}>
                                        </Image>
                                        {index == '3'? <View style={{justifyContent: 'center', alignItems: 'center',flex: 1,position: 'absolute',left: 0,top: 0,opacity: 0.5,backgroundColor: 'black',height:hp('15%'),width:hp('15%')}}>

                                        <Text style={{color: '#fff',fontSize:hp('2%'),alignSelf:'center',fontWeight:'800'}}>8+ images</Text>   
                                        </View> : null }
                                        </View>
                                    </TouchableOpacity>
                                }
                            />



<View style={{height: 50,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 50,fontSize:12}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Product Name*"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

<View style={{height: 50,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 50,fontSize:12}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Real Price*"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

<View style={{height: 50,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 50,fontSize:12}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Your Price*"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

<View style={{height: 50,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 50,fontSize:12}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Brand Name*"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

 <View style={{height: 50,width:width-30,marginLeft:15,marginRight:15,marginTop:15,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{width:width-30,height: 50,fontSize:12}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Model"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>


<View style={{marginTop:15,flexDirection:'row',width:width-hp('2%')}}>
<View style={{flex:1,justifyContent: 'center'}}>
<Text style={{color: '#2b2b2b',fontSize:hp('2.5%'), marginLeft:15,}}>Condition*</Text>
</View>
<View style={{flex:1}}>
<RNPickerSelect
          placeholder={placeholder}
          placeholderTextColor={'#989898'}
          items={sports}
          onValueChange={value => {
            this.setState({
              favSport0: value,
            });
          }}
          style={pickerSelectStyles}
          value={this.state.favSport0}
          ref={el => {
            this.inputRefs.favSport0 = el;
          }}
          Icon={() => {
            return <Chevron size={1} color="#000" />;
          }}
        />

</View>


</View>

<View style={{marginTop:15,flexDirection:'row',width:width-hp('2%')}}>
<View style={{flex:1,justifyContent: 'center'}}>
<Text style={{color: '#2b2b2b',fontSize:hp('2.5%'), marginLeft:15,}}>Cash On Delivery*</Text>
</View>
<View style={{flex:1}}>
<RNPickerSelect
          placeholder={placeholder}
          placeholderTextColor={'#989898'}
          items={sports}
          onValueChange={value => {
            this.setState({
              favSport0: value,
            });
          }}
          style={pickerSelectStyles}
          value={this.state.favSport0}
          ref={el => {
            this.inputRefs.favSport0 = el;
          }}
          Icon={() => {
            return <Chevron size={1} color="#000" />;
          }}
        />

</View>


</View>






<View style={{marginTop:15,marginLeft:15,flexDirection:'row',width:width-30}}>
<View style={{flex:1,justifyContent: 'center'}}>
<Text style={{color: '#2b2b2b',fontSize:hp('2.5%')}}>Stock*</Text>
</View>
<View style={{flex:1}}>
 <View style={{height: 50,width:170,marginLeft:-10,backgroundColor: '#FFF',borderRadius: 4,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:2}}>
                            <View style={{}}>
                                <TextInput style={{height: 50,width:170,fontSize:12}}
                                    underlineColorAndroid="transparent"
                                    placeholder="For example *200*"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>



</View>


</View>

<View style={{marginTop:15,flexDirection:'row',width:width-hp('2%')}}>
<View style={{flex:1,justifyContent: 'center'}}>
<Text style={{color: '#2b2b2b',fontSize:hp('2.5%'), marginLeft:15,}}>Shipping*</Text>
</View>
<View style={{flex:1}}>
<RNPickerSelect
          placeholder={placeholder}
          placeholderTextColor={'#989898'}
          items={sports}
          onValueChange={value => {
            this.setState({
              favSport0: value,
            });
          }}
          style={pickerSelectStyles}
          value={this.state.favSport0}
          ref={el => {
            this.inputRefs.favSport0 = el;
          }}
          Icon={() => {
            return <Chevron size={1} color="#000" />;
          }}
        />

</View>


</View>




<View style={{height: 115,marginLeft:15,width:width-30,marginTop:15}}>
 <View style={{height: 115,backgroundColor: '#FFF',borderRadius: 2,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:1}}>
                            <View style={{}}>
                                <TextInput style={{height: 115,textAlignVertical:'top'}}
                                    underlineColorAndroid="transparent"
                                    multiline={true}
                                    placeholder="Description"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>

<View style={{height: 115,marginLeft:15,width:width-30,marginTop:15,marginBottom:80}}>
 <View style={{height: 115,backgroundColor: '#FFF',borderRadius: 2,paddingHorizontal: 7,borderColor:'#E2E2E2',borderWidth:1}}>
                            <View style={{}}>
                                <TextInput style={{height: 115,textAlignVertical:'top'}}
                                    underlineColorAndroid="transparent"
                                    multiline={true}
                                    placeholder="Privacy Policy"
                                    placeholderTextColor="#989898"
                                    autoCapitalize="none"/>
                            </View>
                        </View>

</View>



       </ScrollView>
</View>


    

    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
    <TouchableOpacity  onPress={() => {this.secureKey()} }>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'NEXT'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>

       </View>
      </SafeAreaView>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      marginRight:5,
        borderColor:'#E2E2E2',borderWidth:2,
        backgroundColor:'#ffffff',
        fontSize: 12,
        paddingVertical: 9,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: '#999999', // to ensure the text is never behind the icon
    },
    inputAndroid: {
          marginRight:5,
        borderColor:'#E2E2E2',borderWidth:2,
        backgroundColor:'#ffffff',
        fontSize: 12,
        paddingVertical: 9,
        paddingHorizontal: 10,
        borderRadius: 4,
        color: '#999999', // to ensure the text is never behind the icon
    },
    iconContainer: {
      top: 20,
      right: 20,
    },
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});
