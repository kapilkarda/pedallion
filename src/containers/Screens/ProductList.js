import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RBSheet from "react-native-raw-bottom-sheet";
import Header from "./Header"

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ProductList extends Component{


  static navigationOptions = {
    header: null
}

  render() {
      const YourOwnComponent = () => 

<View style={{marginLeft:15,alignItems:'flex-start',alignSelf:'flex-start'}}>
<Text style={{ color: '#ddd',fontSize: RF(3),marginTop:10 }}>
                                                       SORT BY
                                                       </Text>
                                                        <Text style={{ color: '#000', fontWeight: 'bold', fontSize: RF(2.8),marginTop:10 }}>
                                                        Most Recent
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>
                                                       Featured
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>
                                                      Bestselling
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>
                                                       Price (Low to High)
                                                       </Text>
                                                       <Text style={{ color: '#000',fontSize: RF(2.8),marginTop:10  }}>Price (High to Low)
                                                       </Text>
                                                    </View>;
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#ddd'}}>
<View style={{flex:1}} >
    
<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>


<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity onPress={() => this.props.navigation.navigate('FilterScreen')}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b' }}>FILTERS</Text>
</TouchableOpacity>
<Image source={require('./../../components/Images/filter.png')}
style={{ width: hp('3%'), height: hp('3%'),marginLeft:5}}>
</Image>

</View>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity onPress={() => this.RBSheet.open()}> 
<Text style={{fontSize: hp('2.5%'),fontWeight: 'bold', color: '#2b2b2b' }}>SORT BY</Text>
</TouchableOpacity>
<Image source={require('./../../components/Images/down.png')}
style={{ width: hp('3%'), height: hp('3%'),marginLeft:5}}>
</Image>
</View>

</View>

    
<FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                columnWrapperStyle={styles.row}
                                numColumns={2}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{marginLeft:wp('1.5%'),marginRight:wp('1.5%'),borderRadius:3}}>
                                       <View>
                                            <View style={{flexDirection:'row',}}>
                                                <View style={{marginTop:wp('2.2%'),backgroundColor: '#fff',alignSelf:'center',
                                                    height: hp('40%'), width: wp('42%'), borderRadius: 3,
                                                }}>

                                           <Image source={item.img1}
                                            style={{
                                                width: wp('42%'),
                                                height: wp('42%'),
                                            }}>
                                        </Image>
                                                   
                                                    <View style={{ width: width / 2 - 40, alignSelf: 'center', marginTop: 10,marginLeft:15}}>
                                                        <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize: RF(2.5) }}>
                                                        {'\u20B1'} 11,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),textDecorationLine:'line-through',color:'#a6a6a6' }}>
                                                        {'\u20B1'} 12,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        Rev Amplification G4
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7), }}>
                                                        Distortion Pedal
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        By: <Text style={{color: '#fe5328', fontWeight: 'bold'}}>Guiter Pusher</Text>
                                                       </Text>
                                                    </View>

                                                 
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />


<RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={250}
          duration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center"
            }
          }}
        >
          <YourOwnComponent />
        </RBSheet>

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});