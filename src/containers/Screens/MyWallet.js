import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image, TouchableOpacity, Dimensions,FlatList,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

var width = Dimensions.get('window').width; //full width

import Header from "./Header";


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class MyWallet extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#ddd'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>


<View style={{flexDirection: 'row',width:width,height:hp('20%'),backgroundColor:'#000',padding:10}}>

<View style={{width:width-20,height:hp('20%')-20,backgroundColor:'#F97B26',borderRadius:4,justifyContent: "center", alignItems: "center"}}>
<Text style={{alignSelf: 'center',fontWeight: 'bold', color: '#fff',fontSize:hp('2.5%')}}>Your Balance</Text>
<Text style={{alignSelf: 'center',fontWeight: 'bold', color: '#fff',fontSize:hp('3.5%'),marginTop:5}}>{'\u20B1'}12,900.00</Text>
</View>



</View>
<LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),flexDirection: 'row'}} >
<TouchableOpacity style={{flex:1,justifyContent: 'center',alignItems: 'center'}} onPress={ () => {    this.props.navigation.navigate('RequestWithdrawn')   } }>
<View>
<Text style={{fontSize:hp('2.1%'),fontWeight:'500',color:'#fff'}}>{'Request Withdrawal'}</Text>
</View> 
</TouchableOpacity>
<TouchableOpacity style={{flex:1,justifyContent: 'center',alignItems: 'center'}} onPress={ () => {    this.props.navigation.navigate('RequestAddMoney')   } }>
<View>
<Text style={{fontSize:hp('2.1%'),fontWeight:'500',color:'#fff'}}>{'Add Money to Wallet'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>

<View style={{marginLeft: wp('2.2%'), marginTop: wp('4.4%'),marginBottom:wp('2.2%')}}>
                            <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>TRANSACTIONS</Text>
                        </View>
    
<FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                renderItem={({ item }) =>
                                <View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2.2%'),width:width-20,marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
                                 <Text style={{ color:'#000',fontSize:RF(2),fontWeight:'500'}}>{'\u20B1'}200 Added to Pedallion Wallet</Text>
                                 <Text style={{ color:'#838181',fontSize:RF(1.8),marginTop:3}}>Sep 20, 2019</Text>
 </View>
                                }
                            />
      

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});