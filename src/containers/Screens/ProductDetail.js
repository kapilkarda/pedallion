import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ImageBackground
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-banner-carousel';
import { Dialog } from 'react-native-simple-dialogs';
import Header from "./Header";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const BannerHeight = 260;

const images = [
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png"
];

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ProductDetail extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        dialogVisible:false,
        showmore:false,
    };
  }

  secureKey() {
    this.setState({dialogVisible: !this.state.dialogVisible})
    
}

secureKeyShowMore() {
    this.setState({showmore: !this.state.showmore})
    
}

renderPage(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: hp('25%'), height: hp('25%'),alignSelf:'center',marginTop:hp('5%'),marginBottom:hp('5%')}} source={{ uri: image }} />
        </View>
    );
}

renderPage2(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: 180, height: 180 }} source={{ uri: image }} />
        </View>
    );
}


  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}}>
<View style={{flex:1}} >

<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>

<View style={{height: 45,width:width,backgroundColor:'#F97B26'}}>
 <View style={{height: 35,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('12%'),height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Search Here"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                            <TouchableOpacity>
                                <View>
                                    <Image source={require("../../components/Images/searchheader.png")}
                                        style={{ width: 18, height:18,marginTop:8.5 }}>
                                    </Image>
                                </View>
                            </TouchableOpacity>
                        </View>

</View>
<ScrollView style={{marginBottom:hp('8%')}}>
<View>



<View style={{marginTop:hp('3%'),marginLeft:hp('3%'),width:hp('10%'),flexDirection: 'row',padding:hp('1%'),justifyContent: 'center', alignItems: 'center',borderWidth: 0.5,borderBottomColor: '#7f7f7f',borderRadius:3}}>

<Image source={require('./../../components/Images/heart.png')}
style={{ width: hp('2%'), height: hp('2%')}}>
</Image>
<Text style={{fontSize: hp('2%'),color: '#7f7f7f',marginLeft:3}}>Watch</Text>

</View>

<View style={{backgroundColor:'#ffffff'}}>



                    <Carousel
                    index={0}
                    activePageIndicatorStyle={{backgroundColor:'#fe5328'}}
                    pageSize={width}>
                    {images.map((image, index) => this.renderPage(image, index))}
                    </Carousel>
</View>

<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:10,marginBottom:10}}/>
<View style={{marginTop: 10,marginLeft:25 }}>


                                                       <Text style={{ fontSize: hp('4%'), }}>
                                                        Rev Amplification G4
                                                       </Text>
                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize:hp('3%')}}>
                                                        {'\u20B1'} 11,900.00
                                                       </Text>
                                                       <Text style={{fontSize:hp('2%'),marginLeft:hp('0.5%'),textDecorationLine:'line-through' }}>
                                                        {'\u20B1'} 12,900.00
                                                       </Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#000',fontSize:hp('2.5%'),}}>
                                                        Shipping : 
                                                       </Text>
                                                       <Text style={{fontSize:hp('2.5%'),marginLeft:hp('0.5%'),color:'#fe5328'}}>
                                                        Free
                                                       </Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#000',fontSize:hp('2.5%'),}}>
                                                        In Stock : 
                                                       </Text>
                                                       <Text style={{fontSize:hp('2.5%'),marginLeft:hp('0.5%'),color:'#fe5328'}}>
                                                        Available
                                                       </Text>
                                                       </View>
                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>

<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%')}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
                                        
  
  
<Text style={{ color: '#fe5328',fontSize:hp('2.5%'),marginLeft:hp('0.5%')}}>{'(300 Reviews)'}</Text>
                                                       </View>


  
                                                    </View>
<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>

<View style={{marginLeft:25,marginRight:25 }}>

                                                       <View style={{flexDirection:'row',alignItems:'center',}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%'),marginLeft:hp('1.5%')}}>Brand :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>Rowin</Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%'),backgroundColor:'#f3f4f5',padding:hp('1.5%')}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%')}}>Model :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>RowinLF12</Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%'),marginLeft:hp('1.5%')}}>Condition :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>Brand New</Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%'),backgroundColor:'#f3f4f5',padding:hp('1.5%')}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%')}}>COD (cash on delivery) :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>Available</Text>
                                                       </View>

                                                    </View>

                                                    <View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>
                                                    <Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>PRODUCT DESCRIPTION</Text>
                                                    <Text style={{ color: '#000000',fontSize:hp('2.1%'),marginLeft:15,marginRight:15,marginTop:15}}>Every point along the customer’s journey from research to purchase is important. There is one place, though, where the customer is called upon to make a choice of exceptional consequence.

That special spot is your product detail page. It’s there that the potential customer will determine whether to purchase your product or keep looking.

That means your product detail page design must do three things well {this.state.showmore==true ? <Text>Every point along the customer’s journey from research to purchase is important. There is one place, though, where the customer is called upon to make a choice of exceptional consequence.

That special spot is your product detail page. It’s there that the potential customer will determine whether to purchase your product or keep looking.

That means your product detail page design must do three things well <Text onPress={() => {this.secureKeyShowMore()}} style={{color: '#fe5328',fontWeight:'700',marginLeft:10}}>SHOW LESS</Text></Text>:<Text onPress={() => {this.secureKeyShowMore()}} style={{color: '#fe5328',fontWeight:'700',marginLeft:10}}>SHOW MORE</Text>}</Text>







<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>


<View style={{flexDirection:'row'}}>
<View style={{flexDirection: 'row',flex:4}}>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>SHIPPING & POLICIES</Text>
</View>



<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<Image source={require('./../../components/Images/plus.png')}
style={{ width: hp('2.5%'), height: hp('2.5%')}}>
</Image>
</View>




</View>



<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>


<View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2%'),marginTop:wp('2%'),marginLeft:10,marginRight:10}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('22%'),width:wp('22%')}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('22%'),
                                                 height:wp('22%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 
 <Text style={{ color:'#000',fontSize:hp('2.2%'),marginLeft:10,marginRight:15}}>Sold By : <Text style={{color:'#fe5328'}}>New Gear Day</Text></Text>
 <Text style={{ color:'#000',fontSize:hp('2.2%'),marginLeft:10,marginTop:5,marginRight:15}}>Location : <Text style={{color:'#fe5328'}}>Metro Manila, Philippines</Text></Text>
 <View style={{marginTop:5,flexDirection:'row',marginLeft:10}}>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: 15, height: 15,marginLeft:2}}/>
</View>
<TouchableOpacity onPress={() => this.props.navigation.navigate('ShopDetail')} >
<Text style={{ color:'#fe5328',fontSize:RF(2),marginLeft:10,marginTop:5,marginRight:15,textDecorationLine: 'underline',}}>Visit Shop</Text>
</TouchableOpacity>


  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>






<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>QUESTIONS ABOUT THIS PRODUCT (10)</Text>

<View style={{flexDirection:'row',marginLeft:15,marginRight:15,marginTop:hp('1.5%')}}>
<View style={{flexDirection: 'row',flex:0.5,}}>
<Image source={require('./../../components/Images/icon_q.png')}
style={{ width: hp('3%'), height: hp('3%')}}>
</Image>
</View>
<View style={{flex:6,marginLeft:10}}>
<Text style={{ color: '#000000',fontSize:hp('2%'),fontWeight:'600'}}>Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</Text>
<Text style={{ color: '#7f7f7f',fontSize:hp('1.5%')}}>Rocky.life - 7 months ago</Text>
</View>
</View>

<View style={{flexDirection:'row',marginLeft:15,marginRight:15,marginTop:hp('1.5%')}}>
<View style={{flexDirection: 'row',flex:0.5,}}>
<Image source={require('./../../components/Images/icon_a.png')}
style={{ width: hp('3%'), height: hp('3%')}}>
</Image>
</View>
<View style={{flex:6,marginLeft:10}}>
<Text style={{ color: '#000000',fontSize:hp('2%')}}>Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</Text>
<Text style={{ color: '#7f7f7f',fontSize:hp('1.5%')}}>New Gear Day</Text>
</View>
</View>


<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>


<View style={{flexDirection: 'row',width:width}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<Text style={{fontSize: hp('2%'),color: '#000000',textDecorationLine: 'underline',fontWeight:'bold' }}>View All Question(s)</Text>
</View>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity  onPress={() => this.props.navigation.navigate('MessageDetail')}>
<Text style={{fontSize: hp('2%'),color: '#fe5328',textDecorationLine: 'underline',fontWeight:'bold' }}>Messege Seller</Text>
</TouchableOpacity>
</View>


</View>


<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>
<View style={{backgroundColor:'#f3f4f5'}}>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>VISITORS WHO VIEWED THIS PRODUCT ALSO VIEWED</Text>

<View style={{marginLeft:15,marginRight:15}}>

                                <FlatList
                                data={items}
                                // showsHorizontalScrollIndicator={false}
                                horizontal={true}
                                renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{marginLeft:wp('1.5%'),marginRight:wp('1.5%'),borderRadius:3}}>
                                       <View>
                                            <View style={{flexDirection:'row',}}>
                                                <View style={{marginTop:wp('2.2%'),backgroundColor: '#fff',alignSelf:'center',
                                                    height: hp('40%'), width: wp('42%'), borderRadius: 3,
                                                }}>

                                           <Image source={item.img1}
                                            style={{
                                                width: wp('42%'),
                                                height: wp('42%'),
                                            }}>
                                        </Image>
                                                   
                                                    <View style={{ width: width / 2 - 40, alignSelf: 'center', marginTop: 10,marginLeft:15}}>
                                                        <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize: RF(2.5) }}>
                                                        {'\u20B1'} 11,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),textDecorationLine:'line-through',color:'#a6a6a6' }}>
                                                        {'\u20B1'} 12,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        Rev Amplification G4
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7), }}>
                                                        Distortion Pedal
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        By: <Text style={{color: '#fe5328', fontWeight: 'bold'}}>Guiter Pusher</Text>
                                                       </Text>
                                                    </View>

                                                 
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                            </View>
</View>



<View style={{backgroundColor:'#f3f4f5',marginTop:20}}>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>VISITORS WHO VIEWED THIS PRODUCT ULTIMATELY BOUGHT</Text>
<View style={{marginLeft:15,marginRight:15}}>
<FlatList
                                data={items}
                                // showsHorizontalScrollIndicator={false}
                                horizontal={true}
                                renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail')} style={{marginLeft:wp('1.5%'),marginRight:wp('1.5%'),borderRadius:3}}>
                                       <View>
                                            <View style={{flexDirection:'row',}}>
                                                <View style={{marginTop:wp('2.2%'),backgroundColor: '#fff',alignSelf:'center',
                                                    height: hp('40%'), width: wp('42%'), borderRadius: 3,
                                                }}>

                                           <Image source={item.img1}
                                            style={{
                                                width: wp('42%'),
                                                height: wp('42%'),
                                            }}>
                                        </Image>
                                                   
                                                    <View style={{ width: width / 2 - 40, alignSelf: 'center', marginTop: 10,marginLeft:15}}>
                                                        <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize: RF(2.5) }}>
                                                        {'\u20B1'} 11,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),textDecorationLine:'line-through',color:'#a6a6a6' }}>
                                                        {'\u20B1'} 12,900.00
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        Rev Amplification G4
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7), }}>
                                                        Distortion Pedal
                                                       </Text>
                                                       <Text style={{ fontSize: RF(1.7),marginTop:5}}>
                                                        By: <Text style={{color: '#fe5328', fontWeight: 'bold'}}>Guiter Pusher</Text>
                                                       </Text>
                                                    </View>

                                                 
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                            </View>
</View>


</View>
</ScrollView>
<LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
<TouchableOpacity  onPress={() => {this.secureKey()} }>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'ADD TO CART'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>

</View>


    <Dialog
    contentStyle={{ margin: 0, padding: 0,height:height/1.8,backgroundColor:'#ffffff' }}
    visible={this.state.dialogVisible}
    onTouchOutside={() => this.setState({dialogVisible: false})} >
    <View style={{alignItems:'center',backgroundColor:'#ffffff'}}>
    <View style={{height:hp('13%'),width:hp('13%')}}>
    <Image source={require("../../components/Images/watch.png")}
                                              style={{
                                                  width:hp('13%'),
                                                  height:hp('13%'),
                                              }}></Image>
    </View>
    <Text style={{ color: '#000000',fontWeight: '500',fontSize:hp('3%'),marginTop:hp('1%')}}>Rowin Pedal Tuner</Text>
    <Text style={{ color: '#fe5328',fontWeight: 'bold',fontSize:hp('3.5%'),marginTop:10}}>{'\u20B1'} 11,900.00</Text>
    <Text style={{ color: '#7f7f7f',fontWeight: 'bold',fontSize: hp('2%'),textDecorationLine:'line-through',marginTop:10}}>{'\u20B1'} 11,900.00</Text>
    <Text style={{ color:'#000',fontSize: hp('2.3%'),marginTop:10,}}>Shipping : <Text style={{color:'#fe5328'}}>Free</Text></Text>
    <Text style={{ color:'#000',fontSize: hp('2.3%'),marginTop:10,}}>In Stock: <Text style={{color:'#fe5328'}}>Available</Text></Text>
    <View style={{flexDirection: 'row',borderWidth: 0.5,borderBottomColor: '#DCDCDC',width:hp('17%'),marginTop:10, justifyContent: 'center',alignItems: 'center',padding:hp('0.5%')}}>
    <Text style={{ color:'#7f7f7f',fontSize: hp('3%'),marginRight:5}}>-</Text>
    <Text style={{ color:'#000',fontSize: hp('3.2%'),marginLeft:25,marginRight:25,}}>1</Text>
    <Text style={{ color:'#7f7f7f',fontSize: hp('3%'),marginLeft:5}}>+</Text>
    </View>
    <Text style={{fontSize: hp('1.8%'),fontWeight:'500',color: '#7f7f7f',textDecorationLine: 'underline',marginTop:25,marginBottom:25}}>Continue Shopping</Text>

    <LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%', 
      height: hp('7%'), 
      justifyContent: 'center', 
      alignItems: 'center'}}>
    <View>
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'BUY IT!'}</Text>
      </View> 
    </LinearGradient>            
   
 
   
 
    </View>
</Dialog> 
       
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});
