import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView
} from 'react-native';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import LinearGradient from "react-native-linear-gradient";
import RNPickerSelect from 'react-native-picker-select';
import { Chevron } from 'react-native-shapes';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const items = [
    {
        img1: require("../../components/Images/circle.png"),
    },
    {
        img1: require("../../components/Images/tms.png"),
    },
    {
        img1: require("../../components/Images/happy.png"),
    },
    {
        img1: require("../../components/Images/spaces.png"),
    },
  ];

const sports = [
    {
      label: 'Football',
      value: 'football',
    },
    {
      label: 'Baseball',
      value: 'baseball',
    },
    {
      label: 'Hockey',
      value: 'hockey',
    },
  ];

export default class Cart extends Component{




  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);

    this.inputRefs = {
      firstTextInput: null,
      favSport0: null,
      favSport1: null,
      lastTextInput: null,
    };

    this.state = {
      numbers: [
        {
          label: '1',
          value: 1,
          color: 'orange',
        },
        {
          label: '2',
          value: 2,
          color: 'green',
        },
      ],
      favSport0: undefined,
      favSport1: undefined,
      favSport2: undefined,
      favSport3: undefined,
      favSport4: 'baseball',
      favNumber: undefined,
    };
  }



  render() {

    const placeholder = {
        label: 'XYZ Shipping Method',
        value: null,
        color: '#000',
      };

    return (
<SafeAreaView style={{flex: 1,backgroundColor: '#e6e6e6'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                          width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>


<View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#e6e6e6' }}>
       <ScrollView>
                        <View style={{marginLeft: wp('2.2%'), marginTop: wp('2.2%')}}>
                            <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>CART</Text>
                        </View>


                        <View>
                        <FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                renderItem={({ item }) =>
                                <View style={{backgroundColor:'#fff',marginTop:wp('2.2%'),borderRadius:3,padding:wp('2.2%'),marginLeft:wp('2.2%'),marginRight:wp('2.2%')}}>
      <View style={{flexDirection: 'row',marginTop:hp('0.2%'),marginBottom:5}}>
      <Text style={{ color:'#000',fontSize: hp('1.9%'),marginLeft:hp('1%'),}}>BY :</Text>
      <Text style={{ color:'#F95227',fontSize: hp('1.9%'),marginLeft:3}}>Guitar Pusher</Text>    
      </View>
      <View
  style={{
      marginTop:wp('0.2%'),
    borderBottomColor: '#DCDCDC',
    borderBottomWidth: 0.5,
  }}
/>

<View>
 <View style={{flexDirection: 'row',width:width,marginTop:wp('2.2%')}}>
 <View style={{height:wp('34%'),width:wp('33%')}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('33%'),
                                                 height:wp('34%'),
                                             }}>
                                         </Image>
 </View>
 <View style={{width:wp('62%'),marginLeft:8}} >
 <Text style={{color:'#000',fontSize:RF(2),flexWrap: 'wrap',flex: 1,marginRight:10}}>Revv Amplification G4 Distortion Pedal</Text>

 <View style={{flexDirection: 'row',borderWidth: 0.5,borderBottomColor: '#DCDCDC',width:hp('15%'),marginTop:5, justifyContent: 'center',alignItems: 'center',padding:hp('0.5%')}}>
 <Text style={{ color:'#7f7f7f',fontSize: hp('3%')}}>-</Text>
 <Text style={{ color:'#000',fontSize: hp('2.5%'),marginLeft:15,marginRight:15,}}>1</Text>
 <Text style={{ color:'#7f7f7f',fontSize: hp('3%')}}>+</Text>
 </View>

<View style={{flexDirection: 'row',marginTop:wp('2.2%'),marginRight:wp('2.5%')}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2%')}}>Unit Cost</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%',}}>
<Text style={{ color:'#000',fontSize: hp('2%'),marginRight:10}}>&#8369; 12,900.00</Text>
</View>
</View>

<View style={{marginBottom:5,marginTop:5,marginRight:wp('5%'),borderBottomColor: '#000000',borderBottomWidth: 0.5,}}/>

<View style={{flexDirection: 'row',marginTop:wp('2.2%'),marginRight:wp('2.5%')}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#ff572b',fontSize: hp('2%')}}>Total</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%'}}>
<Text style={{ color:'#ff572b',fontSize: hp('2%'),marginRight:10}}>&#8369; 11,900.00</Text>
</View>
</View>
</View>
</View>
</View>


<RNPickerSelect
          placeholder={placeholder}
          placeholderTextColor={'#000000'}
          items={sports}
          onValueChange={value => {
            this.setState({
              favSport0: value,
            });
          }}
          style={pickerSelectStyles}
          value={this.state.favSport0}
          ref={el => {
            this.inputRefs.favSport0 = el;
          }}
          Icon={() => {
            return <Chevron size={1.5} color="#000" />;
          }}
        />

<RNPickerSelect
          placeholder={placeholder}
          placeholderTextColor={'#000000'}
          items={sports}
          onValueChange={value => {
            this.setState({
              favSport0: value,
            });
          }}
          style={pickerSelectStyles}
          value={this.state.favSport0}
          ref={el => {
            this.inputRefs.favSport0 = el;
          }}
          Icon={() => {
            return <Chevron size={1.5} color="#000" />;
          }}
        />



</View>
                                }
                            />

<View style={{height:40,backgroundColor:'#F95127',marginLeft:7,marginRight:7,marginTop:15,marginBottom:15,borderRadius:4}}>

<View style={{borderColor:'#ffffff',margin:5,borderWidth:1,borderStyle:'dashed',height:30,borderRadius:4,justifyContent:'center',alignItems:'center'}}>
<Text style={{ color:'#fff',fontSize: hp('2.5%'),fontWeight:'bold'}}>Ask For Total</Text>
</View>


</View>

<View style={{flexDirection:'row',backgroundColor:'#fff',padding:5,marginLeft:7,marginRight:7}} >
<View style={{flex:5,backgroundColor:'#fff',justifyContent:'center',alignItems:'center'}}>
<TextInput style={{width:'95%',height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Coupon Code"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
</View> 
<View style={{flex:1,alignItems:'flex-end'}}>
<LinearGradient style={{ height: 35,
        width: wp('28%'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,}}
                                            start={{x: 0,y: 0}}
                                            end={{x: 0,y: 1}}
                                            colors={['#feae3d','#ff8333','#ff572b',]}>
                                            <Text style={{color: '#fff',
        fontSize: hp('1.9%'),
        fontWeight: 'bold'}}>
                                            APPLY CODE
                                            </Text>
                                            </LinearGradient>
</View> 

</View>

<View style={{flexDirection: 'row',marginTop:15,marginLeft:7,marginRight:7}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>Total Item</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>1</Text>
</View>
</View>
<View style={{flexDirection: 'row',marginTop:15,marginLeft:7,marginRight:7}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>Subtotal</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>&#8369;26400.00</Text>
</View>
</View>
<View style={{flexDirection: 'row',marginTop:15,marginLeft:7,marginRight:7}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>Shipping</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>&#8369;700.00</Text>
</View>
</View>
<View style={{flexDirection: 'row',marginTop:15,marginLeft:7,marginRight:7}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>Tax</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('2.8%')}}>&#8369;11,90.00</Text>
</View>
</View>

<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:15,marginLeft:7,marginRight:7}}/>

<View style={{flexDirection: 'row',marginTop:15,marginLeft:7,marginRight:7}}>
<View style={{justifyContent:'flex-start',alignItems:'flex-start',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('3.5%'),fontWeight:'bold'}}>Total</Text>
</View>

<View style={{justifyContent:'flex-end',alignItems:'flex-end',width:'50%'}}>
<Text style={{ color:'#000',fontSize: hp('3.5%'),fontWeight:'bold'}}>&#8369;28,290.00</Text>
</View>
</View>

<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:15,marginLeft:7,marginRight:7}}/>

<View style={{height:40,justifyContent:'center',alignItems:'center',marginTop:15}}>
<TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList')}>

<Text style={{ color:'#989898',fontSize: hp('1.9%'),fontWeight:'bold',textDecorationLine: 'underline'}}>Continue Shopping</Text>

</TouchableOpacity>

</View>

<LinearGradient style={{height:40,marginTop:15}}
                                            start={{x: 0,y: 0}}
                                            end={{x: 0,y: 1}}
                                            colors={['#feae3d','#ff8333','#ff572b',]}>


<View style={{height:40,justifyContent:'center',alignItems:'center'}}>


<Text style={{ color:'#fff',fontSize: hp('2.5%'),fontWeight:'bold'}}>CHECKOUT</Text>



</View>
</LinearGradient>
                            
                        </View>

                      

                        
                    </ScrollView>
      </View>



                               




     


       </View>
      </SafeAreaView>
    );
  }
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        backgroundColor:'#f3f4f5',
        marginTop:10,
        fontSize: wp('3%'),
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#f3f4f5',
        borderRadius: 4,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        backgroundColor:'#f3f4f5',
        marginTop:10,
        fontSize: wp('3%'),
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#f3f4f5',
        borderRadius: 4,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    iconContainer: {
      top: 25,
      right: 40,
    },
});
