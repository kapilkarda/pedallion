import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  ImageBackground,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';

import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
import Header from "./Header";



const items = [
    {
        img1: require("../../components/Images/new11.png"),
    },
    {
        img1: require("../../components/Images/new6.png"),
    },
    {
        img1: require("../../components/Images/new2.png"),
    },
    {
        img1: require("../../components/Images/new4.png"),
    },
    {
        img1: require("../../components/Images/new5.png"),
    },
    {
        img1: require("../../components/Images/new6.png"),
    },
    {
        img1: require("../../components/Images/new77.png"),
    },
    {
        img1: require("../../components/Images/new8.png"),
    },
  ];

export default class FAQ extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >



<Header 
mGoBack={() => this.props.navigation.goBack()}  
mGoHome={() => this.props.navigation.navigate('DrawerNavigator')} 
mGoNotification={() => this.props.navigation.navigate('Notification')} 
mGoCart={() => this.props.navigation.navigate('Cart')}/>




<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center',justifyContent:'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b'}}>FAQ</Text>
</View>
</View>

                                <FlatList
                                 data={items}
                                 showsVerticalScrollIndicator={true}
                                 renderItem={({ item }) =>
                                 <TouchableOpacity  onPress={ () => {    this.props.navigation.navigate('FAQExplaination')   } }>
                                 <View style={{flexDirection:'row',backgroundColor:'#fff',padding:8,marginTop:10,marginLeft:10,marginRight:10}}>
                                 <View style={{flex:4}}>
                                 <Text style={{ color:'#000',fontSize:RF(2)}}>However, two of the most requested features are still missing today – file management and syncing</Text>
                                 </View>
                                 <View style={{flex:2,justifyContent:'center'}}>
                                 <Image source={require('./../../components/Images/forward.png')}
style={{alignSelf:'flex-end',width: hp('3%'), height: hp('3%')}}>
</Image>
                                 </View>
                                 </View>
                                 </TouchableOpacity>
                                 }
                             />
    

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});