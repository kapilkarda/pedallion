import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
];



export default class ShopReviews extends Component{


  static navigationOptions = {
    header: null
}

  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#F3F3F3'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Cart')} }>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
<View style={{height: 45,width:width}}>
 <View style={{height: 35,width:width-hp('5%'),backgroundColor: '#FFF',borderRadius: 2,justifyContent: 'center',alignSelf: 'center',flexDirection: 'row',paddingHorizontal: 10}}>
                            <View style={{}}>
                                <TextInput style={{width:width - hp('12%'),height: 35}}
                                    underlineColorAndroid="transparent"
                                    placeholder="Search Here"
                                    placeholderTextColor="#b3b3b3"
                                    autoCapitalize="none"/>
                            </View>
                            <TouchableOpacity>
                                <View>
                                    <Image source={require("../../components/Images/searchheader.png")}
                                        style={{ width: 18, height:18,marginTop:8.5 }}>
                                    </Image>
                                </View>
                            </TouchableOpacity>
                        </View>

</View>
</LinearGradient>

<View style={{padding:8,marginBottom:wp('2%'),marginTop:wp('2%'),marginLeft:5,marginRight:5}}>
     
     <View>
 <View style={{flexDirection: 'row',width:width}}>
 <View style={{height:wp('23%'),width:wp('23%'),borderWidth:3,borderRadius:3,borderColor:'#fff'}}>
 <Image source={require('./../../components/Images/homeflat1.png')}
                                             style={{
                                                 width:wp('21%'),
                                                 height:wp('21%'),
                                             }}>
                                         </Image>
 </View>
 <View>
 
 
 <Text style={{ color:'#000',fontWeight:'700',fontSize:hp('4%'),marginLeft:5,marginRight:15}}>New Gear Day</Text>
 <Text style={{ color:'#838383',fontSize:hp('2.4%'),marginLeft:5,marginTop:2,marginRight:15}}>Metro Manila, Phili</Text>
 <View style={{marginTop:5,flexDirection:'row',marginLeft:5}}>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Image source={require('./../../components/Images/starshop.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: 15, height: 15,marginLeft:1.5}}/>
<Text style={{ color:'#FA7E2A',fontSize:RF(2),marginLeft:6,marginRight:15}}>(4.0 Out of 280 Reviews)</Text>
</View>


  
 
 </View>
 
 
 
 
 
 
 </View>
 </View>
 
 </View>




<View style={{flexDirection: 'row',width:width,height:hp('7%'),backgroundColor:'#fff'}}>
<View style={{flexDirection: 'row',flex:1,alignItems: 'center'}}>
<Text style={{fontSize: hp('2.5%'), fontWeight: 'bold', color: '#2b2b2b',marginLeft:15}}>REVIEWS</Text>
</View>
</View>

<View style={{justifyContent: 'center', alignItems: 'center',marginLeft:7,marginRight:7}}>
<FlatList
                                data={items}
                                showsVerticalScrollIndicator={true}
                                renderItem={({ item }) =>

<View style={{width:'100%',backgroundColor:'#fff',marginTop:7,borderRadius:3,padding:5}}>
<Text style={{ color: '#2B2B2B',fontSize:hp('2%'),marginLeft:5,marginRight:5,marginTop:5,fontStyle:'italic'}}>"Every point along the customer’s journey from research to purchase is important. There is one place, though, where the customer is called upon to make a choice of exceptional consequence.
That special spot is your product detail page."</Text>
<View style={{flexDirection: 'row',marginTop:10}}>
      <Text style={{ color:'#000',fontSize: hp('1.9%'),marginLeft:5,}}>BY :</Text>
      <Text style={{ color:'#F95227',fontSize: hp('1.9%'),marginLeft:3}}>Devid Pusher</Text>    
      </View>
      <View style={{flexDirection:'row',alignItems:'center',marginTop:2,marginLeft:5}}>

<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2%'), height: hp('2%')}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2%'), height: hp('2%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2%'), height: hp('2%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2%'), height: hp('2%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starblank.png')} style={{ width: hp('2%'), height: hp('2%'),marginLeft:2}}/>
                                        
                                                       </View>

</View>


                                }
                            />
</View>
    

      

       </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});
