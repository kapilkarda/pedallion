import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ImageBackground
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-banner-carousel';
import { Dialog } from 'react-native-simple-dialogs';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const BannerHeight = 260;

const images = [
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png"
];
const STAR_IMAGE = require('../../components/Images/ratstar.png')

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ProductImageUpload extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        dialogVisible:false
    };
  }

  secureKey() {
    this.props.navigation.navigate('ProductUploadOne')
    
}

renderPage(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: hp('25%'), height: hp('25%'),alignSelf:'center',marginTop:hp('5%'),marginBottom:hp('5%')}} source={{ uri: image }} />
        </View>
    );
}

renderPage2(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: 180, height: 180 }} source={{ uri: image }} />
        </View>
    );
}


  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/additem.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/male.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>

<View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#F3F3F3' }}>
       <ScrollView>
       <View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>PRODUCT UPLOAD</Text>
        </View>

        <View style={{ flexDirection:'row',justifyContent: "center", alignItems: "center",alignSelf: 'center', marginTop: wp('6%')}}>
      

       

        <View style={{justifyContent: "center", alignItems: "center",}}>
        <View style={{justifyContent: "center", alignItems: "center",width: wp('9%'),height: wp('9%'),backgroundColor:'#FA9C36',borderRadius: wp('9%')/2}}>
        <Text style={{color: '#ffffff',fontWeight:'bold',fontSize:hp('2%')}}>1</Text>
        </View>
        <Text style={{color: '#FA9C36',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Upload{"\n"}image(s)</Text>
        </View>
        
        <View style={{justifyContent: "center", alignItems: "center",marginLeft:30,marginRight:30}}>
        <View style={{justifyContent: "center", alignItems: "center",width: wp('9%'),height: wp('9%'),backgroundColor:'#2b2b2b',borderRadius: wp('9%')/2}}>
        <Text style={{color: '#ffffff',fontWeight:'bold',fontSize:hp('2%')}}>2</Text>
        </View>
        <Text style={{color: '#2b2b2b',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Fill{"\n"}Discription</Text>
        </View>

        <View style={{justifyContent: "center", alignItems: "center",}}>
        <View style={{justifyContent: "center", alignItems: "center",width: wp('9%'),height: wp('9%'),backgroundColor:'#2b2b2b',borderRadius: wp('9%')/2}}>
        <Text style={{color: '#ffffff',fontWeight:'bold',fontSize:hp('2%')}}>3</Text>
        </View>
        <Text style={{color: '#2b2b2b',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Review{"\n"}& Upload</Text>
        </View>

    

        </View>

        <View style={{alignSelf:'center',marginTop:20,marginBottom:20,width:hp('30%'),height:hp('30%'),backgroundColor:'#fff',borderColor:'#E2E2E2',borderWidth:2}}>
        <Image source={require('./../../components/Images/add.png')}
        style={{ width: hp('7%'), height: hp('7%'),alignSelf:'center',marginTop:50}}></Image>

                                            <LinearGradient style={{ height: hp('5%'),
        width: wp('30%'),
        marginTop:30,
        alignItems: 'center',
        alignSelf:'center',
        justifyContent: 'center',
        borderRadius: 2, }}
                                            start={{x: 0,y: 0}}
                                            end={{x: 0,y: 1}}
                                            colors={['#feae3d','#ff8333','#ff572b',]}>
                                            <Text style={{color: '#fff',
        fontSize: hp('1.3%'),
        fontWeight: 'bold'}}>
                                            UPLOAD IMAGE(S)
                                            </Text>
                                            </LinearGradient>

        </View>

        <Text style={{color: '#838383',marginLeft:45,marginRight:45,textAlign: 'center',fontStyle:'italic', fontSize:hp('2%'),alignSelf:'center',marginTop:15,marginBottom:15}}>*You can upload multiple images, it will increase the chances of selling that product and please try to upload good quality images so your customer can see properly what they are buying</Text>


</ScrollView>
</View>
<LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
<TouchableOpacity  onPress={() => {this.secureKey()} }>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'NEXT'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>

</View>


  
       
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});