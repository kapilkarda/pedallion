import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image, TouchableOpacity, Dimensions,FlatList,ScrollView,SafeAreaView,ImageBackground
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Header, Left, Right, Tab, Tabs, Row } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-banner-carousel';
import { Dialog } from 'react-native-simple-dialogs';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const BannerHeight = 260;

const images = [
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",
    "https://img.pngio.com/fitbit-surge-fitbit-png-1080_920.png",

];
const STAR_IMAGE = require('../../components/Images/ratstar.png')

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];



export default class ProductDetailConfirm extends Component{


  static navigationOptions = {
    header: null
}

constructor(props) {
    super(props);
    this.state = {
        dialogVisible:false
    };
  }

  secureKey() {
    this.setState({dialogVisible: !this.state.dialogVisible})
    
}

renderPage(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: hp('25%'), height: hp('25%'),alignSelf:'center',marginTop:hp('5%'),marginBottom:hp('5%')}} source={{ uri: image }} />
        </View>
    );
}

renderPage2(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: 180, height: 180 }} source={{ uri: image }} />
        </View>
    );
}


  render() {
    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/additem.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                            width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<TouchableOpacity onPress={() => {this.props.navigation.navigate('Notification')} }>
                                        <Image source={require('./../../components/Images/notification.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginRight:hp('3%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/male.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('3%')}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>

<View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#F3F3F3' }}>
       <ScrollView>
       <View style={{ alignSelf: 'center', marginTop: wp('6%')}}>
        <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>PRODUCT UPLOAD</Text>
        </View>

        <View style={{ flexDirection:'row',justifyContent: "center", alignItems: "center",alignSelf: 'center', marginTop: wp('6%')}}>
        <View style={{justifyContent: "center", alignItems: "center"}}>
        <Image source={require("../../components/Images/verified.png")}  style={{width: wp('9%'),height: wp('9%'),}}></Image>
        <Text style={{color: '#2b2b2b',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Upload{"\n"}image(s)</Text>
        </View>

       

       
        
        <View style={{justifyContent: "center", alignItems: "center",marginLeft:30,marginRight:30}}>
        <Image source={require("../../components/Images/verified.png")}  style={{width: wp('9%'),height: wp('9%'),}}></Image>
        <Text style={{color: '#2b2b2b',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Fill{"\n"}Discription</Text>
        </View>

        <View style={{justifyContent: "center", alignItems: "center",}}>
        <View style={{justifyContent: "center", alignItems: "center",width: wp('9%'),height: wp('9%'),backgroundColor:'#FA9C36',borderRadius: wp('9%')/2}}>
        <Text style={{color: '#ffffff',fontWeight:'bold',fontSize:hp('2%')}}>3</Text>
        </View>
        <Text style={{color: '#FA9C36',fontSize:hp('2%'),alignSelf:'center',marginTop:1,textAlign: 'center'}}>Review{"\n"}& Upload</Text>
        </View>


        </View>

        <Text style={{color: '#838383',marginLeft:35,marginRight:35,textAlign: 'center',fontStyle:'italic', fontSize:hp('2%'),alignSelf:'center',marginTop:15,marginBottom:15}}>*Please re-check your product once below and then click "Confirm & Upload" at the bottom for posting on your shop</Text>


<View style={{backgroundColor:'#fff'}}>



<View style={{marginTop:hp('3%'),marginLeft:hp('3%'),width:hp('10%'),flexDirection: 'row',padding:hp('1%'),justifyContent: 'center', alignItems: 'center',borderWidth: 0.5,borderBottomColor: '#7f7f7f',borderRadius:3}}>

<Image source={require('./../../components/Images/heart.png')}
style={{ width: hp('2%'), height: hp('2%')}}>
</Image>
<Text style={{fontSize: hp('2%'),color: '#7f7f7f',marginLeft:3}}>Watch</Text>

</View>

<View style={{backgroundColor:'#ffffff'}}>



                    <Carousel
                    index={0}
                    activePageIndicatorStyle={{backgroundColor:'#fe5328'}}
                    pageSize={width}>
                    {images.map((image, index) => this.renderPage(image, index))}
                    </Carousel>
</View>

<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:10,marginBottom:10}}/>
<View style={{marginTop: 10,marginLeft:25 }}>


                                                       <Text style={{ fontSize: hp('4%'), }}>
                                                        Rev Amplification G4
                                                       </Text>
                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#fe5328', fontWeight: 'bold', fontSize:hp('3%')}}>
                                                        {'\u20B1'} 11,900.00
                                                       </Text>
                                                       <Text style={{fontSize:hp('2%'),marginLeft:hp('0.5%'),textDecorationLine:'line-through' }}>
                                                        {'\u20B1'} 12,900.00
                                                       </Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#000',fontSize:hp('2.5%'),}}>
                                                        Shipping : 
                                                       </Text>
                                                       <Text style={{fontSize:hp('2.5%'),marginLeft:hp('0.5%'),color:'#fe5328'}}>
                                                        Free
                                                       </Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#000',fontSize:hp('2.5%'),}}>
                                                        In Stock : 
                                                       </Text>
                                                       <Text style={{fontSize:hp('2.5%'),marginLeft:hp('0.5%'),color:'#fe5328'}}>
                                                        Available
                                                       </Text>
                                                       </View>
                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>

<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%')}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
<Image source={require('./../../components/Images/starfill.png')} style={{ width: hp('2.5%'), height: hp('2.5%'),marginLeft:2}}/>
                                        
  
  
<Text style={{ color: '#fe5328',fontSize:hp('2.5%'),marginLeft:hp('0.5%')}}>{'(300 Reviews)'}</Text>
                                                       </View>


  
                                                    </View>
<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>

<View style={{marginLeft:25,marginRight:25 }}>

                                                       <View style={{flexDirection:'row',alignItems:'center',}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%'),marginLeft:hp('1.5%')}}>Brand :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>Rowin</Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%'),backgroundColor:'#f3f4f5',padding:hp('1.5%')}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%')}}>Model :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>RowinLF12</Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%')}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%'),marginLeft:hp('1.5%')}}>Condition :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>Brand New</Text>
                                                       </View>

                                                       <View style={{flexDirection:'row',alignItems:'center',marginTop:hp('0.5%'),backgroundColor:'#f3f4f5',padding:hp('1.5%')}}>
                                                       <Text style={{ color: '#000000',fontSize:hp('2.5%')}}>COD (cash on delivery) :</Text>
                                                       <Text style={{color:'#000000',marginLeft:hp('0.5%'),fontSize:hp('2.5%')}}>Available</Text>
                                                       </View>

                                                    </View>

                                                    <View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>
                                                    <Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>PRODUCT DESCRIPTION</Text>
                                                    <Text style={{ color: '#000000',fontSize:hp('2.1%'),marginLeft:15,marginRight:15,marginTop:15}}>Every point along the customer’s journey from research to purchase is important. There is one place, though, where the customer is called upon to make a choice of exceptional consequence.

That special spot is your product detail page. It’s there that the potential customer will determine whether to purchase your product or keep looking.

That means your product detail page design must do three things well <Text style={{color: '#fe5328',fontWeight:'700',marginLeft:10}}>SHOW MORE</Text></Text>







<View style={{height:1,backgroundColor:'#7f7f7f',marginTop:20,marginBottom:20}}/>


<View style={{flexDirection:'row',marginBottom:60}}>
<View style={{flexDirection: 'row',flex:4}}>
<Text style={{ color: '#000000',fontSize:hp('2.5%'),fontWeight:'600',marginLeft:15}}>SHIPPING & POLICIES</Text>
</View>



<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center'}}>
<Image source={require('./../../components/Images/plus.png')}
style={{ width: hp('2.5%'), height: hp('2.5%')}}>
</Image>
</View>




</View>

















</View>
</ScrollView>
</View>
<LinearGradient colors={['#feae3d', '#ff8333', '#ff572b']} style={{width: '100%',height: hp('7%'),justifyContent: 'center',alignItems: 'center',position: 'absolute',bottom: 0}} >
<TouchableOpacity  onPress={() => {this.secureKey()} }>
<View >
           <Text style={{fontSize:hp('2.5%'),fontWeight:'bold',color:'#fff'}}>{'CONFIRM & UPLOAD'}</Text>
</View> 
</TouchableOpacity>
</LinearGradient>

</View>


  
       
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});