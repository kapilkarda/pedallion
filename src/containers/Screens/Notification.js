import React, { Component } from 'react';
import {
  View, Text, StatusBar, StyleSheet, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
  ScrollView, KeyboardAvoidingView, FlatList,SafeAreaView
} from "react-native";
import { Rating, AirbnbRating } from 'react-native-ratings';
import LinearGradient from "react-native-linear-gradient";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const items = [
  {
      img1: require("../../components/Images/new11.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new2.png"),
  },
  {
      img1: require("../../components/Images/new4.png"),
  },
  {
      img1: require("../../components/Images/new5.png"),
  },
  {
      img1: require("../../components/Images/new6.png"),
  },
  {
      img1: require("../../components/Images/new77.png"),
  },
  {
      img1: require("../../components/Images/new8.png"),
  },
];

export default class Notification extends Component {

    static navigationOptions = {
        header: null
    }


  render() {
    return (
<SafeAreaView style={{flex: 1,backgroundColor: '#e6e6e6'}}>
<View style={{flex:1}} >
<LinearGradient colors={['#FAAF40', '#FA8B2F', '#F97B26']}>
<View style={{flexDirection:'row',height:hp('8%')}}>
<View style={{flexDirection: 'row',flex:1,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>
<TouchableOpacity  onPress={ () => { this.props.navigation.goBack() } }>
                                            <Image source={require('./../../components/Images/icback.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>


                                    <TouchableOpacity>
                                            <Image source={require('./../../components/Images/ichome.png')}
                                            style={{
                                                width: hp('4%'),
                                                height: hp('4%'),
                                                marginLeft:hp('2%'),
                                            }}>
                                        </Image>
                                    </TouchableOpacity>

</View>

<View style={{flex:2,justifyContent: 'center', alignItems: 'center',height:hp('8%')}}>

<Image source={require('./../../components/Images/body.png')}
                                        style={{
                                          width: hp('14%'),
                                            height: hp('5%'),

                                            // tintColor: '#737373',
                                        }}>
                                    </Image>

</View>


<View style={{flexDirection: 'row',flex:1,alignItems: 'center',justifyContent:'flex-end',height:hp('8%')}}>


                                    <TouchableOpacity>
                                        <Image source={require('./../../components/Images/cart.png')}
                                            style={{ width: hp('4%'), height: hp('4%'),marginRight:hp('2%'),}}>
                                        </Image>
                                    </TouchableOpacity>

</View>




</View>
</LinearGradient>



        <View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#e6e6e6' }}>
        <ScrollView>
        <View style={{ alignSelf: 'center', marginTop: wp('2.2%')}}>
                            <Text style={{ fontWeight: 'bold', color: '#2b2b2b',fontSize:hp('2.5%')}}>Notifications</Text>
                        </View>

      
        
 
        <FlatList
                                 data={items}
                                 showsVerticalScrollIndicator={true}
                                 renderItem={({ item }) =>
                                 
                                 <View style={{backgroundColor:'#fff',padding:8,marginBottom:wp('2.2%'),width:width-20,marginTop:wp('2.2%'),marginLeft:10,marginRight:10}}>
                                 <Text style={{ color:'#000',fontSize:RF(2),fontWeight:'500'}}>However, two of the most requested features are still missing today – file management and syncing</Text>
                                 <Text style={{ color:'#838181',fontSize:RF(1.8),marginTop:3}}>Sep 20, 2019</Text>
 </View>
                                 }
                             /> 
 
        </ScrollView>
        </View>
        </View></SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
},
textinput1: {
    color: '#000',
    marginLeft: 10,
    fontSize:RF(2.3),marginTop:2
},
row: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal:12
}
});
