import { StyleSheet } from 'react-native';
import { Colors } from '../../Theme';

export const COLOR = {
DARK: "#040207",
PANTOME: '#ff6f61',
LIGHT: "#ffffff",
BLACK: "#000",
GRAY: "#9A9A9A",
LIGHT_GRAY: "#ffffff",
DANGER: "#FF5370",
RED: "#800000",
WHITE:"#FFF",
CYAN: "#09818F",
LIGHT_ORANGE:"#ff944d"
};

export default StyleSheet.create({
container: {
flex: 1,
backgroundColor:'#f2f2f2',

},
form: {
flex: 1,
flexDirection: 'column',
justifyContent: 'space-around',
alignItems: 'stretch'
},
logo: {
flex: 0.42,
alignSelf: 'center',
top: '10%'
},
input: {
//flex: 0.21,
alignSelf: 'flex-end',
fontWeight: '900'
},
label: {
fontWeight: '900',
color: Colors.dark
},
button: {
marginBottom: 15,
width: '90%',
alignSelf: 'center'
},
animatedView: {
alignSelf: 'center',
bottom: '5%',
position: 'relative',
justifyContent: 'center',
alignItems: 'center'
},

header: {
position: "absolute",
left: 0,
right: 0,
top: 0,
width: '100%',
zIndex: 100
},
signin: {
color: "red",
fontWeight: 'bold',
fontSize: 50,
textAlign: 'left',
margin: 10,

},
emailaddress: {
fontSize: 22,
textAlign: 'left',
color: '#000000',
marginTop: 10,
marginLeft:30,

},
completetext: {
fontSize: 25,
textAlign: 'left',
color: '#000000',
marginTop: 10,
marginLeft: 30,


},
card: {
marginLeft: 30,
marginRight: 30,
marginTop:'10%',
height:50,
flexDirection: 'row',
justifyContent:'flex-start',
alignItems: 'center',
shadowOpacity: 0.3,
shadowRadius:3.84,
shadowOffset: {width: 0,height: 4,},
shadowColor: "#E69151",
elevation:3,



},
forgetpass: {
marginTop:20,
backgroundColor:'transparent'



},
carditem: {
textAlign: 'left',
marginLeft: 10,
color: '#000000',
},
buttonsignin: {
justifyContent:'center',
backgroundColor: '#E69151',
height:50,
width:300,
marginTop:10,
borderBottomLeftRadius: 5,
borderBottomRightRadius: 5,
borderTopLeftRadius:5, 
borderTopRightRadius: 5,

},
buttonrigster: {

justifyContent:'center',
backgroundColor: '#ffffff',
height:60,
width:250,
marginTop:20,
borderWidth: 2,
borderColor: '#09818F',
borderBottomLeftRadius: 30,
borderBottomRightRadius: 30,
borderTopLeftRadius: 30, 
borderTopRightRadius: 30,

},
buttonrigsterfpass: {

justifyContent:'center',
backgroundColor: '#ffffff',
height:50,
width:200,

marginTop:2,
borderWidth: 2,
borderColor: '#151B54',
borderBottomLeftRadius: 30,
borderBottomRightRadius: 0,
borderTopLeftRadius:0, 
borderTopRightRadius: 30,

},
buttontextwhite:{
fontSize:15,
color:"white",
textAlign: 'center',
},
buttontextblack:{
fontSize:15,
color:"#09818F",
textAlign: 'center',
},
centerview:{

flex: 1,
flexDirection:'column',
justifyContent: 'center',
alignItems: 'center',
marginTop:30,


},
centerviewl:{

flex: 1,
flexDirection:'column',
justifyContent: 'space-around',
alignItems: 'center',
marginTop:30,

},
centerviewfind:{

flex: 1,
flexDirection:'column',
justifyContent: 'space-around',
alignItems: 'center',
marginTop: 5,

},
centerviewf:{

flex: 1,
flexDirection:'column',
justifyContent: 'space-between',
alignItems: 'center',
marginTop:30,


},
loginbuttonsignin:{
justifyContent:'center',
backgroundColor: '#151B54',
height:60,
marginLeft:70,
marginRight:70,

marginTop:10,
borderBottomLeftRadius: 30,
borderBottomRightRadius: 0,
borderTopLeftRadius:0, 
borderTopRightRadius: 30,
},
loginbuttonsignup:{
justifyContent:'center',
backgroundColor: '#ffffff',
height:60,
marginLeft:70,
marginRight:70,

marginTop:15,
borderWidth: 2,
borderColor: '#151B54',
borderBottomLeftRadius: 30,
borderBottomRightRadius: 0,
borderTopLeftRadius:0, 
borderTopRightRadius: 30,
},
buttonhireCandidate: {
justifyContent:'center',
backgroundColor: COLOR.RED,
height:60,
width:250,

marginTop:10,
borderBottomLeftRadius: 30,
borderBottomRightRadius: 0,
borderTopLeftRadius:0, 
borderTopRightRadius: 30,

},
buttonjobsearch: {
justifyContent:'center',
backgroundColor: 'red',
height:60,
width:250,

marginTop:10,
borderBottomLeftRadius: 0,
borderBottomRightRadius: 30,
borderTopLeftRadius:30, 
borderTopRightRadius: 0,


},
});