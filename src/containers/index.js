import React, { Component } from 'react';
import { Router, Scene } from "react-native-router-flux";
import { connect } from "react-redux";
import { AsyncStorage, Text } from "react-native";
import Loader from './../constants/Loader';
import Splash from './../containers/Splash';
import Login from './../containers/Login';
import Signup from './../containers/Signup';
import Home from './../containers/Home';
import TabNavigator from './../containers/TabNavigator';
import Favourites from './../containers/Favourites';
import Tab from './../containers/Tab';
import NewProducts from './../containers/NewProducts';
import MostViewed from './../containers/MostViewed';
import LikedTab from './../containers/LikedTab';








const RouterWithRedux = connect()(Router);

class Root extends Component {
    constructor(props) {
        super(props);

        this.state = {
            token: null,
            isStorageLoaded: false
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('token').then((token) => {
            this.setState({
                token: token !== null,
                isStorageLoaded: true
            })
        });
    }

    render() {
        let { isLogged } = this.props.login;
        let { token, isStorageLoaded } = this.state;
        if (!isStorageLoaded) {
            return (
                <Loader loading={true} />
            )
        } else {
            return (

                <RouterWithRedux>
                    <Scene key='root'>
                        <Scene
                            component={Splash}
                            initial={true}
                            hideNavBar={true}
                            key='Splash'
                            title='Splash'
                        />
                        <Scene
                            component={Login}
                            initial={false}
                            hideNavBar={true}
                            key='Login'
                            title='Login'
                        />
                        <Scene
                            component={Signup}
                            initial={false}
                            hideNavBar={true}
                            key='Signup'
                            title='Signup'
                        />
                        <Scene
                            component={Home}
                            initial={true}
                            hideNavBar={true}
                            key='Home'
                            title='Home'
                        />
                      
                        <Scene
                            component={Favourites}
                            initial={false}
                            hideNavBar={true}
                            key='Favourites'
                            title='Favourites'
                        />
                           <Scene
                            component={Tab}
                            initial={false}
                            hideNavBar={true}
                            key='Tab'
                            title='Tab'
                        />
                         <Scene
                            component={NewProducts}
                            initial={false}
                            hideNavBar={true}
                            key='NewProducts'
                            title='NewProducts'
                        />
                          <Scene
                            component={MostViewed}
                            initial={false}
                            hideNavBar={true}
                            key='MostViewed'
                            title='MostViewed'
                        />
                         <Scene
                            component={LikedTab}
                            initial={false}
                            hideNavBar={true}
                            key='LikedTab'
                            title='LikedTab'
                        />

                           



                    </Scene>
                </RouterWithRedux>
            )
        }
    }
}
const mapStateToProps = (state) => {
    return {
        login: state.login
    }
};
export default connect(mapStateToProps)(Root)
console.disableYellowBox = true