import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, KeyboardAvoidingView,ActivityIndicator
} from "react-native";
import styles from './styles';
import { TextField } from 'react-native-material-textfield';
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';


var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class Forget extends React.Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            secureKey: true,
            isLoading:false
        };
    }

    secureKey() {
        if (this.state.secureKey == false) {
            this.setState({
                secureKey: true
            })
        }
        else {
            this.setState({
                secureKey: false
            })
        }
    }

    mLoaderShowHide() {
        this.setState({
            isLoading: !this.state.isLoading
        });
      };

    mValidation() {
        if(this.state.username.length<=0){
        alert('Please enter username')
        return false;
        }
        this.mLoaderShowHide();
        this.forget(this.state.username);
            
        }
    
        forget(username) {
            let form_data = new FormData();
            form_data.append("username",username)
             const requestOptions = {
             method: 'POST',
             body: form_data
             };
             fetch('https://beta.pedallion.com/api/forgot_password', requestOptions)
             .then((response) => this.handleResponse(response))
             .then(user => {
             });
             }

             handleResponse(response) {
                response.text().then(text => {
                    if (response.status==200) {
                    const data = text && JSON.parse(text);
                    this.mLoaderShowHide();
                    if(data.status==1){
                    alert(data.msg)
                    this.props.navigation.navigate('Login')
                    }else{
                    alert(data.msg)
                    this.props.navigation.navigate('Login')
                    }
                    }else{
                    this.mLoaderShowHide();
                    }
       
       
                  
               });
           }
       



    render() {
        return (
            <View>
<KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <View>
                    <View>
                        <ImageBackground source={require("../../components/Images/login_bg_2.jpg")}
                            style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                            <View   style={{height:hp('25%')}}/>
                            <View style={styles.view1}>
                            <View style={{marginTop:10,marginBottom:10,alignSelf:'center'}}>
                          
                                        <Text style={{
                                             color: '#fe5328',
                                             fontSize: RF(2),
                                             fontWeight: 'bold'
                                        }}>FORGOT PASSWORD</Text>
                          
                                    </View>
                                <View style={{ flexDirection: 'row', alignSelf: 'center',alignItems:'center',height: hp('7%') }}>


                             

                                    <View style={{ alignSelf: 'center' }}>
                                        <Image source={require("../../components/Images/Loginimg.png")}
                                            style={{ width: wp('4%'), height: hp('2%'), }}>

                                        </Image>
                                    </View>
                                    <View style={{ width: wp('70%') }}>
                                        <TextInput style={styles.textinput1}
                                            underlineColorAndroid="transparent"
                                            placeholder="Username"
                                            placeholderTextColor="#999999"
                                            autoCapitalize="none"
                                            onChangeText={(text) => this.setState({ username: text })} />
                                    </View>
                                </View>
                                <View style={{
                                    height: 1,
                                    width: wp('75%'),
                                    backgroundColor: '#e9e9e9',
                                    alignSelf: 'center',
                                   
                                }} />
                              
                                <View style={{
                                    height: 1,
                                    width: wp('75%'),
                                    backgroundColor: '#e9e9e9',
                                    alignSelf: 'center'
                                }} />
                                <TouchableOpacity
                                  onPress={() => this.mValidation()}
                                >
                                    <LinearGradient style={{
                                        height: hp('8%'),
                                        width: wp('75%'),
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderRadius: 2,
                                        alignSelf: 'center',
                                        marginTop: 20
                                    }}
                                        start={{
                                            x: 0,
                                            y: 0
                                        }}
                                        end={{
                                            x: 0,
                                            y: 1
                                        }}
                                        colors={['#feae3d',
                                            '#ff8333',
                                            '#ff572b',]}>
                                        <Text style={{
                                            color: '#fff',
                                            fontSize: RF(2.8),
                                            fontWeight: 'bold'
                                        }}>SUBMIT</Text>

                                    </LinearGradient>
                                </TouchableOpacity>
                             
                               
                                <View style={{ height: 15 }} />
                            </View>
                            

                            <View style={{marginTop:10,alignSelf:'center'}}>
                            <TouchableOpacity onPress={() => {this.props.navigation.navigate('Login')} }>
                                        <Text style={{
                                             color: '#fe5328',
                                             fontSize: RF(2),
                                             textDecorationLine: 'underline',
                                             fontWeight: 'bold'
                                        }}>Back to Login</Text>
                                        </TouchableOpacity>
                                    </View>
                                  
                        </ImageBackground>
                    </View>
                  

                </View>
                </KeyboardAvoidingView> 
                {this.state.isLoading==true ? <View style={styles.overlay}>
                  <ActivityIndicator size="large" color="#f27029" />
                  </View> : <View></View>}
                </View>

        );
    }
}


export default Forget;