import { StyleSheet, Dimensions,Platform } from 'react-native'; 
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import RF from "react-native-responsive-fontsize";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


export const COLOR = {
    BLACK: "#000",
    GRAY: "#9A9A9A",
    WHITE: "#FFF",
    LIGHT_ORANGE: "#E69151"
};
var padding
var paddingvet
    Platform.select({
      ios: () => {
        padding = 15
        paddingvet = null
      },
      android: () => {
        padding = 0
        paddingvet = 15
      }
    })();
export default StyleSheet.create({

    container: {
        backgroundColor: '#CCCCCC',
        height: Dimensions.get('window').height,
        display: 'flex',
        width: '100%',
    },
    textinput1: {
        color: '#000',
        marginLeft: 10,
        fontSize:RF(2.3),marginTop:2
    },
    view1:{
        width: width / 1.15,
        backgroundColor: '#fff',
        alignSelf: 'center',
        shadowOffset: { width: 0, height: 3, },
        shadowColor: '#000000',
        elevation: 5,
        padding: 15,
    },
    overlay:{
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        height:'100%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'

    },
 
});