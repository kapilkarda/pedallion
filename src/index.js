import React, { Component } from 'react';
import {createStackNavigator, createDrawerNavigator,createMaterialTopTabNavigator, createAppContainer} from 'react-navigation';
import {Image} from 'react-native';

import Home from './containers/Screens/HomeScreen/Home';
import Favourite from './containers/Screens/FavouriteScreen/Favourite';
import MostViewed from './containers/Screens/MostViewed';
import NewProducts from './containers/Screens/NewProducts';
import Shopes from './containers/Screens/Shopes';

import Liked from './containers/Screens/Liked';
import Cart from './containers/Screens/Cart';
import Splash from './containers/Splash/Splash';
import Login from './containers/Login/Login';
import Forget from './containers/Forget/Forget';
import SignUp from './containers/SignUp/SignUp';

import ProductList from './containers/Screens/ProductList';

import ProductDetail from './containers/Screens/ProductDetail';
import UserMessage from './containers/Screens/UserMessage';
import ShopDetail from './containers/Screens/ShopDetail';

import ShopProductList from './containers/Screens/ShopProductList';
import ShopPolicies from './containers/Screens/ShopPolicies';
import ShopReviews from './containers/Screens/ShopReviews';
import ShopMessage from './containers/Screens/ShopMessage';
import ProductUploadOne from './containers/Screens/ProductUploadOne';
import ProductDetailConfirm from './containers/Screens/ProductDetailConfirm';
import Notification from './containers/Screens/Notification';
import ProductImageUpload from './containers/Screens/ProductImageUpload';
import MessageDetail from './containers/Screens/MessageDetail';
import ShareAndEarn from './containers/Screens/ShareAndEarn';
import MyWallet from './containers/Screens/MyWallet';
import MyAccount from './containers/Screens/MyAccount';
import PrivacyPolicy from './containers/Screens/PrivacyPolicy';
import ContactUs from './containers/Screens/ContactUs';
import RewardPoint from './containers/Screens/RewardPoint';
import MyOrder from './containers/Screens/MyOrder';
import AddUserAddress from './containers/Screens/AddUserAddress';
import UpdateBankDetail from './containers/Screens/UpdateBankDetail';
import UpdateUserAddress from './containers/Screens/UpdateUserAddress';
import RequestWithdrawn from './containers/Screens/RequestWithdrawn';
import FAQ from './containers/Screens/FAQ';
import UpdateUserProfile from './containers/Screens/UpdateUserProfile';
import FilterScreen from './containers/Screens/FilterScreen';
import RequestAddMoney from './containers/Screens/RequestAddMoney';
import FavoritScreen from './containers/Screens/FavoritScreen';
import FAQExplaination from './containers/Screens/FAQExplaination';
import MyOrders from './containers/Screens/MyOrders';






import DrawerScreen from './containers/Screens/DrawerScreen';
import CustomHeader from "./containers/Screens/CustomHeader";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';



const Tabs = createMaterialTopTabNavigator({

    Home:{
        screen:Home,
        navigationOptions: {
          tabBarIcon: ({ tintColor, focused }) => (
          <Image 
          source={focused ? require('./components/Images/home_filled.png') : require('./components/Images/home.png') }
          style={{width: hp('4%'),height: hp('4%'),}}/>
          ),

      
         
            
        }
      },

      Favourite:{
        screen:Favourite,
        navigationOptions: {
          tabBarIcon: ({ tintColor, focused }) => (
            <Image 
            source={focused ? require('./components/Images/star.png') : require('./components/Images/star1.png') }
            style={{width: hp('4%'),height: hp('4%'),}}/>
            ),
        }
      },

      NewProducts:{
        screen:NewProducts,
        navigationOptions: {
          tabBarIcon: ({ tintColor, focused }) => (
            <Image 
            source={focused ? require('./components/Images/newfilled.png') : require('./components/Images/new.png') }
            style={{width: hp('4%'),height: hp('4%'),}}/>
            ),
        }
      },

      Shopes:{
        screen:Shopes,
        navigationOptions: {

          tabBarIcon: ({ tintColor, focused }) => (
            <Image 
            source={focused ? require('./components/Images/boxfilled.png') : require('./components/Images/box.png') }
            style={{width: hp('4%'),height: hp('4%'),}}/>
            ),
        }
      },

      MostViewed:{
        screen:MostViewed,
        navigationOptions: {

          tabBarIcon: ({ tintColor, focused }) => (
            <Image 
            source={focused ? require('./components/Images/eyefilled.png') : require('./components/Images/eye.png') }
            style={{width: hp('4%'),height: hp('4%'),}}/>
            ),
        }
      },

      Liked:{
        screen:Liked,
        navigationOptions: {

          tabBarIcon: ({ tintColor, focused }) => (
            <Image 
            source={focused ? require('./components/Images/thambsfilled.png') : require('./components/Images/thambs.png') }
            style={{width: hp('4%'),height: hp('4%'),}}/>
            ),
        }
      },


},{
    tabBarOptions: {
      showIcon:true,
      showLabel:false,
      activeTintColor:'#ffffff',
      style:{backgroundColor:'#FF8328'},
    },
      initialRouteName: 'Home',
      mode: 'modal',
      headerMode: 'none',
  });

const DrawerNavigator = createDrawerNavigator({
    Home:{
        screen: Tabs,
    },
    
},{
    initialRouteName: 'Home',
    contentComponent: DrawerScreen,
    drawerWidth: 150
});

const MenuImage = ({navigation}) => {
    if(!navigation.state.isDrawerOpen){
        return <Image source={require('./containers/Screens/menu-button.png')}/>
    }else{
        return <Image source={require('./containers/Screens/left-arrow.png')}/>
    }
}

const StackNavigator = createStackNavigator({
    
    //important: key and screen name (i.e. DrawerNavigator) should be same while using the drawer navigator inside stack navigator.
    
    
    Splash:{
      screen: Splash,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
  },
  Login:{
    screen: Login,
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
  }
},
Forget:{
  screen: Forget,
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
}
},

SignUp:{
  screen: SignUp,
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
}
},

    DrawerNavigator:{
        screen: DrawerNavigator,
        navigationOptions: {
          header: props => <CustomHeader {...props} />
        }
    },
    Cart:{
      screen: Cart,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    ProductList:{
      screen: ProductList,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    ProductDetail:{
      screen: ProductDetail,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    UserMessage:{
      screen: UserMessage,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ShopDetail:{
      screen: ShopDetail,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ShopProductList:{
      screen: ShopProductList,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ShopPolicies:{
      screen: ShopPolicies,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    ShopReviews:{
      screen: ShopReviews,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    ShopMessage:{
      screen: ShopMessage,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ProductUploadOne:{
      screen: ProductUploadOne,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    ProductDetailConfirm:{
      screen: ProductDetailConfirm,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ProductImageUpload:{
      screen: ProductImageUpload,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    Notification:{
      screen: Notification,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    MessageDetail:{
      screen: MessageDetail,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ShareAndEarn:{
      screen: ShareAndEarn,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    MyWallet:{
      screen: MyWallet,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    MyAccount:{
      screen: MyAccount,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    PrivacyPolicy:{
      screen: PrivacyPolicy,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    ContactUs:{
      screen: ContactUs,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    RewardPoint:{
      screen: RewardPoint,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    MyOrder:{
      screen: MyOrder,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    AddUserAddress:{
      screen: AddUserAddress,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    UpdateBankDetail:{
      screen: UpdateBankDetail,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    UpdateUserAddress:{
      screen: UpdateUserAddress,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    RequestWithdrawn:{
      screen: RequestWithdrawn,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    FAQ:{
      screen: FAQ,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    UpdateUserProfile:{
      screen: UpdateUserProfile,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    FilterScreen:{
      screen: FilterScreen,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    RequestAddMoney:{
      screen: RequestAddMoney,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

    FavoritScreen:{
      screen: FavoritScreen,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    FAQExplaination:{
      screen: FAQExplaination,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },
    MyOrders:{
      screen: MyOrders,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
    }
    },

});
const App = createAppContainer(StackNavigator);
export default App;
